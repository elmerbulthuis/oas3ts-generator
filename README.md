# OAS3TS Generator
...

## Schema models
Every schema will be a model, the name of the model is derived from the pointer that points to the schema. The name is as short as possible, but still unique.

Names are pascal cased, property names are left as they are. This is mainly because renaming some properties to be camel case would be a huge performance hit.

## Parameter models
For every operation there are the following parameter models:
- incoming parameter request
- outgoing parameter request
- incoming parameter response
- outgoing parameter response

Paremeter models are a map of the parameters name and the corrseponding schema model. The names of the parameters are camel cased.

There is a difference between incoming and outgoing parameters. Default values are required in the incoming version, but are optional in the outgoing parameters.

## Request and response models
For every operation there are four request or response models
- incoming request
- outgoing request
- incoming response
- outgoing response

We need different request and response models because the parameters models that are encapsulated by these models have different incoming and outgoing versions.

## client flow
Parameters might be validated for debugging purpose
Parameters are marshalled into path, querystring, headers and cookies

querystring is strigified
router stringifies the path parameters
cookies are stringified

authorization is marshalled into querystring, headers or cookies

the request is made!

the entity(ies) might be validated
entities are marshalled

The response parameters are unmarshalled from headers
response parameters might be validated

responsee entities are unmarshalled from chunks
and might be validated


## server flow
For every request

First perform routing
Then parse querystring
(headers are parsed by node lib)
Then parse cookies, cookies are a header

Then perform authorization, authorization is based on querystring, headers or cookies
authorization is unmarshalling credentials and then validating credentials via the provided authorization functions.

Then unmarshall params from path, querystring, headers and cookies
Unmarshalling the params also means casting the primitives to whatever is described in the spec

Then validate the params!

Now we are ready to execute the handler. This handler gets the parameters passed and there are also methods passed to the handler to retrieve the body. Those methods will also trigger validation.

When the body retrievel functions (like entity or entities) is used we first unmarshall the chunk of data in the body or the entire body, depending on the function used.
Then the unmarshalled chunk will be validated.

The handler function will return a response.

Optionally the parameters in the response could be validated (might be useful for debugging!)
Then the parameters are marshalled into headers that are added to the response
The response headers are sent.

Then, id neccessary, the body of the repsponse is sent. If this is an entity, the entity might be validated first, and then marshalled to the response stream.

## content-negotiation
request and response content types can be specifed when generating the clients and servers. The first type is the default type used for the requests or responses coming from clients or servers, so these are the outgoing requests and responses.

The client will send an accept header that should be respected by the server. This accep header has all the content types that are available for the operations response ordered via the `q` attribute.

The server will read the accept header and sort the values by the `q` attribute. The server will then response with the content-type that comes first and has a response defined for the status code of the response.

