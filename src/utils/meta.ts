import ts from "typescript";

export interface MetaInfo {
    description?: string,
    deprecated?: boolean,
}
export function addMeta(
    node: ts.Node,
    { description, deprecated }: MetaInfo,
) {
    const parts = new Array<string>();
    if (description) parts.push(description.trim());
    if (deprecated) parts.push("@deprecated");

    ts.addSyntheticLeadingComment(
        node,
        ts.SyntaxKind.MultiLineCommentTrivia,
        "*\n" + parts.map(part => part + "\n").join(""),
        true,
    );
}
