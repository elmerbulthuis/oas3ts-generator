import assert from "assert";
import camelcase from "camelcase";
import { toReference } from "./index.js";

export interface NameMeta {
    pointerParts: Array<string>,
    nameParts: Array<string>,
}

export function* generateNamePairs(
    nameMetas: Iterable<NameMeta>,
): Iterable<[string, string]> {
    const nameMetaSet = new Set(nameMetas);
    let nameCounters: Record<string, number>;

    nameCounters = {};

    for (const nameMeta of nameMetaSet) {
        const { nameParts } = nameMeta;
        const name = stringifyType(nameParts);

        nameCounters[name] ??= 0;
        nameCounters[name] += 1;
    }

    for (const nameMeta of nameMetaSet) {
        const { pointerParts, nameParts } = nameMeta;
        const name = stringifyType(nameParts);

        assert(/^[a-zA-Z_$]/.test(name));
        if (nameCounters[name] !== 1) continue;

        nameMetaSet.delete(nameMeta);

        const reference = toReference(pointerParts);

        yield [reference, name];
    }

    nameCounters = {};

    for (const nameMeta of nameMetaSet) {
        const { pointerParts, nameParts } = nameMeta;
        const name = stringifyType(nameParts);

        nameMetaSet.delete(nameMeta);

        nameCounters[name] ??= 0;
        nameCounters[name] += 1;

        const reference = toReference(pointerParts);

        yield [reference, name + String(nameCounters[name])];
    }

}

export function stringifyIdentifier(nameParts: string[]) {
    return camelcase(
        nameParts.map(s => s.replace(/[^\w]/g, " ")),
        { pascalCase: false, preserveConsecutiveUppercase: true },
    );
}

export function stringifyType(nameParts: string[]) {
    return camelcase(
        nameParts.map(s => s.replace(/[^\w]/g, " ")),
        { pascalCase: true, preserveConsecutiveUppercase: true },
    );
}
