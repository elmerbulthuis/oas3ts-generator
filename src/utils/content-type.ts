export type ContentKind =
    "json" |
    "form" |
    "text" |
    "stream" |
    "empty";

export function getContentKind(contentType: string | null): ContentKind {
    switch (contentType ?? null) {
        case null:
            return "empty";

        case "application/json":
        case "application/ld+json":
        case "application/vnd.api+json":
        case "application/x-ndjson":
        case "application/json-seq":
            return "json";

        case "application/x-www-form-urlencoded":
            return "form";

        case "text/plain":
            return "text";

        case "application/octet-stream":
        case "application/x-tar":
        case "application/gzip":
            return "stream";

        default:
            throw new TypeError(`contentType ${contentType} not supported`);
    }
}
