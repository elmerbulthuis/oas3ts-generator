import { OpenAPIV3 } from "openapi-types";

export function resolvePointerParts(
    pointerParts: string[],
    maybeRef: OpenAPIV3.ReferenceObject | object | undefined,
) {
    if (maybeRef == null) return undefined;

    if (isReferenceObject(maybeRef)) {
        return fromReference(maybeRef.$ref);
    }

    return pointerParts;
}

export function isReferenceObject(
    maybeReference: OpenAPIV3.ReferenceObject | object,
): maybeReference is OpenAPIV3.ReferenceObject {
    return "$ref" in maybeReference;
}

export function resolveObject<T extends object>(
    document: OpenAPIV3.Document,
    maybeReferenceObject: OpenAPIV3.ReferenceObject | T | undefined,
) {
    if (!maybeReferenceObject) return maybeReferenceObject;

    if (isReferenceObject(maybeReferenceObject)) {
        return resolveReference(document, maybeReferenceObject.$ref) as T;
    }

    return maybeReferenceObject;
}

export function resolveReference(
    document: OpenAPIV3.Document,
    reference: string,
): unknown {
    const modelPath = fromReference(reference);
    const resolved = resolveModelPath(document, modelPath);
    return resolved;
}

export function resolveModelPath(
    document: OpenAPIV3.Document,
    modelPath: Array<string>,
): unknown {
    let resolved = document as any;

    for (const modelPathPart of modelPath) {
        if (typeof resolved !== "object") throw new Error("bad $ref");

        resolved = resolved[modelPathPart];
    }

    return resolved;
}

export function toReference(
    pointerParts: Array<string>,
): string {
    return "#/" + pointerParts.
        map(encodeURIComponent).
        join("/");
}

export function fromReference(
    reference: string,
): Array<string> {
    if (!reference.startsWith("#/")) {
        throw new Error("only local, absolute, refs are supported (starting with \"#/\")");
    }

    const modelPath = reference.
        split("/").
        slice(1).
        map(decodeURIComponent);

    return modelPath;
}

