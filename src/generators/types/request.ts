import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllOperations, selectContentTypesFromContentContainer } from "../../selectors/index.js";
import { getContentKind, resolveObject, resolvePointerParts, stringifyType, toReference } from "../../utils/index.js";
import { PackageConfig } from "../index.js";

export function* generateRequestTypeDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    config: PackageConfig,
    nameIndex: Record<string, string>,
    direction: "incoming" | "outgoing",
) {
    for (const { operationObject, path, method } of selectAllOperations(document)) {
        yield createFromOperation(
            operationObject,
            path,
            method,
        );
    }

    function createFromOperation(
        operationObject: OpenAPIV3.OperationObject,
        path: string,
        method: OpenAPIV3.HttpMethods,
    ) {
        assert(operationObject.operationId);

        const typeName = stringifyType([
            operationObject.operationId,
            direction,
            "request",
        ]);

        const pointerParts = ["paths", path, method];

        return factory.createTypeAliasDeclaration(
            [
                factory.createToken(ts.SyntaxKind.ExportKeyword),
            ],
            typeName,
            undefined,
            factory.createUnionTypeNode([...emitRequestTypes(
                operationObject,
                pointerParts,
            )]),
        );
    }

    function* emitRequestTypes(
        operationObject: OpenAPIV3.OperationObject,
        pointerParts: string[],
    ) {
        assert(operationObject.operationId);

        const requestBody = resolveObject(document, operationObject.requestBody);
        const requestBodyPointerParts = resolvePointerParts(
            [...pointerParts, "requestBody"],
            operationObject.requestBody,
        );

        const contentTypeEntries = requestBody && requestBodyPointerParts ?
            [...selectContentTypesFromContentContainer(
                config.requestTypes,
                requestBody,
                requestBodyPointerParts,
            )] :
            [];

        const parametersTypeName = stringifyType([
            operationObject.operationId,
            "request",
            "parameters",
        ]);

        if (contentTypeEntries.length === 0) {
            if (direction === "outgoing") {
                yield createTypeNode([], null, null, parametersTypeName, true);
            }

            yield createTypeNode([], null, null, parametersTypeName, false);
        }
        else {
            assert(requestBodyPointerParts);

            if (direction === "outgoing") {
                const [defaultContentTypeEntry] = contentTypeEntries;
                const { contentType, mediaTypeObject } = defaultContentTypeEntry;
                yield createTypeNode(
                    [...requestBodyPointerParts, "content", contentType],
                    contentType,
                    mediaTypeObject,
                    parametersTypeName,
                    true,
                );
            }

            for (const { contentType, mediaTypeObject } of contentTypeEntries) {
                yield createTypeNode(
                    [...requestBodyPointerParts, "content", contentType],
                    contentType,
                    mediaTypeObject,
                    parametersTypeName,
                    false,
                );
            }
        }

    }

    function createTypeNode(
        pointerParts: string[],
        contentType: string | null,
        mediaTypeObject: OpenAPIV3.MediaTypeObject | null,
        parametersTypeName: string,
        isDefault: boolean,
    ) {
        const contentKind = getContentKind(contentType);

        const parametersTypeNode = factory.createTypeReferenceNode(
            factory.createQualifiedName(
                factory.createIdentifier("shared"),
                parametersTypeName,
            ),
        );

        const contentTypeTypeNode = factory.createLiteralTypeNode(
            factory.createStringLiteral(contentType ?? ""),
        );

        const schemaPointerParts = resolvePointerParts(
            [...pointerParts, "schema"],
            mediaTypeObject?.schema,
        );

        const modelReference = schemaPointerParts && toReference(schemaPointerParts);
        const modelName = modelReference && nameIndex[modelReference];

        const bodyModelTypeNode = modelName && factory.createTypeReferenceNode(
            factory.createQualifiedName(
                factory.createIdentifier("shared"),
                modelName,
            ),
        );

        const typeNameParts = [direction, contentKind, "request"];
        if (isDefault) typeNameParts.push("default");
        const typeName = stringifyType(typeNameParts);

        switch (contentKind) {
            case "empty":
                return factory.createTypeReferenceNode(
                    factory.createQualifiedName(
                        factory.createIdentifier("lib"),
                        typeName,
                    ),
                    [
                        parametersTypeNode,
                    ],
                );

            case "stream":
            case "text":
                return factory.createTypeReferenceNode(
                    factory.createQualifiedName(
                        factory.createIdentifier("lib"),
                        typeName,
                    ),
                    isDefault ?
                        [
                            parametersTypeNode,
                        ] :
                        [
                            parametersTypeNode,
                            contentTypeTypeNode,
                        ],
                );

            case "json":
            case "form":
                assert(bodyModelTypeNode);

                return factory.createTypeReferenceNode(
                    factory.createQualifiedName(
                        factory.createIdentifier("lib"),
                        typeName,
                    ),
                    isDefault ?
                        [
                            parametersTypeNode,
                            bodyModelTypeNode,
                        ] :
                        [
                            parametersTypeNode,
                            contentTypeTypeNode,
                            bodyModelTypeNode,
                        ],
                );

            default: assert.fail();
        }

    }

}
