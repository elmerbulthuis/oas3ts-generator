import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllOperations } from "../../selectors/index.js";
import { stringifyIdentifier, stringifyType } from "../../utils/name.js";

export function* generateOperationAuthorizationTypeDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
) {
    for (
        const { operationObject, method, path } of
        selectAllOperations(document)
    ) {
        yield createAuthorizationType(
            operationObject,
        );
    }

    function createAuthorizationType(
        operationObject: OpenAPIV3.OperationObject,
    ) {
        assert(operationObject.operationId);

        const authorizationTypeName = stringifyType([
            operationObject.operationId,
            "authorization",
        ]);

        const requirements = operationObject.security ?? document.security ?? [];

        const authorizationType = requirements.length === 0 ?
            factory.createKeywordTypeNode(ts.SyntaxKind.ObjectKeyword) :
            factory.createUnionTypeNode(requirements.map(
                requirement => {
                    const securitySchemes = Object.keys(requirement);
                    return factory.createTypeReferenceNode(
                        "Pick",
                        [
                            factory.createTypeReferenceNode("Authorization"),
                            securitySchemes.length === 0 ?
                                factory.createKeywordTypeNode(ts.SyntaxKind.NeverKeyword) :
                                factory.createUnionTypeNode(securitySchemes.map(
                                    scheme => factory.createLiteralTypeNode(
                                        factory.createStringLiteral(
                                            stringifyIdentifier([scheme]),
                                        ),
                                    ),
                                )),
                        ],
                    );
                },
            ));

        return factory.createTypeAliasDeclaration(
            [
                factory.createToken(ts.SyntaxKind.ExportKeyword),
            ],
            authorizationTypeName,
            [
                factory.createTypeParameterDeclaration(
                    undefined,
                    "Authorization",
                    factory.createTypeReferenceNode("ServerAuthorization"),
                    factory.createTypeReferenceNode("ServerAuthorization"),
                ),
            ],
            authorizationType,
        );
    }

}
