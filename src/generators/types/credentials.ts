import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllSecuritySchemes } from "../../selectors/index.js";
import { toReference } from "../../utils/index.js";

export function* generateCredentialTypeDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    nameIndex: Record<string, string>,
) {

    for (const { securitySchemeObject, pointerParts } of selectAllSecuritySchemes(document)) {
        yield createCredentialTypeDeclaration(
            securitySchemeObject,
            pointerParts,
        );
    }

    function createCredentialTypeDeclaration(
        securitySchemeObject: OpenAPIV3.SecuritySchemeObject,
        securitySchemaPointerParts: string[],
    ) {
        const securitySchemeTypeReference = toReference(securitySchemaPointerParts);
        const securitySchemeTypeName = nameIndex[securitySchemeTypeReference];

        switch (securitySchemeObject.type) {
            case "apiKey":
                return factory.createTypeAliasDeclaration(
                    [
                        factory.createToken(ts.SyntaxKind.ExportKeyword),
                    ],
                    securitySchemeTypeName,
                    undefined,
                    factory.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
                );

            case "http":
                switch (securitySchemeObject.scheme) {
                    case "bearer":
                        return factory.createTypeAliasDeclaration(
                            [
                                factory.createToken(ts.SyntaxKind.ExportKeyword),
                            ],
                            securitySchemeTypeName,
                            undefined,
                            factory.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
                        );

                    case "basic":
                        return factory.createInterfaceDeclaration(
                            [
                                factory.createToken(ts.SyntaxKind.ExportKeyword),
                            ],
                            securitySchemeTypeName,
                            undefined,
                            undefined,
                            [
                                factory.createPropertySignature(
                                    undefined,
                                    factory.createIdentifier("username"),
                                    undefined,
                                    factory.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
                                ),
                                factory.createPropertySignature(
                                    undefined,
                                    factory.createIdentifier("password"),
                                    undefined,
                                    factory.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
                                ),

                            ],
                        );

                    default: throw new Error("security schema schema not supported");
                }

            case "oauth2":
                if (securitySchemeObject.flows.clientCredentials) {
                    return factory.createInterfaceDeclaration(
                        [
                            factory.createToken(ts.SyntaxKind.ExportKeyword),
                        ],
                        securitySchemeTypeName,
                        undefined,
                        undefined,
                        [
                            factory.createPropertySignature(
                                undefined,
                                factory.createIdentifier("clientId"),
                                undefined,
                                factory.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
                            ),
                            factory.createPropertySignature(
                                undefined,
                                factory.createIdentifier("clientSecret"),
                                undefined,
                                factory.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
                            ),

                        ],
                    );
                }
                else {
                    throw new Error("security scheme flow not supported");
                }

            default: throw new Error("security scheme type not supported");

        }

    }

}
