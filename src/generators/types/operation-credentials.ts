import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllOperations, selectSecuritySchemesForOperation } from "../../selectors/index.js";
import { toReference } from "../../utils/index.js";
import { stringifyIdentifier, stringifyType } from "../../utils/name.js";

export function* generateOperationCredentialsTypeDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    nameIndex: Record<string, string>,
) {
    for (
        const { operationObject, method, path } of
        selectAllOperations(document)
    ) {
        yield createCredentialsType(
            operationObject,
        );
    }

    function createCredentialsType(
        operationObject: OpenAPIV3.OperationObject,
    ) {
        assert(operationObject.operationId);

        const credentialsTypeName = stringifyType([
            operationObject.operationId,
            "credentials",
        ]);

        const requirements = operationObject.security ?? document.security ?? [];

        if (requirements.length === 0) {
            return factory.createTypeAliasDeclaration(
                [
                    factory.createToken(ts.SyntaxKind.ExportKeyword),
                ],
                credentialsTypeName,
                undefined,
                factory.createKeywordTypeNode(ts.SyntaxKind.ObjectKeyword),
            );
        }
        else {
            return factory.createInterfaceDeclaration(
                [
                    factory.createToken(ts.SyntaxKind.ExportKeyword),
                ],
                credentialsTypeName,
                undefined,
                undefined,
                [
                    ...selectSecuritySchemesForOperation(document, operationObject),
                ].map(({ pointerParts, securityScheme }) => {
                    const securitySchemeTypeReference = toReference(pointerParts);
                    const securitySchemeTypeName = nameIndex[securitySchemeTypeReference];

                    return factory.createPropertySignature(
                        undefined,
                        stringifyIdentifier([securityScheme]),
                        factory.createToken(ts.SyntaxKind.QuestionToken),
                        factory.createTypeReferenceNode(
                            factory.createQualifiedName(
                                factory.createIdentifier("shared"),
                                factory.createIdentifier(securitySchemeTypeName),
                            ),
                        ),
                    );
                }),
            );
        }

    }

}
