import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { generateDeserializeEntityDeclarations, generateExtractParametersDeclarations, generateInjectCredentialsDeclarations, generateInjectParametersDeclarations, generateSerializeEntityDeclarations } from "../functions/index.js";
import { PackageConfig } from "../index.js";

export function createClientInternalSourceFile(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    config: PackageConfig,
    nameIndex: Record<string, string>,
) {
    const nodes = [
        ...emitImports(),
        ...generateInjectCredentialsDeclarations(
            factory,
            document,
            nameIndex,
        ),
        ...generateInjectParametersDeclarations(
            factory,
            document,
            nameIndex,
            "request",
        ),
        ...generateExtractParametersDeclarations(
            factory,
            document,
            nameIndex,
            "response",
        ),
        ...generateSerializeEntityDeclarations(
            factory,
            document,
            config,
            nameIndex,
            "request",
        ),
        ...generateDeserializeEntityDeclarations(
            factory,
            document,
            config,
            nameIndex,
            "response",
        ),
    ];

    const sourceFile = factory.createSourceFile(
        nodes,
        factory.createToken(ts.SyntaxKind.EndOfFileToken),
        ts.NodeFlags.None,
    );

    return sourceFile;

    function* emitImports() {
        yield factory.createImportDeclaration(
            undefined,
            factory.createImportClause(
                false,
                undefined,
                factory.createNamespaceImport(factory.createIdentifier("lib")),
            ),
            factory.createStringLiteral("@oas3/oas3ts-lib"),
        );

        // yield factory.createImportDeclaration(
        //     undefined,
        //     factory.createImportClause(
        //         false,
        //         undefined,
        //         factory.createNamedImports([factory.createImportSpecifier(
        //             undefined,
        //             factory.createIdentifier("Router"),
        //         )]),
        //     ),
        //     factory.createStringLiteral("goodrouter"),
        // );

        yield factory.createImportDeclaration(
            undefined,
            factory.createImportClause(
                false,
                undefined,
                factory.createNamespaceImport(factory.createIdentifier("shared")),
            ),
            factory.createStringLiteral("./shared.js"),
        );

    }

}

