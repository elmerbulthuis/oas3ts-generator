import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { createMetadataConstantDeclaration, createSpecificationConstantDeclaration, generateAcceptTypesConstantDeclarations } from "../constants/index.js";
import { generateValidateModelDeclarations, generateValidateRequestParametersDeclarations, generateValidateResponseParametersDeclarations } from "../functions/index.js";
import { PackageConfig } from "../index.js";
import { generateAcceptTypesTypeDeclarations, generateCredentialTypeDeclarations, generateModelTypeDeclarations, generateRequestParametersTypeDeclarations, generateResponseParametersTypeDeclarations } from "../types/index.js";

export function createSharedSourceFile(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    config: PackageConfig,
    nameIndex: Record<string, string>,
) {
    const nodes = [
        ...emitImports(),
        createSpecificationConstantDeclaration(
            factory,
            document,
        ),
        createMetadataConstantDeclaration(
            factory,
            document,
            config,
        ),
        ...generateAcceptTypesConstantDeclarations(
            factory,
            document,
            config,
        ),
        ...generateCredentialTypeDeclarations(
            factory,
            document,
            nameIndex,
        ),
        ...generateAcceptTypesTypeDeclarations(
            factory,
            document,
            config,
        ),
        ...generateModelTypeDeclarations(
            factory,
            document,
            config,
            nameIndex,
        ),
        ...generateValidateModelDeclarations(
            factory,
            document,
            config,
            nameIndex,
        ),

        ...generateRequestParametersTypeDeclarations(
            factory,
            document,
            nameIndex,
        ),
        ...generateResponseParametersTypeDeclarations(
            factory,
            document,
            nameIndex,
        ),
        ...generateValidateRequestParametersDeclarations(
            factory,
            document,
            nameIndex,
        ),
        ...generateValidateResponseParametersDeclarations(
            factory,
            document,
            nameIndex,
        ),

    ];

    const sourceFile = factory.createSourceFile(
        nodes,
        factory.createToken(ts.SyntaxKind.EndOfFileToken),
        ts.NodeFlags.None,
    );

    return sourceFile;

    function* emitImports() {
        yield factory.createImportDeclaration(
            undefined,
            factory.createImportClause(
                false,
                undefined,
                factory.createNamespaceImport(factory.createIdentifier("lib")),
            ),
            factory.createStringLiteral("@oas3/oas3ts-lib"),
        );

    }

}

