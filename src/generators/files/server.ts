import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { PackageConfig } from "../index.js";
import { createServerAuthorizationType, createServerClass, generateAuthorizationHandlerTypeDeclarations, generateOperationAuthorizationTypeDeclarations, generateOperationCredentialsTypeDeclarations, generateOperationHandlerTypeDeclarations, generateRequestTypeDeclarations, generateResponseTypeDeclarations } from "../types/index.js";

export function createServerSourceFile(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    config: PackageConfig,
    nameIndex: Record<string, string>,
) {
    const nodes = [
        ...emitImports(),
        createServerClass(
            factory,
            document,
            config,
            nameIndex,
        ),
        createServerAuthorizationType(
            factory,
            document,
        ),
        ...generateOperationCredentialsTypeDeclarations(
            factory,
            document,
            nameIndex,
        ),
        ...generateOperationAuthorizationTypeDeclarations(
            factory,
            document,
        ),
        ...generateRequestTypeDeclarations(
            factory,
            document,
            config,
            nameIndex,
            "incoming",
        ),
        ...generateResponseTypeDeclarations(
            factory,
            document,
            config,
            nameIndex,
            "outgoing",
        ),
        ...generateOperationHandlerTypeDeclarations(
            factory,
            document,
        ),
        ...generateAuthorizationHandlerTypeDeclarations(
            factory,
            document,
            nameIndex,
        ),

    ];

    const sourceFile = factory.createSourceFile(
        nodes,
        factory.createToken(ts.SyntaxKind.EndOfFileToken),
        ts.NodeFlags.None,
    );

    return sourceFile;

    function* emitImports() {
        yield factory.createImportDeclaration(
            undefined,
            factory.createImportClause(
                false,
                undefined,
                factory.createNamedImports([
                    factory.createImportSpecifier(
                        false,
                        undefined,
                        factory.createIdentifier("Promisable"),
                    ),
                ]),
            ),
            factory.createStringLiteral("type-fest"),
        );

        yield factory.createImportDeclaration(
            undefined,
            factory.createImportClause(
                false,
                undefined,
                factory.createNamedImports([
                    factory.createImportSpecifier(
                        false,
                        undefined,
                        factory.createIdentifier("Route"),
                    ),
                ]),
            ),
            factory.createStringLiteral("goodrouter"),
        );

        yield factory.createImportDeclaration(
            undefined,
            factory.createImportClause(
                false,
                undefined,
                factory.createNamespaceImport(factory.createIdentifier("lib")),
            ),
            factory.createStringLiteral("@oas3/oas3ts-lib"),
        );

        yield factory.createImportDeclaration(
            undefined,
            factory.createImportClause(
                false,
                undefined,
                factory.createNamespaceImport(factory.createIdentifier("shared")),
            ),
            factory.createStringLiteral("./shared.js"),
        );
        yield factory.createImportDeclaration(
            undefined,
            factory.createImportClause(
                false,
                undefined,
                factory.createNamespaceImport(factory.createIdentifier("internal")),
            ),
            factory.createStringLiteral("./server-internal.js"),
        );

    }

}

