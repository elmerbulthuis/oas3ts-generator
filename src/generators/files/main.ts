import ts from "typescript";

export function createMainSourceFile(
    factory: ts.NodeFactory,
) {
    const nodes = [
        ...emitExports(),
    ];

    const sourceFile = factory.createSourceFile(
        nodes,
        factory.createToken(ts.SyntaxKind.EndOfFileToken),
        ts.NodeFlags.None,
    );

    return sourceFile;

    function* emitExports() {
        yield factory.createExportDeclaration(
            undefined,
            false,
            undefined,
            factory.createStringLiteral("@oas3/oas3ts-lib"),
        );

        yield factory.createExportDeclaration(
            undefined,
            false,
            undefined,
            factory.createStringLiteral("./shared.js"),
        );

        yield factory.createExportDeclaration(
            undefined,
            false,
            undefined,
            factory.createStringLiteral("./client.js"),
        );

        yield factory.createExportDeclaration(
            undefined,
            false,
            undefined,
            factory.createStringLiteral("./server.js"),
        );

    }

}

