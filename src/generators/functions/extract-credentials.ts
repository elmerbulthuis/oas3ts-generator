import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllSecuritySchemes } from "../../selectors/security-scheme.js";
import { stringifyIdentifier, toReference } from "../../utils/index.js";

export function* generateExtractCredentialsDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    nameIndex: Record<string, string>,
) {
    for (
        const { securityScheme, securitySchemeObject, pointerParts } of
        selectAllSecuritySchemes(document)
    ) {
        yield createFunctionDeclaration(
            securityScheme,
            securitySchemeObject,
            pointerParts,
        );
    }

    function createFunctionDeclaration(
        name: string,
        securitySchemeObject: OpenAPIV3.SecuritySchemeObject,
        pointerParts: string[],
    ) {
        const credentialTypeReference = toReference(pointerParts);
        const credentialTypeName = nameIndex[credentialTypeReference];
        const functionName = stringifyIdentifier([
            "extract",
            credentialTypeName,
        ]);

        const statements = [...emitStatements(name, securitySchemeObject)];

        return factory.createFunctionDeclaration(
            [
                factory.createToken(ts.SyntaxKind.ExportKeyword),
            ],
            undefined,
            functionName,
            undefined,
            [
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    "parameters",
                    undefined,
                    factory.createTypeReferenceNode(
                        factory.createQualifiedName(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("Parameters"),
                        ),
                    ),
                ),
            ],
            factory.createUnionTypeNode([
                factory.createKeywordTypeNode(ts.SyntaxKind.UndefinedKeyword),
                factory.createTypeReferenceNode(
                    factory.createQualifiedName(
                        factory.createIdentifier("shared"),
                        credentialTypeName,
                    ),
                ),
            ]),
            factory.createBlock(statements, true),
        );

    }

    function* emitStatements(
        parameterName: string,
        securitySchemeObject: OpenAPIV3.SecuritySchemeObject,
    ): Iterable<ts.Statement> {
        switch (securitySchemeObject.type) {
            case "apiKey": {
                yield factory.createVariableStatement(
                    undefined,
                    factory.createVariableDeclarationList([
                        factory.createVariableDeclaration(
                            factory.createIdentifier("parameterValue"),
                            undefined,
                            undefined,
                            factory.createCallExpression(
                                factory.createPropertyAccessExpression(
                                    factory.createIdentifier("lib"),
                                    factory.createIdentifier("getParameterValue"),
                                ),
                                undefined,
                                [
                                    factory.createIdentifier("parameters"),
                                    createdEncodeParameterNameExpression(
                                        securitySchemeObject,
                                        factory.createStringLiteral(securitySchemeObject.name),
                                    ),
                                ],
                            ),
                        ),
                    ], ts.NodeFlags.Const),
                );
                yield factory.createIfStatement(
                    factory.createBinaryExpression(
                        factory.createIdentifier("parameterValue"),
                        factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                        factory.createNull(),
                    ),
                    factory.createReturnStatement(),
                );
                yield factory.createExpressionStatement(
                    factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("clearParameter"),
                        ),
                        undefined,
                        [
                            factory.createIdentifier("parameters"),
                            createdEncodeParameterNameExpression(
                                securitySchemeObject,
                                factory.createStringLiteral(parameterName),
                            ),
                        ],
                    ),
                );
                yield factory.createReturnStatement(
                    createdDecodedParameterValueExpression(
                        securitySchemeObject,
                        factory.createIdentifier("parameterValue"),
                    ),
                );
                break;
            }

            case "http":
                switch (securitySchemeObject.scheme.toLowerCase()) {
                    case "basic":
                        yield factory.createVariableStatement(
                            undefined,
                            factory.createVariableDeclarationList([
                                factory.createVariableDeclaration(
                                    factory.createIdentifier("authorizationHeader"),
                                    undefined,
                                    undefined,
                                    factory.createCallExpression(
                                        factory.createPropertyAccessExpression(
                                            factory.createIdentifier("lib"),
                                            factory.createIdentifier("getParameterValue"),
                                        ),
                                        undefined,
                                        [
                                            factory.createIdentifier("parameters"),
                                            createdEncodeParameterNameExpression(
                                                securitySchemeObject,
                                                factory.createStringLiteral(parameterName),
                                            ),
                                        ],
                                    ),
                                ),
                            ], ts.NodeFlags.Const),
                        );
                        yield factory.createIfStatement(
                            factory.createBinaryExpression(
                                factory.createIdentifier("authorizationHeader"),
                                factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                                factory.createNull(),
                            ),
                            factory.createReturnStatement(),
                        );
                        yield factory.createVariableStatement(
                            undefined,
                            factory.createVariableDeclarationList([
                                factory.createVariableDeclaration(
                                    factory.createIdentifier("parameterValue"),
                                    undefined,
                                    undefined,
                                    factory.createCallExpression(
                                        factory.createPropertyAccessExpression(
                                            factory.createIdentifier("lib"),
                                            factory.createIdentifier("parseBasicAuthorizationHeader"),
                                        ),
                                        undefined,
                                        [factory.createIdentifier("authorizationHeader")],
                                    ),
                                ),
                            ], ts.NodeFlags.Const),
                        );
                        yield factory.createIfStatement(
                            factory.createBinaryExpression(
                                factory.createIdentifier("parameterValue"),
                                factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                                factory.createNull(),
                            ),
                            factory.createReturnStatement(),
                            undefined,
                        );
                        yield factory.createExpressionStatement(
                            factory.createCallExpression(
                                factory.createPropertyAccessExpression(
                                    factory.createIdentifier("lib"),
                                    factory.createIdentifier("clearParameter"),
                                ),
                                undefined,
                                [
                                    factory.createIdentifier("parameters"),
                                    createdEncodeParameterNameExpression(
                                        securitySchemeObject,
                                        factory.createStringLiteral(parameterName),
                                    ),
                                ],
                            ),
                        );
                        yield factory.createReturnStatement(
                            createdDecodedParameterValueExpression(
                                securitySchemeObject,
                                factory.createIdentifier("parameterValue"),
                            ),
                        );
                        break;

                    default:
                        yield factory.createVariableStatement(
                            undefined,
                            factory.createVariableDeclarationList([
                                factory.createVariableDeclaration(
                                    factory.createIdentifier("authorizationHeader"),
                                    undefined,
                                    undefined,
                                    factory.createCallExpression(
                                        factory.createPropertyAccessExpression(
                                            factory.createIdentifier("lib"),
                                            factory.createIdentifier("getParameterValue"),
                                        ),
                                        undefined,
                                        [
                                            factory.createIdentifier("parameters"),
                                            createdEncodeParameterNameExpression(
                                                securitySchemeObject,
                                                factory.createStringLiteral(parameterName),
                                            ),
                                        ],
                                    ),
                                ),
                            ], ts.NodeFlags.Const),
                        );
                        yield factory.createIfStatement(
                            factory.createBinaryExpression(
                                factory.createIdentifier("authorizationHeader"),
                                factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                                factory.createNull(),
                            ),
                            factory.createReturnStatement(),
                        );
                        yield factory.createVariableStatement(
                            undefined,
                            factory.createVariableDeclarationList([
                                factory.createVariableDeclaration(
                                    factory.createIdentifier("parameterValue"),
                                    undefined,
                                    undefined,
                                    factory.createCallExpression(
                                        factory.createPropertyAccessExpression(
                                            factory.createIdentifier("lib"),
                                            factory.createIdentifier("parseAuthorizationHeader"),
                                        ),
                                        undefined,
                                        [
                                            factory.createStringLiteral(
                                                securitySchemeObject.scheme,
                                            ),
                                            factory.createIdentifier("authorizationHeader"),
                                        ],
                                    ),
                                ),
                            ], ts.NodeFlags.Const),
                        );
                        yield factory.createIfStatement(
                            factory.createBinaryExpression(
                                factory.createIdentifier("parameterValue"),
                                factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                                factory.createNull(),
                            ),
                            factory.createReturnStatement(),
                        );
                        yield factory.createExpressionStatement(
                            factory.createCallExpression(
                                factory.createPropertyAccessExpression(
                                    factory.createIdentifier("lib"),
                                    factory.createIdentifier("clearParameter"),
                                ),
                                undefined,
                                [
                                    factory.createIdentifier("parameters"),
                                    createdEncodeParameterNameExpression(
                                        securitySchemeObject,
                                        factory.createStringLiteral(parameterName),
                                    ),
                                ],
                            ),
                        );
                        yield factory.createReturnStatement(
                            createdDecodedParameterValueExpression(
                                securitySchemeObject,
                                factory.createIdentifier("parameterValue"),
                            ),
                        );
                        break;
                }
                break;

            default: throw new Error("cannot happen");
        }
    }

    function createdDecodedParameterValueExpression(
        securitySchemeObject: OpenAPIV3.SecuritySchemeObject,
        parameterValueExpression: ts.Expression,
    ) {
        switch (securitySchemeObject.type) {
            case "apiKey": {
                switch (securitySchemeObject.in) {
                    case "query":
                        return factory.createCallExpression(
                            factory.createIdentifier("decodeURIComponent"),
                            undefined,
                            [
                                parameterValueExpression,
                            ],
                        );

                    default: return parameterValueExpression;
                }
            }

            default: return parameterValueExpression;

        }
    }

    function createdEncodeParameterNameExpression(
        securitySchemeObject: OpenAPIV3.SecuritySchemeObject,
        parameterNameExpression: ts.Expression,
    ) {
        switch (securitySchemeObject.type) {
            case "apiKey":
                switch (securitySchemeObject.in) {
                    case "header":
                        return factory.createCallExpression(
                            factory.createPropertyAccessExpression(
                                parameterNameExpression,
                                "toLowerCase",
                            ),
                            undefined,
                            [],
                        );

                    default: return parameterNameExpression;
                }

            case "http":
                return factory.createStringLiteral("authorization");

            default: return parameterNameExpression;
        }
    }
}
