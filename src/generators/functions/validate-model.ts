import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectModel, selectPropertiesFromSchema, selectSchemaType } from "../../selectors/index.js";
import { generateLiteral, resolveObject, resolvePointerParts, stringifyIdentifier, toReference } from "../../utils/index.js";
import { PackageConfig } from "../index.js";

export function* generateValidateModelDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    config: PackageConfig,
    nameIndex: Record<string, string>,
) {
    for (
        const { schemaObject, pointerParts } of
        selectModel(document, config)
    ) {
        yield createFunctionDeclaration(
            pointerParts,
            schemaObject,
        );
        yield* emitFunctionExtensions(
            pointerParts,
            schemaObject,
        );
    }

    function createFunctionDeclaration(
        schemaPointerParts: Array<string>,
        schemaObject: OpenAPIV3.SchemaObject,
    ) {
        const schemaReference = toReference(schemaPointerParts);
        const schemaTypeName = nameIndex[schemaReference];
        const validateFn = stringifyIdentifier(["validate", schemaTypeName]);

        return factory.createFunctionDeclaration(
            [
                factory.createToken(ts.SyntaxKind.ExportKeyword),
            ],
            factory.createToken(ts.SyntaxKind.AsteriskToken),
            validateFn,
            undefined,
            [
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    "model",
                    undefined,
                    factory.createTypeReferenceNode(schemaTypeName),
                ),
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    "path",
                    undefined,
                    factory.createArrayTypeNode(
                        factory.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
                    ),
                    factory.createArrayLiteralExpression([]),
                ),
            ],
            undefined,
            factory.createBlock([...emitFunctionBody(
                schemaPointerParts,
                schemaObject,
            )], true),
        );

    }

    function* emitFunctionBody(
        schemaPointerParts: Array<string>,
        schemaObject: OpenAPIV3.SchemaObject,
    ) {
        const schemaReference = toReference(schemaPointerParts);
        const schemaTypeName = nameIndex[schemaReference];

        const validators = [...emitValidators(schemaObject)];

        if (validators.length > 0) {
            yield factory.createIfStatement(
                factory.createPrefixUnaryExpression(
                    ts.SyntaxKind.ExclamationToken,
                    validators
                        .reduce((a, b) => factory.createLogicalAnd(a, b)),
                ),
                factory.createBlock([
                    factory.createExpressionStatement(factory.createYieldExpression(
                        undefined,
                        factory.createIdentifier("path"),
                    )),
                    factory.createReturnStatement(),
                ], true),
            );

        }

        for (
            const { property, propertySchemaPointerParts } of
            selectPropertiesFromSchema(document, schemaObject, schemaPointerParts)
        ) {
            const propertySchemaReference = toReference(propertySchemaPointerParts);
            const propertySchemaTypeName = nameIndex[propertySchemaReference];
            const propertyValidateFn = stringifyIdentifier(["validate", propertySchemaTypeName]);

            yield factory.createIfStatement(
                factory.createBinaryExpression(
                    factory.createElementAccessExpression(
                        factory.createIdentifier("model"),
                        factory.createStringLiteral(property),
                    ),
                    factory.createToken(ts.SyntaxKind.ExclamationEqualsToken),
                    factory.createNull(),
                ),
                factory.createBlock([
                    factory.createExpressionStatement(factory.createYieldExpression(
                        factory.createToken(ts.SyntaxKind.AsteriskToken),
                        factory.createCallExpression(
                            factory.createIdentifier(propertyValidateFn),
                            undefined,
                            [
                                factory.createElementAccessExpression(
                                    factory.createIdentifier("model"),
                                    factory.createStringLiteral(property),
                                ),
                                factory.createArrayLiteralExpression([
                                    factory.createSpreadElement(factory.createIdentifier("path")),
                                    factory.createStringLiteral(property),
                                ]),
                            ],
                        ),
                    )),
                ], true),
            );
        }

        if (schemaObject.oneOf) {
            // only one should be valid! -> https://swagger.io/docs/specification/data-models/oneof-anyof-allof-not/
            if (schemaObject.discriminator) {
                // we got a discriminator! this is great wel can use it to
                // determinte the validator to use!
                const caseClauses = new Array<ts.CaseClause>();

                for (const [sub, maybeSubSchemaObject] of Object.entries(schemaObject.oneOf)) {
                    const subSchemaPointerParts = resolvePointerParts(
                        [...schemaPointerParts, "oneOf", sub],
                        maybeSubSchemaObject,
                    );
                    assert(subSchemaPointerParts);

                    const subSchemaObject = resolveObject(document, maybeSubSchemaObject);
                    assert(subSchemaObject);

                    const subSchemaReference = toReference(subSchemaPointerParts);
                    const subSchemaTypeName = nameIndex[subSchemaReference];
                    const subValidateFn = stringifyIdentifier(["validate", subSchemaTypeName]);

                    const discriminatorSchemePointerParts = resolvePointerParts(
                        [...subSchemaPointerParts, "properties", schemaObject.discriminator.propertyName],
                        maybeSubSchemaObject,
                    );
                    assert(discriminatorSchemePointerParts);

                    const discriminatorSchemaObject = resolveObject(
                        document,
                        subSchemaObject.properties?.[schemaObject.discriminator.propertyName],
                    );
                    assert(discriminatorSchemaObject);

                    const enumValues = [...discriminatorSchemaObject?.enum ?? []];
                    const lastEnumValue = enumValues.pop();
                    if (lastEnumValue != null) {
                        for (const enumValue of enumValues) {
                            caseClauses.push(factory.createCaseClause(
                                generateLiteral(factory, enumValue),
                                [],
                            ));
                        }
                        caseClauses.push(factory.createCaseClause(
                            generateLiteral(factory, lastEnumValue),
                            [
                                factory.createExpressionStatement(factory.createYieldExpression(
                                    factory.createToken(ts.SyntaxKind.AsteriskToken),
                                    factory.createCallExpression(
                                        factory.createIdentifier(subValidateFn),
                                        undefined,
                                        [
                                            factory.createIdentifier("model"),
                                            factory.createIdentifier("path"),
                                        ],
                                    ),
                                )),
                                factory.createBreakStatement(),
                            ],
                        ));
                    }

                }

                yield factory.createSwitchStatement(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("model"),
                        schemaObject.discriminator.propertyName,
                    ),
                    factory.createCaseBlock([
                        ...caseClauses,
                        factory.createDefaultClause([
                            factory.createExpressionStatement(factory.createYieldExpression(
                                undefined,
                                factory.createIdentifier("path"),
                            )),
                            factory.createReturnStatement(),
                        ]),
                    ]),
                );
            }
            else {
                // we got no discriminator! we should run all validators
                // and only one should validate successfull

                yield factory.createVariableStatement(
                    undefined,
                    factory.createVariableDeclarationList([
                        factory.createVariableDeclaration(
                            factory.createIdentifier("validCount"),
                            undefined,
                            undefined,
                            factory.createNumericLiteral(0),
                        ),
                    ], ts.NodeFlags.Let),
                );

                for (const [sub, maybeSubSchemaObject] of Object.entries(schemaObject.oneOf)) {
                    const subSchemaPointerParts = resolvePointerParts(
                        [...schemaPointerParts, "oneOf", sub],
                        maybeSubSchemaObject,
                    );
                    assert(subSchemaPointerParts);

                    const subSchemaObject = resolveObject(document, maybeSubSchemaObject);
                    assert(subSchemaObject);

                    const subSchemaReference = toReference(subSchemaPointerParts);
                    const subSchemaTypeName = nameIndex[subSchemaReference];
                    const subValidateFn = stringifyIdentifier(["validate", subSchemaTypeName]);

                    yield factory.createBlock([
                        factory.createVariableStatement(
                            undefined,
                            factory.createVariableDeclarationList([
                                factory.createVariableDeclaration(
                                    factory.createIdentifier("invalidPaths"),
                                    undefined,
                                    undefined,
                                    factory.createArrayLiteralExpression([
                                        factory.createSpreadElement(factory.createCallExpression(
                                            factory.createIdentifier(subValidateFn),
                                            undefined,
                                            [factory.createAsExpression(
                                                factory.createIdentifier("model"),
                                                factory.createTypeReferenceNode(
                                                    factory.createIdentifier(subSchemaTypeName),
                                                ),
                                            )],
                                        )),
                                    ], false),
                                ),
                            ], ts.NodeFlags.Const),
                        ),
                        factory.createIfStatement(
                            factory.createBinaryExpression(
                                factory.createPropertyAccessExpression(
                                    factory.createIdentifier("invalidPaths"),
                                    factory.createIdentifier("length"),
                                ),
                                factory.createToken(ts.SyntaxKind.GreaterThanToken),
                                factory.createNumericLiteral(0),
                            ),
                            factory.createBlock([
                                factory.createExpressionStatement(
                                    factory.createPostfixUnaryExpression(
                                        factory.createIdentifier("validCount"),
                                        ts.SyntaxKind.PlusPlusToken,
                                    ),
                                ),
                            ], true),
                        ),
                    ], true);
                }

                yield factory.createIfStatement(
                    factory.createBinaryExpression(
                        factory.createIdentifier("validCount"),
                        factory.createToken(ts.SyntaxKind.ExclamationEqualsEqualsToken),
                        factory.createNumericLiteral(1),
                    ),
                    factory.createBlock([
                        factory.createExpressionStatement(factory.createYieldExpression(
                            undefined,
                            factory.createIdentifier("path"),
                        )),
                        factory.createReturnStatement(),
                    ], true),
                );

            }
        }

        if (schemaObject.allOf) {
            for (const [sub, maybeSubSchemaObject] of Object.entries(schemaObject.allOf)) {
                const subSchemaPointerParts = resolvePointerParts(
                    [...schemaPointerParts, "allOf", sub],
                    maybeSubSchemaObject,
                );
                assert(subSchemaPointerParts);

                const subSchemaReference = toReference(subSchemaPointerParts);
                const subSchemaTypeName = nameIndex[subSchemaReference];
                const subValidateFn = stringifyIdentifier(["validate", subSchemaTypeName]);

                yield factory.createExpressionStatement(factory.createYieldExpression(
                    factory.createToken(ts.SyntaxKind.AsteriskToken),
                    factory.createCallExpression(
                        factory.createIdentifier(subValidateFn),
                        undefined,
                        [
                            factory.createIdentifier("model"),
                            factory.createIdentifier("path"),
                        ],
                    ),
                ));
            }
        }

        if ("items" in schemaObject) {
            if (Array.isArray(schemaObject.items)) {
                for (const [sub, maybeSubSchemaObject] of Object.entries(schemaObject.items)) {
                    const subSchemaPointerParts = resolvePointerParts(
                        [...schemaPointerParts, "items", sub],
                        maybeSubSchemaObject,
                    );
                    assert(subSchemaPointerParts);

                    const subSchemaReference = toReference(subSchemaPointerParts);
                    const subSchemaTypeName = nameIndex[subSchemaReference];
                    const subValidateFn = stringifyIdentifier(["validate", subSchemaTypeName]);

                    yield factory.createExpressionStatement(factory.createYieldExpression(
                        factory.createToken(ts.SyntaxKind.AsteriskToken),
                        factory.createCallExpression(
                            factory.createIdentifier(subValidateFn),
                            undefined,
                            [
                                factory.createElementAccessExpression(
                                    factory.createIdentifier("model"),
                                    factory.createIdentifier(sub),
                                ),
                                factory.createIdentifier("path"),
                            ],
                        ),
                    ));
                }
            }
            else {
                const itemsSchemePointerParts = resolvePointerParts(
                    [...schemaPointerParts, "items"],
                    schemaObject.items,
                );
                assert(itemsSchemePointerParts);

                const itemsSchemaReference = toReference(itemsSchemePointerParts);
                const itemsSchemaTypeName = nameIndex[itemsSchemaReference];
                const itemsValidateFn = stringifyIdentifier(["validate", itemsSchemaTypeName]);

                yield factory.createForOfStatement(
                    undefined,
                    factory.createVariableDeclarationList([
                        factory.createVariableDeclaration(
                            factory.createArrayBindingPattern([
                                factory.createBindingElement(
                                    undefined,
                                    undefined,
                                    factory.createIdentifier("key"),
                                    undefined,
                                ),
                                factory.createBindingElement(
                                    undefined,
                                    undefined,
                                    factory.createIdentifier("value"),
                                    undefined,
                                ),
                            ]),
                            undefined,
                            undefined,
                            undefined,
                        ),
                    ], ts.NodeFlags.Const),
                    factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("Object"),
                            factory.createIdentifier("entries"),
                        ),
                        undefined,
                        [factory.createIdentifier("model")],
                    ),
                    factory.createBlock([
                        factory.createExpressionStatement(factory.createYieldExpression(
                            factory.createToken(ts.SyntaxKind.AsteriskToken),
                            factory.createCallExpression(
                                factory.createIdentifier(itemsValidateFn),
                                undefined,
                                [
                                    factory.createIdentifier("value"),
                                    factory.createArrayLiteralExpression([
                                        factory.createSpreadElement(factory.createIdentifier("path")),
                                        factory.createIdentifier("key"),
                                    ]),
                                ],
                            ),
                        )),
                    ], true),
                );

            }
        }

        if (typeof schemaObject.additionalProperties === "object") {
            const subSchemaPointerParts = resolvePointerParts(
                [...schemaPointerParts, "additionalProperties"],
                schemaObject.additionalProperties,
            );
            assert(subSchemaPointerParts);

            const subSchemaReference = toReference(subSchemaPointerParts);
            const subSchemaTypeName = nameIndex[subSchemaReference];
            const subValidateFn = stringifyIdentifier(["validate", subSchemaTypeName]);

            yield factory.createForOfStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createArrayBindingPattern([
                            factory.createBindingElement(
                                undefined,
                                undefined,
                                factory.createIdentifier("key"),
                                undefined,
                            ),
                            factory.createBindingElement(
                                undefined,
                                undefined,
                                factory.createIdentifier("value"),
                                undefined,
                            ),
                        ]),
                        undefined,
                        undefined,
                        undefined,
                    ),
                ], ts.NodeFlags.Const),
                factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("Object"),
                        factory.createIdentifier("entries"),
                    ),
                    undefined,
                    [factory.createIdentifier("model")],
                ),
                factory.createBlock([
                    factory.createExpressionStatement(factory.createYieldExpression(
                        factory.createToken(ts.SyntaxKind.AsteriskToken),
                        factory.createCallExpression(
                            factory.createIdentifier(subValidateFn),
                            undefined,
                            [
                                factory.createIdentifier("value"),
                                factory.createArrayLiteralExpression([
                                    factory.createSpreadElement(factory.createIdentifier("path")),
                                    factory.createIdentifier("key"),
                                ]),
                            ],
                        ),
                    )),
                ], true),
            );
        }
    }

    function* emitFunctionExtensions(
        schemaPointerParts: Array<string>,
        schemaObject: OpenAPIV3.SchemaObject,
    ) {
        const schemaReference = toReference(schemaPointerParts);
        const schemaTypeName = nameIndex[schemaReference];
        const validateFn = stringifyIdentifier(["validate", schemaTypeName]);

        if (schemaObject.properties != null) {
            const properties = new Array<ts.PropertyAssignment>();
            for (
                const { property, propertySchemaPointerParts } of
                selectPropertiesFromSchema(document, schemaObject, schemaPointerParts)
            ) {
                const propertySchemaReference = toReference(propertySchemaPointerParts);
                const propertySchemaTypeName = nameIndex[propertySchemaReference];
                const propertyValidateFn = stringifyIdentifier(["validate", propertySchemaTypeName]);

                properties.push(factory.createPropertyAssignment(
                    factory.createStringLiteral(property),
                    factory.createIdentifier(propertyValidateFn),
                ));
            }
            yield factory.createExpressionStatement(factory.createBinaryExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier(validateFn),
                    factory.createIdentifier("properties"),
                ),
                factory.createToken(ts.SyntaxKind.EqualsToken),
                factory.createObjectLiteralExpression(properties, true),
            ));
        }

        if (schemaObject.oneOf) {
            const elements = new Array<ts.Identifier>();

            for (const [sub, maybeSubSchemaObject] of Object.entries(schemaObject.oneOf)) {
                const subSchemaPointerParts = resolvePointerParts(
                    [...schemaPointerParts, "oneOf", sub],
                    maybeSubSchemaObject,
                );
                assert(subSchemaPointerParts);

                const subSchemaObject = resolveObject(document, maybeSubSchemaObject);
                assert(subSchemaObject);

                const subSchemaReference = toReference(subSchemaPointerParts);
                const subSchemaTypeName = nameIndex[subSchemaReference];
                const subValidateFn = stringifyIdentifier(["validate", subSchemaTypeName]);

                elements.push(
                    factory.createIdentifier(subValidateFn),
                );
            }

            yield factory.createExpressionStatement(factory.createBinaryExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier(validateFn),
                    factory.createIdentifier("oneOf"),
                ),
                factory.createToken(ts.SyntaxKind.EqualsToken),
                factory.createArrayLiteralExpression(elements, true),
            ));
        }

        if (schemaObject.allOf) {
            const elements = new Array<ts.Identifier>();

            for (const [sub, maybeSubSchemaObject] of Object.entries(schemaObject.allOf)) {
                const subSchemaPointerParts = resolvePointerParts(
                    [...schemaPointerParts, "allOf", sub],
                    maybeSubSchemaObject,
                );
                assert(subSchemaPointerParts);

                const subSchemaReference = toReference(subSchemaPointerParts);
                const subSchemaTypeName = nameIndex[subSchemaReference];
                const subValidateFn = stringifyIdentifier(["validate", subSchemaTypeName]);

                elements.push(
                    factory.createIdentifier(subValidateFn),
                );
            }

            yield factory.createExpressionStatement(factory.createBinaryExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier(validateFn),
                    factory.createIdentifier("allOf"),
                ),
                factory.createToken(ts.SyntaxKind.EqualsToken),
                factory.createArrayLiteralExpression(elements, true),
            ));
        }

        if ("items" in schemaObject) {
            const elements = new Array<ts.Identifier>();

            if (Array.isArray(schemaObject.items)) {
                for (const [sub, maybeSubSchemaObject] of Object.entries(schemaObject.items)) {
                    const subSchemaPointerParts = resolvePointerParts(
                        [...schemaPointerParts, "items", sub],
                        maybeSubSchemaObject,
                    );
                    assert(subSchemaPointerParts);

                    const subSchemaReference = toReference(subSchemaPointerParts);
                    const subSchemaTypeName = nameIndex[subSchemaReference];
                    const subValidateFn = stringifyIdentifier(["validate", subSchemaTypeName]);

                    elements.push(
                        factory.createIdentifier(subValidateFn),
                    );
                }

                yield factory.createExpressionStatement(factory.createBinaryExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier(validateFn),
                        factory.createIdentifier("items"),
                    ),
                    factory.createToken(ts.SyntaxKind.EqualsToken),
                    factory.createArrayLiteralExpression(elements, true),
                ));
            }
            else {

                const itemsSchemePointerParts = resolvePointerParts(
                    [...schemaPointerParts, "items"],
                    schemaObject.items,
                );
                assert(itemsSchemePointerParts);

                const itemsSchemaReference = toReference(itemsSchemePointerParts);
                const itemsSchemaTypeName = nameIndex[itemsSchemaReference];
                const itemsValidateFn = stringifyIdentifier(["validate", itemsSchemaTypeName]);

                yield factory.createExpressionStatement(factory.createBinaryExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier(validateFn),
                        factory.createIdentifier("items"),
                    ),
                    factory.createToken(ts.SyntaxKind.EqualsToken),
                    factory.createIdentifier(itemsValidateFn),
                ));
            }
        }

        if (typeof schemaObject.additionalProperties === "object") {
            const subSchemaPointerParts = resolvePointerParts(
                [...schemaPointerParts, "additionalProperties"],
                schemaObject.additionalProperties,
            );
            assert(subSchemaPointerParts);

            const subSchemaReference = toReference(subSchemaPointerParts);
            const subSchemaTypeName = nameIndex[subSchemaReference];
            const subValidateFn = stringifyIdentifier(["validate", subSchemaTypeName]);

            yield factory.createExpressionStatement(factory.createBinaryExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier(validateFn),
                    factory.createIdentifier("additionalProperties"),
                ),
                factory.createToken(ts.SyntaxKind.EqualsToken),
                factory.createIdentifier(subValidateFn),
            ));
        }

    }

    function* emitValidators(
        schemaObject: OpenAPIV3.SchemaObject,
    ): Iterable<ts.Expression> {
        yield* emitCallValidator("validateType", schemaObject.type);
        yield* emitCallValidator("validateEnum", schemaObject.enum);
        // yield* createValidatorCall("validateConst", schemaObject.const);

        switch (selectSchemaType(schemaObject)) {
            case "object":
                yield* emitCallValidator("validateMaxProperties", schemaObject.maxProperties);
                yield* emitCallValidator("validateMinProperties", schemaObject.minProperties);

                yield* emitCallValidator("validateRequired", schemaObject.required);
                // yield* emitCallValidator("validateDependentRequired", schemaObject.dependentRequired);
                break;

            case "array": {
                yield* emitCallValidator("validateMaxItems", schemaObject.maxItems);
                yield* emitCallValidator("validateMinItems", schemaObject.minItems);
                yield* emitCallValidator("validateUniqueItems", schemaObject.uniqueItems);
                // yield* emitCallValidator("validateMaxContains", schemaObject.maxContains);
                // yield* emitCallValidator("validateMinContains", schemaObject.minContains);
                break;
            }

            case "string":
                yield* emitCallValidator("validateMaxLength", schemaObject.maxLength);
                yield* emitCallValidator("validateMinLength", schemaObject.minLength);
                yield* emitCallValidator("validatePattern", schemaObject.pattern);
                break;

            case "number":
            case "integer":
                yield* emitCallValidator("validateMultipleOf", schemaObject.multipleOf);
                yield* emitCallValidator("validateMaximum", schemaObject.maximum);
                yield* emitCallValidator("validateExclusiveMaximum", schemaObject.exclusiveMaximum);
                yield* emitCallValidator("validateMinimum", schemaObject.minimum);
                yield* emitCallValidator("validateExclusiveMinimum", schemaObject.exclusiveMinimum);
                break;

            case "boolean":
                break;

            default:
                break;

        }

    }

    function* emitCallValidator(
        validatorName: string,
        validateArgument: unknown,
    ) {
        if (validateArgument == null) return;

        yield factory.createCallExpression(
            factory.createPropertyAccessExpression(
                factory.createIdentifier("lib"),
                validatorName,
            ),
            undefined,
            [
                factory.createIdentifier("model"),
                generateLiteral(factory, validateArgument),
            ],
        );
    }

}

