import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { ParameterIn, selectAllRequestParameters, selectAllResponseParameters, selectParameterProperties, selectPropertiesFromSchema, selectSchemaType } from "../../selectors/index.js";
import { resolveObject, resolvePointerParts, stringifyIdentifier, toReference } from "../../utils/index.js";

export function* generateInjectParametersDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    nameIndex: Record<string, string>,
    type: "request" | "response",
) {
    if (type === "request") for (
        const { parameterObject, pointerParts } of
        selectAllRequestParameters(document)
    ) {
        yield createFunctionDeclaration(
            parameterObject.name,
            parameterObject.in as ParameterIn,
            parameterObject,
            pointerParts,
        );
    }

    if (type === "response") for (
        const { header, headerObject, pointerParts } of
        selectAllResponseParameters(document)
    ) {
        yield createFunctionDeclaration(
            header,
            "header",
            headerObject,
            pointerParts,
        );
    }

    function createFunctionDeclaration(
        parameterName: string,
        parameterIn: ParameterIn,
        parameterObject: OpenAPIV3.ParameterBaseObject,
        parameterPointerParts: string[],
    ) {
        assert(parameterObject.allowReserved !== true, "allowReserved not supported");

        const parameterReference = toReference(parameterPointerParts);
        const parameterTypeName = nameIndex[parameterReference];
        const functionName = stringifyIdentifier([
            "inject",
            parameterTypeName,
        ]);

        const schemaObject = resolveObject(
            document,
            parameterObject.schema,
        );
        assert(schemaObject);

        const schemaPointerParts = resolvePointerParts(
            [...parameterPointerParts, "schema"],
            parameterObject.schema,
        );
        assert(schemaPointerParts);

        const typeReference = toReference(schemaPointerParts);
        const typeName = nameIndex[typeReference];

        const statements = [...emitStatements(
            parameterName,
            parameterIn,
            parameterObject,
            schemaObject,
            schemaPointerParts,
        )];

        return factory.createFunctionDeclaration(
            [
                factory.createToken(ts.SyntaxKind.ExportKeyword),
            ],
            undefined,
            functionName,
            undefined,
            [
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    "parameters",
                    undefined,
                    factory.createTypeReferenceNode(
                        factory.createQualifiedName(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("Parameters"),
                        ),
                    ),
                ),
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    "value",
                    undefined,
                    factory.createUnionTypeNode([
                        factory.createKeywordTypeNode(ts.SyntaxKind.UndefinedKeyword),
                        factory.createTypeReferenceNode(
                            factory.createQualifiedName(
                                factory.createIdentifier("shared"),
                                factory.createIdentifier(typeName),
                            ),
                        ),
                    ]),
                ),
            ],
            undefined,
            factory.createBlock(statements, true),
        );

    }

    function* emitStatements(
        parameterName: string,
        parameterIn: ParameterIn,
        parameterObject: OpenAPIV3.ParameterBaseObject,
        schemaObject: OpenAPIV3.SchemaObject,
        schemaPointerParts: string[],
    ) {
        const {
            prefix, assignment, glue, deepObject,
        } = selectParameterProperties(parameterIn, parameterObject);

        yield factory.createIfStatement(
            factory.createBinaryExpression(
                factory.createIdentifier("value"),
                factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                factory.createNull(),
            ),
            factory.createReturnStatement(),
        );

        const schemaType = selectSchemaType(schemaObject);
        switch (schemaType) {
            case "string":
            case "number":
            case "integer":
            case "boolean": {
                if (deepObject) throw new Error("unsupported type");

                yield* emitPrimitiveStatements(
                    parameterName,
                    parameterIn,
                    schemaType,
                    prefix,
                );
                break;
            }

            case "array":
                if (deepObject) throw new Error("unsupported type");

                assert("items" in schemaObject);

                yield* emitArrayStatements(
                    parameterName,
                    parameterIn,
                    schemaObject,
                    schemaPointerParts,
                    prefix,
                    glue,
                );
                break;

            case "object":
                if (schemaObject.properties != null) {
                    yield* emitObjectStatements(
                        parameterName,
                        parameterIn,
                        schemaObject,
                        schemaPointerParts,
                        prefix,
                        glue,
                        assignment,
                        deepObject,
                    );
                }
                else if (
                    schemaObject.additionalProperties != null &&
                    schemaObject.additionalProperties !== false
                ) {
                    yield* emitDictionaryStatements(
                        parameterName,
                        parameterIn,
                        schemaObject,
                        schemaPointerParts,
                        prefix,
                        glue,
                        assignment,
                        deepObject,
                    );
                }
                else {
                    throw new Error("not supported");
                }
                break;

            default:
                throw new Error(`unexpected schema type ${schemaType}`);
        }
    }

    function* emitPrimitiveStatements(
        parameterName: string,
        parameterIn: ParameterIn,
        parameterType: "boolean" | "number" | "string" | "integer",
        prefix: string | null,
    ) {
        const parameterValueExpression = createdEncodedParameterValueExpression(
            parameterIn,
            parameterType,
            factory.createIdentifier("value"),
        );

        yield factory.createExpressionStatement(factory.createCallExpression(
            factory.createPropertyAccessExpression(
                factory.createIdentifier("lib"),
                factory.createIdentifier("addParameter"),
            ),
            undefined,
            [
                factory.createIdentifier("parameters"),
                createdEncodeParameterNameExpression(
                    parameterIn,
                    factory.createStringLiteral(parameterName),
                ),
                prefix == null ?
                    parameterValueExpression :
                    factory.createBinaryExpression(
                        factory.createStringLiteral(prefix),
                        factory.createToken(ts.SyntaxKind.PlusToken),
                        parameterValueExpression,
                    ),
            ],
        ));
    }

    function* emitArrayStatements(
        parameterName: string,
        parameterIn: ParameterIn,
        schemaObject: OpenAPIV3.ArraySchemaObject,
        schemaPointerParts: string[],
        prefix: string | null,
        glue: string | null,
    ) {
        const itemsSchemaObject = resolveObject(document, schemaObject.items);
        assert(itemsSchemaObject);

        const itemsSchemaPointerParts = resolvePointerParts(
            [...schemaPointerParts, "items"],
            schemaObject.items,
        );
        assert(itemsSchemaPointerParts);

        if (glue == null) {
            yield factory.createForOfStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createIdentifier("valueElement"),
                    ),
                ], ts.NodeFlags.Const),
                factory.createIdentifier("value"),
                factory.createBlock([
                    factory.createExpressionStatement(factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("addParameter"),
                        ),
                        undefined,
                        [
                            factory.createIdentifier("parameters"),
                            createdEncodeParameterNameExpression(
                                parameterIn,
                                factory.createStringLiteral(parameterName),
                            ),
                            createdEncodedParameterValueExpression(
                                parameterIn,
                                selectSchemaType(itemsSchemaObject),
                                factory.createIdentifier("valueElement"),
                            ),
                        ],
                    )),
                ], true),
            );
        }
        else {
            const parameterValueExpression = factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("value"),
                            factory.createIdentifier("map"),
                        ),
                        undefined,
                        [factory.createArrowFunction(
                            undefined,
                            undefined,
                            [factory.createParameterDeclaration(
                                undefined,
                                undefined,
                                factory.createIdentifier("elementValue"),
                            )],
                            undefined,
                            factory.createToken(ts.SyntaxKind.EqualsGreaterThanToken),
                            createdEncodedParameterValueExpression(
                                parameterIn,
                                selectSchemaType(itemsSchemaObject),
                                factory.createIdentifier("elementValue"),
                            ),
                        )],
                    ), "join"),
                undefined,
                [factory.createStringLiteral(glue)],
            );
            yield factory.createExpressionStatement(factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("lib"),
                    factory.createIdentifier("addParameter"),
                ),
                undefined,
                [
                    factory.createIdentifier("parameters"),
                    createdEncodeParameterNameExpression(
                        parameterIn,
                        factory.createStringLiteral(parameterName),
                    ),
                    prefix == null ?
                        parameterValueExpression :
                        factory.createBinaryExpression(
                            factory.createStringLiteral(prefix),
                            factory.createToken(ts.SyntaxKind.PlusToken),
                            parameterValueExpression,
                        ),
                ],
            ));
        }
    }

    function* emitObjectStatements(
        parameterName: string,
        parameterIn: ParameterIn,
        schemaObject: OpenAPIV3.SchemaObject,
        schemaPointerParts: string[],
        prefix: string | null,
        glue: string | null,
        assignment: string | null,
        deepObject: boolean,
    ) {
        if (deepObject) {
            for (
                const { property, propertySchemaObject } of
                selectPropertiesFromSchema(
                    document,
                    schemaObject,
                    schemaPointerParts,
                )
            ) {
                const propertyType = selectSchemaType(propertySchemaObject);

                yield factory.createBlock([
                    factory.createVariableStatement(
                        undefined,
                        factory.createVariableDeclarationList([
                            factory.createVariableDeclaration(
                                factory.createIdentifier("propertyValue"),
                                undefined,
                                undefined,
                                factory.createElementAccessExpression(
                                    factory.createIdentifier("value"),
                                    factory.createStringLiteral(property),
                                ),
                            ),
                        ], ts.NodeFlags.Const),
                    ),
                    factory.createIfStatement(
                        factory.createBinaryExpression(
                            factory.createIdentifier("propertyValue"),
                            factory.createToken(ts.SyntaxKind.ExclamationEqualsToken),
                            factory.createNull(),
                        ),
                        factory.createBlock([
                            factory.createVariableStatement(
                                undefined,
                                factory.createVariableDeclarationList(
                                    [factory.createVariableDeclaration(
                                        factory.createIdentifier("parameterName"),
                                        undefined,
                                        undefined,
                                        factory.createCallExpression(
                                            factory.createPropertyAccessExpression(
                                                factory.createIdentifier("lib"),
                                                factory.createIdentifier("stringifyDeepObjectPropertyName"),
                                            ),
                                            undefined,
                                            [
                                                factory.createStringLiteral(parameterName),
                                                factory.createStringLiteral(property),
                                            ],
                                        ),
                                    )],
                                    ts.NodeFlags.Const,
                                ),
                            ),
                            factory.createExpressionStatement(factory.createCallExpression(
                                factory.createPropertyAccessExpression(
                                    factory.createIdentifier("lib"),
                                    factory.createIdentifier("addParameter"),
                                ),
                                undefined,
                                [
                                    factory.createIdentifier("parameters"),
                                    createdEncodeParameterNameExpression(
                                        parameterIn,
                                        factory.createIdentifier("parameterName"),
                                    ),
                                    createdEncodedParameterValueExpression(
                                        parameterIn,
                                        propertyType,
                                        factory.createIdentifier("propertyValue"),
                                    ),
                                ],
                            )),
                        ], true),
                    ),

                ], true);
            }
        }
        else if (glue == null || assignment == null) {
            for (
                const { property, propertySchemaObject } of
                selectPropertiesFromSchema(
                    document,
                    schemaObject,
                    schemaPointerParts,
                )
            ) {
                const propertyType = selectSchemaType(propertySchemaObject);

                yield factory.createBlock([
                    factory.createVariableStatement(
                        undefined,
                        factory.createVariableDeclarationList([
                            factory.createVariableDeclaration(
                                factory.createIdentifier("propertyValue"),
                                undefined,
                                undefined,
                                factory.createElementAccessExpression(
                                    factory.createIdentifier("value"),
                                    factory.createStringLiteral(property),
                                ),
                            ),
                        ], ts.NodeFlags.Const),
                    ),
                    factory.createIfStatement(
                        factory.createBinaryExpression(
                            factory.createIdentifier("propertyValue"),
                            factory.createToken(ts.SyntaxKind.ExclamationEqualsToken),
                            factory.createNull(),
                        ),
                        factory.createBlock([
                            factory.createExpressionStatement(factory.createCallExpression(
                                factory.createPropertyAccessExpression(
                                    factory.createIdentifier("lib"),
                                    factory.createIdentifier("addParameter"),
                                ),
                                undefined,
                                [
                                    factory.createIdentifier("parameters"),
                                    createdEncodeParameterNameExpression(
                                        parameterIn,
                                        factory.createStringLiteral(property),
                                    ),
                                    createdEncodedParameterValueExpression(
                                        parameterIn,
                                        propertyType,
                                        factory.createIdentifier("propertyValue"),
                                    ),
                                ],
                            )),
                        ], true),
                    ),
                ], true);
            }
        }
        else {
            yield factory.createVariableStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createIdentifier("parameterEntries"),
                        undefined,
                        undefined,
                        factory.createNewExpression(
                            factory.createIdentifier("Array"),
                            [factory.createTupleTypeNode([
                                factory.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
                                factory.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
                            ])],
                            [],
                        ),
                    ),
                ], ts.NodeFlags.Const),
            );

            for (
                const { property, propertySchemaObject } of
                selectPropertiesFromSchema(
                    document,
                    schemaObject,
                    schemaPointerParts,
                )
            ) {
                const propertyType = selectSchemaType(propertySchemaObject);

                yield factory.createBlock([
                    factory.createVariableStatement(
                        undefined,
                        factory.createVariableDeclarationList([
                            factory.createVariableDeclaration(
                                factory.createIdentifier("propertyValue"),
                                undefined,
                                undefined,
                                factory.createElementAccessExpression(
                                    factory.createIdentifier("value"),
                                    factory.createStringLiteral(property),
                                ),
                            ),
                        ], ts.NodeFlags.Const),
                    ),
                    factory.createIfStatement(
                        factory.createBinaryExpression(
                            factory.createIdentifier("propertyValue"),
                            factory.createToken(ts.SyntaxKind.ExclamationEqualsToken),
                            factory.createNull(),
                        ),
                        factory.createBlock([
                            factory.createExpressionStatement(factory.createCallExpression(
                                factory.createPropertyAccessExpression(
                                    factory.createIdentifier("parameterEntries"),
                                    factory.createIdentifier("push"),
                                ),
                                undefined,
                                [factory.createArrayLiteralExpression(
                                    [
                                        factory.createStringLiteral(property),
                                        createdEncodedParameterValueExpression(
                                            parameterIn,
                                            propertyType,
                                            factory.createIdentifier("propertyValue"),
                                        ),
                                    ],
                                    false,
                                )],
                            )),
                        ], true),
                    ),

                ], true);
            }

            yield factory.createVariableStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createIdentifier("parameterValue"),
                        undefined,
                        undefined,
                        factory.createCallExpression(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("lib"),
                                factory.createIdentifier("stringifyParameterEntries"),
                            ),
                            undefined,
                            [
                                factory.createIdentifier("parameterEntries"),
                                factory.createStringLiteral(prefix ?? ""),
                                factory.createStringLiteral(glue),
                                factory.createStringLiteral(assignment),
                            ],
                        ),
                    ),
                ], ts.NodeFlags.Const),
            );
            yield factory.createExpressionStatement(factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("lib"),
                    factory.createIdentifier("addParameter"),
                ),
                undefined,
                [
                    factory.createIdentifier("parameters"),
                    createdEncodeParameterNameExpression(
                        parameterIn,
                        factory.createStringLiteral(parameterName),
                    ),
                    factory.createIdentifier("parameterValue"),
                ],
            ));
        }
    }

    function* emitDictionaryStatements(
        parameterName: string,
        parameterIn: ParameterIn,
        schemaObject: OpenAPIV3.SchemaObject,
        schemaPointerParts: string[],
        prefix: string | null,
        glue: string | null,
        assignment: string | null,
        deepObject: boolean,
    ) {
        assert(
            schemaObject.additionalProperties != null &&
            schemaObject.additionalProperties !== false,
        );
        assert(schemaObject.additionalProperties !== true);

        const additionalPropertiesSchemaObject = resolveObject(
            document,
            schemaObject.additionalProperties,
        );
        assert(additionalPropertiesSchemaObject);

        const additionalPropertiesSchemaObjectPointerParts = resolvePointerParts(
            [...schemaPointerParts, "additionalProperties"],
            schemaObject.additionalProperties,
        );
        assert(additionalPropertiesSchemaObjectPointerParts);

        const additionalPropertiesType = selectSchemaType(additionalPropertiesSchemaObject);

        if (deepObject) {
            yield factory.createForOfStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createArrayBindingPattern([
                            factory.createBindingElement(
                                undefined,
                                undefined,
                                factory.createIdentifier("propertyName"),
                                undefined,
                            ),
                            factory.createBindingElement(
                                undefined,
                                undefined,
                                factory.createIdentifier("propertyValue"),
                                undefined,
                            ),
                        ]),
                    ),
                ], ts.NodeFlags.Const),
                factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("Object"),
                        factory.createIdentifier("entries"),
                    ),
                    undefined,
                    [factory.createIdentifier("value")],
                ),
                factory.createBlock([
                    factory.createIfStatement(
                        factory.createBinaryExpression(
                            factory.createIdentifier("propertyValue"),
                            factory.createToken(ts.SyntaxKind.ExclamationEqualsToken),
                            factory.createNull(),
                        ),
                        factory.createBlock([
                            factory.createVariableStatement(
                                undefined,
                                factory.createVariableDeclarationList([
                                    factory.createVariableDeclaration(
                                        factory.createIdentifier("parameterName"),
                                        undefined,
                                        undefined,
                                        factory.createCallExpression(
                                            factory.createPropertyAccessExpression(
                                                factory.createIdentifier("lib"),
                                                factory.createIdentifier("stringifyDeepObjectPropertyName"),
                                            ),
                                            undefined,
                                            [
                                                factory.createStringLiteral(parameterName),
                                                factory.createIdentifier("propertyName"),
                                            ],
                                        ),
                                    ),
                                ], ts.NodeFlags.Const),
                            ),
                            factory.createExpressionStatement(factory.createCallExpression(
                                factory.createPropertyAccessExpression(
                                    factory.createIdentifier("lib"),
                                    factory.createIdentifier("addParameter"),
                                ),
                                undefined,
                                [
                                    factory.createIdentifier("parameters"),
                                    createdEncodeParameterNameExpression(
                                        parameterIn,
                                        factory.createIdentifier("parameterName"),
                                    ),
                                    createdEncodedParameterValueExpression(
                                        parameterIn,
                                        additionalPropertiesType,
                                        factory.createIdentifier("propertyValue"),
                                    ),
                                ],
                            )),
                        ], true),
                    ),
                ], true),
            );
        }
        else if (glue == null || assignment == null) {
            yield factory.createForOfStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createArrayBindingPattern([
                            factory.createBindingElement(
                                undefined,
                                undefined,
                                factory.createIdentifier("propertyName"),
                                undefined,
                            ),
                            factory.createBindingElement(
                                undefined,
                                undefined,
                                factory.createIdentifier("propertyValue"),
                                undefined,
                            ),
                        ]),
                    ),
                ], ts.NodeFlags.Const),
                factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("Object"),
                        factory.createIdentifier("entries"),
                    ),
                    undefined,
                    [factory.createIdentifier("value")],
                ),
                factory.createBlock([
                    factory.createIfStatement(
                        factory.createBinaryExpression(
                            factory.createIdentifier("propertyValue"),
                            factory.createToken(ts.SyntaxKind.ExclamationEqualsToken),
                            factory.createNull(),
                        ),
                        factory.createBlock([
                            factory.createExpressionStatement(factory.createCallExpression(
                                factory.createPropertyAccessExpression(
                                    factory.createIdentifier("lib"),
                                    factory.createIdentifier("addParameter"),
                                ),
                                undefined,
                                [
                                    factory.createIdentifier("parameters"),
                                    createdEncodeParameterNameExpression(
                                        parameterIn,
                                        factory.createIdentifier("propertyName"),
                                    ),
                                    createdEncodedParameterValueExpression(
                                        parameterIn,
                                        additionalPropertiesType,
                                        factory.createIdentifier("propertyValue"),
                                    ),
                                ],
                            )),
                        ], true),
                    ),
                ], true),
            );
        }
        else {
            yield factory.createVariableStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createIdentifier("parameterEntries"),
                        undefined,
                        undefined,
                        factory.createNewExpression(
                            factory.createIdentifier("Array"),
                            [factory.createTupleTypeNode([
                                factory.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
                                factory.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
                            ])],
                            [],
                        ),
                    ),
                ], ts.NodeFlags.Const),
            );
            yield factory.createForOfStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createArrayBindingPattern([
                            factory.createBindingElement(
                                undefined,
                                undefined,
                                factory.createIdentifier("propertyName"),
                                undefined,
                            ),
                            factory.createBindingElement(
                                undefined,
                                undefined,
                                factory.createIdentifier("propertyValue"),
                                undefined,
                            ),
                        ]),
                    ),
                ], ts.NodeFlags.Const),
                factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("Object"),
                        factory.createIdentifier("entries"),
                    ),
                    undefined,
                    [factory.createIdentifier("value")],
                ),
                factory.createBlock([
                    factory.createIfStatement(
                        factory.createBinaryExpression(
                            factory.createIdentifier("propertyValue"),
                            factory.createToken(ts.SyntaxKind.ExclamationEqualsToken),
                            factory.createNull(),
                        ),
                        factory.createBlock([
                            factory.createExpressionStatement(factory.createCallExpression(
                                factory.createPropertyAccessExpression(
                                    factory.createIdentifier("parameterEntries"),
                                    factory.createIdentifier("push"),
                                ),
                                undefined,
                                [factory.createArrayLiteralExpression(
                                    [
                                        factory.createIdentifier("propertyName"),
                                        createdEncodedParameterValueExpression(
                                            parameterIn,
                                            additionalPropertiesType,
                                            factory.createIdentifier("propertyValue"),
                                        ),
                                    ],
                                    false,
                                )],
                            )),
                        ], true),
                    ),
                ], true),
            );
            yield factory.createVariableStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createIdentifier("parameterValue"),
                        undefined,
                        undefined,
                        factory.createCallExpression(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("lib"),
                                factory.createIdentifier("stringifyParameterEntries"),
                            ),
                            undefined,
                            [
                                factory.createIdentifier("parameterEntries"),
                                factory.createStringLiteral(prefix ?? ""),
                                factory.createStringLiteral(glue),
                                factory.createStringLiteral(assignment),
                            ],
                        ),
                    ),
                ], ts.NodeFlags.Const),
            );
            yield factory.createExpressionStatement(factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("lib"),
                    factory.createIdentifier("addParameter"),
                ),
                undefined,
                [
                    factory.createIdentifier("parameters"),
                    createdEncodeParameterNameExpression(
                        parameterIn,
                        factory.createStringLiteral(parameterName),
                    ),
                    factory.createIdentifier("parameterValue"),
                ],
            ));
        }
    }

    function createdEncodedParameterValueExpression(
        parameterIn: ParameterIn,
        type: "boolean" | "object" | "number" | "string" | "integer" | "array" | undefined,
        parameterValueExpression: ts.Expression,
    ): ts.Expression {
        switch (type) {
            case "string":
                switch (parameterIn) {
                    case "path":
                    case "query": return factory.createCallExpression(
                        factory.createIdentifier("encodeURIComponent"),
                        undefined,
                        [
                            parameterValueExpression,
                        ],
                    );

                    default:
                        return parameterValueExpression;
                }

            case "number":
            case "integer":
            case "boolean": return factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    parameterValueExpression,
                    factory.createIdentifier("toString"),
                ),
                undefined,
                [],
            );

            default:
                throw new Error(`unexpected schema type ${type}`);

        }

    }

    function createdEncodeParameterNameExpression(
        parameterIn: ParameterIn,
        parameterNameExpression: ts.Expression,
    ) {
        switch (parameterIn) {
            case "header":
                return factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        parameterNameExpression,
                        "toLowerCase",
                    ),
                    undefined,
                    [],
                );

            default: return parameterNameExpression;
        }
    }

}
