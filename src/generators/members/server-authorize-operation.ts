import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllOperations } from "../../selectors/index.js";
import { stringifyIdentifier, stringifyType } from "../../utils/index.js";

export function* generateServeAuthorizeOperationDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
) {
    for (
        const { operationObject, method, path } of
        selectAllOperations(document)
    ) {
        yield createMethodDeclaration(operationObject, method, path);
    }

    function createMethodDeclaration(
        operationObject: OpenAPIV3.OperationObject,
        method: OpenAPIV3.HttpMethods,
        path: string,
    ) {
        assert(operationObject.operationId);

        const credentialsTypeName = stringifyType([
            operationObject.operationId,
            "credentials",
        ]);

        const authorizationMethodName = stringifyIdentifier([
            "authorize",
            operationObject.operationId,
        ]);

        const statements = [
            ...emitStatements(operationObject),
        ];

        const methodDeclaration = factory.createMethodDeclaration(
            [
                factory.createToken(ts.SyntaxKind.ProtectedKeyword),
                factory.createToken(ts.SyntaxKind.AsyncKeyword),
            ],
            undefined,
            authorizationMethodName,
            undefined,
            undefined,
            [
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    "credentials",
                    undefined,
                    factory.createTypeReferenceNode(
                        factory.createIdentifier(credentialsTypeName),
                    ),
                ),
            ],
            undefined,
            factory.createBlock(statements, true),
        );

        return methodDeclaration;
    }

    function* emitStatements(
        operationObject: OpenAPIV3.OperationObject,
    ) {
        assert(operationObject.operationId);

        const authorizationTypeName = stringifyType([
            operationObject.operationId,
            "authorization",
        ]);

        const securityRequirements = operationObject.security ?? document.security ?? [];
        if (securityRequirements.length === 0) {
            yield factory.createReturnStatement(factory.createAsExpression(
                factory.createObjectLiteralExpression([]),
                factory.createTypeReferenceNode(
                    factory.createIdentifier(authorizationTypeName),
                    [
                        factory.createTypeReferenceNode("Authorization"),
                    ],
                ),
            ));
            return;
        }

        const authorizationSet = new Set<string>();

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("authorization"),
                    undefined,
                    factory.createTypeReferenceNode(
                        factory.createIdentifier("Partial"),
                        [
                            factory.createTypeReferenceNode("Authorization"),
                        ],
                    ),
                    factory.createObjectLiteralExpression([]),
                ),
            ], ts.NodeFlags.Const),
        );

        for (const requirement of securityRequirements) {
            for (const scheme of Object.keys(requirement)) {
                if (authorizationSet.has(scheme)) continue;

                authorizationSet.add(scheme);

                const schemeIdentifier = stringifyIdentifier([scheme]);

                const propertyName = stringifyIdentifier([
                    scheme,
                    "authorization",
                    "handler",
                ]);

                yield factory.createIfStatement(
                    factory.createLogicalAnd(
                        factory.createBinaryExpression(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("credentials"),
                                factory.createIdentifier(schemeIdentifier),
                            ),
                            factory.createToken(ts.SyntaxKind.ExclamationEqualsToken),
                            factory.createNull(),
                        ),
                        factory.createBinaryExpression(
                            factory.createPropertyAccessExpression(
                                factory.createThis(),
                                factory.createIdentifier(propertyName),
                            ),
                            factory.createToken(ts.SyntaxKind.ExclamationEqualsToken),
                            factory.createNull(),
                        ),
                    ),
                    factory.createBlock([
                        factory.createExpressionStatement(factory.createBinaryExpression(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("authorization"),
                                factory.createIdentifier(schemeIdentifier),
                            ),
                            factory.createToken(ts.SyntaxKind.EqualsToken),
                            factory.createAwaitExpression(
                                factory.createCallExpression(
                                    factory.createPropertyAccessExpression(
                                        factory.createThis(),
                                        factory.createIdentifier(propertyName),
                                    ),
                                    undefined,
                                    [
                                        factory.createPropertyAccessExpression(
                                            factory.createIdentifier("credentials"),
                                            factory.createIdentifier(schemeIdentifier),
                                        ),
                                    ],
                                ),
                            ),
                        )),
                    ], true),
                );
            }

            const schemes = Object.keys(requirement);
            if (schemes.length > 0) {
                const test = schemes.
                    map(scheme => factory.createBinaryExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("authorization"),
                            stringifyIdentifier([scheme]),
                        ),
                        factory.createToken(ts.SyntaxKind.ExclamationEqualsToken),
                        factory.createNull(),
                    )).
                    reduce((left, right) => factory.createLogicalAnd(left, right));

                yield factory.createIfStatement(
                    test,
                    factory.createBlock([
                        factory.createReturnStatement(factory.createAsExpression(
                            factory.createIdentifier("authorization"),
                            factory.createTypeReferenceNode(
                                factory.createIdentifier(authorizationTypeName),
                                [
                                    factory.createTypeReferenceNode(
                                        "Authorization",
                                    ),
                                ],
                            ),
                        )),
                    ]),
                );
            }
        }

        yield factory.createReturnStatement();
    }

}
