import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllOperations, selectContentTypesFromContentContainer, selectHeadersFromResponse, selectParametersFromPathAndMethod, selectReponsesFromOperation, selectSecuritySchemesForOperation } from "../../selectors/index.js";
import { addMeta, generateAllStatusCodes, resolveObject, resolvePointerParts, stringifyIdentifier, stringifyType, takeStatusCodes, toReference } from "../../utils/index.js";
import { PackageConfig } from "../package.js";

export function* generateClientOperationDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    config: PackageConfig,
    nameIndex: Record<string, string>,
) {
    for (
        const { operationObject, path, method } of
        selectAllOperations(document)
    ) {
        yield createMethodDeclaration(operationObject, path, method);
    }

    function createMethodDeclaration(
        operationObject: OpenAPIV3.OperationObject,
        path: string,
        method: OpenAPIV3.HttpMethods,
    ) {
        assert(operationObject.operationId);

        const methodName = stringifyIdentifier([operationObject.operationId]);
        const outgoingRequestTypeName = stringifyType([
            operationObject.operationId,
            "outgoing",
            "request",
        ]);
        const incomingResponseTypeName = stringifyType([
            operationObject.operationId,
            "incoming",
            "response",
        ]);

        const statements = [
            ...emitStatements(operationObject, path, method),
        ];

        const methodDeclaration = factory.createMethodDeclaration(
            [
                factory.createToken(ts.SyntaxKind.PublicKeyword),
                factory.createToken(ts.SyntaxKind.AsyncKeyword),
            ],
            undefined,
            methodName,
            undefined,
            undefined,
            [
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    "outgoingRequestModel",
                    undefined,
                    factory.createTypeReferenceNode(
                        factory.createIdentifier(outgoingRequestTypeName),
                    ),
                ),
            ],
            factory.createTypeReferenceNode(
                "Promise",
                [
                    factory.createTypeReferenceNode(
                        factory.createIdentifier(incomingResponseTypeName),
                    ),
                ],
            ),
            factory.createBlock(statements, true),
        );

        addMeta(methodDeclaration, {
            deprecated: operationObject.deprecated,
            description: operationObject.description,
        });

        return methodDeclaration;
    }

    function* emitStatements(
        operationObject: OpenAPIV3.OperationObject,
        path: string,
        method: OpenAPIV3.HttpMethods,
    ) {
        yield* emitValidateRequestParametersStatements(operationObject, path, method);

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("routeParameters"),
                    undefined,
                    factory.createTypeReferenceNode(
                        factory.createQualifiedName(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("Parameters"),
                        ),
                    ),
                    factory.createObjectLiteralExpression([], true),
                ),
            ], ts.NodeFlags.Const),
        );

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("requestQuery"),
                    undefined,
                    factory.createTypeReferenceNode(
                        factory.createQualifiedName(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("Parameters"),
                        ),
                    ),
                    factory.createObjectLiteralExpression([], true),
                ),
            ], ts.NodeFlags.Const),
        );

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("requestHeaders"),
                    undefined,
                    factory.createTypeReferenceNode(
                        factory.createQualifiedName(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("Parameters"),
                        ),
                    ),
                    factory.createObjectLiteralExpression([], true),
                ),
            ], ts.NodeFlags.Const),
        );

        yield factory.createTryStatement(
            factory.createBlock([...emitInjectRequestParametersStatements(
                operationObject,
                path,
                method,
            )], true),
            factory.createCatchClause(undefined, factory.createBlock([
                factory.createThrowStatement(factory.createNewExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("OutgoingRequestParameterInjectionError"),
                    ),
                    undefined,
                    [
                    ],
                )),
            ], true)),
            undefined,
        );

        yield* emitSetupRequestStatements(operationObject, path, method);

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("clientResponse"),
                    undefined,
                    undefined,
                    factory.createAwaitExpression(factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createThis(),
                            factory.createIdentifier("send"),
                        ),
                        undefined,
                        [factory.createIdentifier("clientRequest")],
                    )),
                ),
            ], ts.NodeFlags.Const),
        );

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("responseHeaders"),
                    undefined,
                    undefined,
                    factory.createObjectLiteralExpression([
                        factory.createSpreadAssignment(factory.createPropertyAccessExpression(
                            factory.createIdentifier("clientResponse"),
                            factory.createIdentifier("headers"),
                        )),
                    ], false),
                ),
            ], ts.NodeFlags.Const),
        );

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("responseContentTypeHeader"),
                    undefined,
                    undefined,
                    factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("getParameterValue"),
                        ),
                        undefined,
                        [
                            factory.createIdentifier("responseHeaders"),
                            factory.createStringLiteral("content-type"),
                        ],
                    ),
                ),
            ], ts.NodeFlags.Const),
        );

        yield* emitResponseStatements(operationObject, path, method);

        yield factory.createReturnStatement(
            factory.createIdentifier("incomingResponseModel"),
        );
    }

    function* emitSetupRequestStatements(
        operationObject: OpenAPIV3.OperationObject,
        path: string,
        method: OpenAPIV3.HttpMethods,
    ) {
        assert(operationObject.operationId);

        const operationPointerParts = ["paths", path, method];

        const requestBodyPointerParts = resolvePointerParts(
            [...operationPointerParts, "requestBody"],
            operationObject.requestBody,
        );
        const requestBody = resolveObject(
            document,
            operationObject.requestBody,
        );

        const acceptTypesConstant = stringifyIdentifier([operationObject.operationId, "accept", "types"]);

        yield factory.createExpressionStatement(factory.createCallExpression(
            factory.createPropertyAccessExpression(
                factory.createIdentifier("lib"),
                factory.createIdentifier("addParameter"),
            ),
            undefined,
            [
                factory.createIdentifier("requestHeaders"),
                factory.createStringLiteral("accept"),
                factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("stringifyAcceptHeader"),
                    ),
                    undefined,
                    [factory.createPropertyAccessExpression(
                        factory.createIdentifier("shared"),
                        factory.createIdentifier(acceptTypesConstant),
                    )],
                ),
            ],
        ));

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("requestPath"),
                    undefined,
                    undefined,
                    factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createPropertyAccessExpression(
                                factory.createThis(),
                                factory.createIdentifier("router"),
                            ),
                            factory.createIdentifier("stringifyRoute"),
                        ),
                        undefined,
                        [
                            factory.createObjectLiteralExpression(
                                [
                                    factory.createPropertyAssignment(
                                        factory.createIdentifier("name"),
                                        factory.createStringLiteral(path),
                                    ),
                                    factory.createPropertyAssignment(
                                        factory.createIdentifier("parameters"),
                                        factory.createCallExpression(
                                            factory.createPropertyAccessExpression(
                                                factory.createIdentifier("Object"),
                                                factory.createIdentifier("fromEntries"),
                                            ),
                                            undefined,
                                            [factory.createCallExpression(
                                                factory.createPropertyAccessExpression(
                                                    factory.createIdentifier("lib"),
                                                    factory.createIdentifier("parametersToEntries"),
                                                ),
                                                undefined,
                                                [factory.createIdentifier("routeParameters")],
                                            )],
                                        ),
                                    ),
                                ], true),
                        ],
                    ),
                ),
            ], ts.NodeFlags.Const),
        );

        yield factory.createIfStatement(
            factory.createBinaryExpression(
                factory.createIdentifier("requestPath"),
                factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                factory.createNull(),
            ),
            factory.createBlock([
                factory.createThrowStatement(factory.createNewExpression(
                    factory.createIdentifier("Error"),
                    undefined,
                    [
                        factory.createStringLiteral("Expected requestPath"),
                    ],
                )),

            ]),
        );

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("requestUrl"),
                    undefined,
                    undefined,
                    factory.createNewExpression(
                        factory.createIdentifier("URL"),
                        undefined,
                        [
                            factory.createIdentifier("requestPath"),
                            factory.createPropertyAccessExpression(
                                factory.createThis(),
                                factory.createIdentifier("baseUrl"),
                            ),
                        ],
                    ),
                ),
            ], ts.NodeFlags.Const),
        );
        yield factory.createExpressionStatement(factory.createBinaryExpression(
            factory.createPropertyAccessExpression(
                factory.createIdentifier("requestUrl"),
                "search",
            ),
            factory.createToken(ts.SyntaxKind.EqualsToken),
            factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("lib"),
                    factory.createIdentifier("stringifyParameters"),
                ),
                undefined,
                [factory.createIdentifier("requestQuery")],
            ),
        ));

        const contentTypeEntries = requestBody && requestBodyPointerParts ?
            [...selectContentTypesFromContentContainer(
                config.requestTypes,
                requestBody,
                requestBodyPointerParts,
            )] :
            [];

        if (contentTypeEntries.length > 0) {
            assert(requestBody);
            assert(requestBodyPointerParts);

            const [defaultContentTypeEntry] = contentTypeEntries;
            const {
                contentType: defaultContentType,
                mediaTypeObject: defaultMediaTypeObject,
            } = defaultContentTypeEntry;

            const caseClauses = [...emitRequestContentTypeCaseClauses(
                requestBodyPointerParts,
                requestBody,
                method,
            )];

            yield factory.createVariableStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createIdentifier("clientRequest"),
                        undefined,
                        factory.createTypeReferenceNode(
                            factory.createQualifiedName(
                                factory.createIdentifier("lib"),
                                "ClientOutgoingRequest",
                            ),
                        ),
                        undefined,
                    ),
                ], ts.NodeFlags.Let),
            );

            yield factory.createIfStatement(
                factory.createBinaryExpression(
                    factory.createStringLiteral("contentType"),
                    factory.createToken(ts.SyntaxKind.InKeyword),
                    factory.createIdentifier("outgoingRequestModel"),
                ),
                factory.createBlock([
                    factory.createExpressionStatement(factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("addParameter"),
                        ),
                        undefined,
                        [
                            factory.createIdentifier("requestHeaders"),
                            factory.createStringLiteral("content-type"),
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("outgoingRequestModel"),
                                factory.createIdentifier("contentType"),
                            ),
                        ],
                    )),

                    factory.createSwitchStatement(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("outgoingRequestModel"),
                            "contentType",
                        ),
                        factory.createCaseBlock(caseClauses),
                    ),
                ]),
                factory.createBlock([
                    factory.createExpressionStatement(factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("addParameter"),
                        ),
                        undefined,
                        [
                            factory.createIdentifier("requestHeaders"),
                            factory.createStringLiteral("content-type"),
                            factory.createStringLiteral(defaultContentType),
                        ],
                    )),
                    factory.createExpressionStatement(factory.createBinaryExpression(
                        factory.createIdentifier("clientRequest"),
                        factory.createToken(ts.SyntaxKind.EqualsToken),
                        createClientRequest(
                            [...requestBodyPointerParts, "content", defaultContentType],
                            method,
                        ),
                    )),
                ]),
            );
        }
        else {
            yield factory.createVariableStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createIdentifier("clientRequest"),
                        undefined,
                        factory.createTypeReferenceNode(
                            factory.createQualifiedName(
                                factory.createIdentifier("lib"),
                                "ClientOutgoingRequest",
                            ),
                        ),
                        createEmptyClientRequest(method),
                    ),
                ], ts.NodeFlags.Const),
            );
        }

    }

    function* emitValidateRequestParametersStatements(
        operationObject: OpenAPIV3.OperationObject,
        path: string,
        method: OpenAPIV3.HttpMethods,
    ) {
        assert(operationObject.operationId);

        const parametersTypeName = stringifyType([
            operationObject.operationId,
            "request",
            "parameters",
        ]);
        const validateRequestParametersFunctionName = stringifyIdentifier([
            "validate",
            parametersTypeName,
        ]);

        yield factory.createIfStatement(
            factory.createPropertyAccessExpression(
                factory.createThis(),
                factory.createIdentifier("validateOutgoingParameters"),
            ),
            factory.createBlock([
                factory.createVariableStatement(
                    undefined,
                    factory.createVariableDeclarationList([factory.createVariableDeclaration(
                        factory.createIdentifier("invalidPaths"),
                        undefined,
                        undefined,
                        factory.createArrayLiteralExpression([
                            factory.createSpreadElement(factory.createCallExpression(
                                factory.createPropertyAccessExpression(
                                    factory.createIdentifier("shared"),
                                    factory.createIdentifier(validateRequestParametersFunctionName),
                                ),
                                undefined,
                                [factory.createPropertyAccessExpression(
                                    factory.createIdentifier("outgoingRequestModel"),
                                    factory.createIdentifier("parameters"),
                                )],
                            )),
                        ], false),
                    )], ts.NodeFlags.Const),
                ),
                factory.createIfStatement(
                    factory.createBinaryExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("invalidPaths"),
                            factory.createIdentifier("length"),
                        ),
                        factory.createToken(ts.SyntaxKind.GreaterThanToken),
                        factory.createNumericLiteral(0),
                    ),
                    factory.createBlock([
                        factory.createThrowStatement(factory.createNewExpression(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("lib"),
                                factory.createIdentifier("OutgoingRequestParametersValidationError"),
                            ),
                            undefined,
                            [factory.createIdentifier("invalidPaths")],
                        )),
                    ], true),
                ),
            ], true),
        );

    }

    function* emitInjectRequestParametersStatements(
        operationObject: OpenAPIV3.OperationObject,
        path: string,
        method: OpenAPIV3.HttpMethods,
    ) {
        for (
            const { parameterObject, parameterPointerParts } of
            selectParametersFromPathAndMethod(document, path, method)
        ) {
            const parameterTypeReference = toReference(parameterPointerParts);
            const parameterTypeName = nameIndex[parameterTypeReference];
            const functionName = stringifyIdentifier([
                "inject",
                parameterTypeName,
            ]);
            const parameterName = stringifyIdentifier([
                parameterObject.name,
            ]);

            let parametersSource: ts.Expression;
            switch (parameterObject.in) {
                case "path":
                    parametersSource = factory.createIdentifier("routeParameters");
                    break;

                case "query":
                    parametersSource = factory.createIdentifier("requestQuery");
                    break;

                case "header":
                    parametersSource = factory.createIdentifier("requestHeaders");
                    break;

                default: throw new Error("cannot happen");
            }

            const parameterValue = factory.createPropertyAccessExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("outgoingRequestModel"),
                    factory.createIdentifier("parameters"),
                ),
                factory.createIdentifier(parameterName),
            );

            yield factory.createExpressionStatement(factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("internal"),
                    factory.createIdentifier(functionName),
                ),
                undefined,
                [
                    parametersSource,
                    parameterValue,
                ],
            ));
        }

        for (
            const { securityScheme, securitySchemeObject, pointerParts } of
            selectSecuritySchemesForOperation(document, operationObject)
        ) {
            const credentialTypeReference = toReference(pointerParts);
            const credentialTypeName = nameIndex[credentialTypeReference];
            const functionName = stringifyIdentifier([
                "inject",
                credentialTypeName,
            ]);
            const credentialName = stringifyIdentifier([
                securityScheme,
            ]);

            let credentialsSource: ts.Expression;
            switch (securitySchemeObject.type) {
                case "apiKey":
                    switch (securitySchemeObject.in) {
                        case "query":
                            credentialsSource = factory.createIdentifier("requestQuery");
                            break;

                        case "header":
                            credentialsSource = factory.createIdentifier("requestHeaders");
                            break;

                        default: throw new Error("cannot happen");
                    }
                    break;

                case "http":
                    credentialsSource = factory.createIdentifier("requestHeaders");
                    break;

                default: throw new Error("cannot happen");
            }

            const credentialValue = factory.createPropertyAccessExpression(
                factory.createPropertyAccessExpression(
                    factory.createThis(),
                    factory.createIdentifier("credentials"),
                ),
                factory.createIdentifier(credentialName),
            );

            yield factory.createExpressionStatement(factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("internal"),
                    factory.createIdentifier(functionName),
                ),
                undefined,
                [
                    credentialsSource,
                    credentialValue,
                ],
            ));

        }

    }

    function* emitResponseParametersStatements(
        responseObject: OpenAPIV3.ResponseObject,
        responsePointerPath: string[],
    ) {
        const responseReference = toReference(responsePointerPath);
        const typeName = nameIndex[responseReference];
        const parametersTypeName = stringifyType([
            typeName,
            "response",
            "parameters",
        ]);
        const validateResponseParametersFn = stringifyIdentifier([
            "validate",
            parametersTypeName,
        ]);

        const contentTypes = [
            ...selectContentTypesFromContentContainer(
                config.responseTypes,
                responseObject,
                responsePointerPath,
            ),
        ];

        if (contentTypes.length > 0) {
            yield factory.createIfStatement(
                factory.createBinaryExpression(
                    factory.createIdentifier("responseContentTypeHeader"),
                    factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                    factory.createNull(),
                ),
                factory.createBlock([
                    factory.createThrowStatement(factory.createNewExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("MissingResponseContentTypeError"),
                        ),
                        undefined,
                        [
                        ],
                    )),
                ], true),
            );

            yield factory.createVariableStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createIdentifier("responseContentType"),
                        undefined,
                        undefined,
                        factory.createCallExpression(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("lib"),
                                factory.createIdentifier("parseContentTypeHeader"),
                            ),
                            undefined,
                            [
                                factory.createIdentifier("responseContentTypeHeader"),
                            ],
                        ),
                    ),
                ], ts.NodeFlags.Const),
            );
        }

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("responseParameters"),
                    undefined,
                    undefined,
                    undefined,
                ),
            ], ts.NodeFlags.Let),
        );
        yield factory.createTryStatement(
            factory.createBlock([factory.createExpressionStatement(
                factory.createBinaryExpression(
                    factory.createIdentifier("responseParameters"),
                    factory.createToken(ts.SyntaxKind.EqualsToken),
                    factory.createAsExpression(
                        factory.createObjectLiteralExpression([
                            ...emitResponseParametersAssignments(
                                responseObject,
                                responsePointerPath,
                            ),
                        ], true),
                        factory.createTypeReferenceNode(
                            factory.createQualifiedName(
                                factory.createIdentifier("shared"),
                                factory.createIdentifier(parametersTypeName),
                            ),
                        ),
                    ),
                ),
            )], true),
            factory.createCatchClause(undefined, factory.createBlock([
                factory.createThrowStatement(factory.createNewExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("IncomingResponseParameterExtractionError"),
                    ),
                    undefined,
                    [
                    ],
                )),
            ], true)),
            undefined,
        );

        yield factory.createIfStatement(
            factory.createPropertyAccessExpression(
                factory.createThis(),
                factory.createIdentifier("validateIncomingParameters"),
            ),
            factory.createBlock([
                factory.createVariableStatement(
                    undefined,
                    factory.createVariableDeclarationList([factory.createVariableDeclaration(
                        factory.createIdentifier("invalidPaths"),
                        undefined,
                        undefined,
                        factory.createArrayLiteralExpression([
                            factory.createSpreadElement(factory.createCallExpression(
                                factory.createPropertyAccessExpression(
                                    factory.createIdentifier("shared"),
                                    factory.createIdentifier(validateResponseParametersFn),
                                ),
                                undefined,
                                [factory.createIdentifier("responseParameters")],
                            )),
                        ], false),
                    )], ts.NodeFlags.Const),
                ),
                factory.createIfStatement(
                    factory.createBinaryExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("invalidPaths"),
                            factory.createIdentifier("length"),
                        ),
                        factory.createToken(ts.SyntaxKind.GreaterThanToken),
                        factory.createNumericLiteral(0),
                    ),
                    factory.createBlock([
                        factory.createThrowStatement(factory.createNewExpression(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("lib"),
                                factory.createIdentifier("IncomingResponseParametersValidationError"),
                            ),
                            undefined,
                            [factory.createIdentifier("invalidPaths")],
                        )),
                    ], true),
                ),
            ], true),
        );

    }

    function* emitResponseParametersAssignments(
        responseObject: OpenAPIV3.ResponseObject,
        responsePointerPath: string[],
    ) {
        for (
            const { header, headerPointerParts } of
            selectHeadersFromResponse(document, responseObject, responsePointerPath)
        ) {
            const parameterTypeReference = toReference(headerPointerParts);
            const parameterTypeName = nameIndex[parameterTypeReference];
            const functionName = stringifyIdentifier([
                "extract",
                parameterTypeName,
            ]);
            const parameterName = stringifyIdentifier([
                header,
            ]);

            const parametersSource = factory.createIdentifier("responseHeaders");

            const parameterValue = factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("internal"),
                    factory.createIdentifier(functionName),
                ),
                undefined,
                [
                    parametersSource,
                ],
            );

            yield factory.createPropertyAssignment(
                parameterName, parameterValue,
            );
        }

    }

    function* emitResponseStatements(
        operationObject: OpenAPIV3.OperationObject,
        path: string,
        method: OpenAPIV3.HttpMethods,
    ) {
        assert(operationObject.operationId);

        const operationPointerParts = ["paths", path, method];

        const incomingResponseTypeName = stringifyType([
            operationObject.operationId,
            "incoming",
            "response",
        ]);

        const caseClauses = [...emitResponseStatusCaseClauses(
            operationPointerParts,
            operationObject,
        )];

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("incomingResponseModel"),
                    undefined,
                    factory.createTypeReferenceNode(
                        factory.createIdentifier(incomingResponseTypeName),
                    ),
                ),
            ], ts.NodeFlags.Let),
        );

        yield factory.createSwitchStatement(
            factory.createPropertyAccessExpression(
                factory.createIdentifier("clientResponse"),
                factory.createIdentifier("status"),
            ),
            factory.createCaseBlock(caseClauses),
        );

    }

    function* emitRequestContentTypeCaseClauses(
        requestBodyPointerParts: string[],
        requestBodyObject: OpenAPIV3.RequestBodyObject,
        method: OpenAPIV3.HttpMethods,
    ) {
        const contentTypeEntries = selectContentTypesFromContentContainer(
            config.requestTypes,
            requestBodyObject,
            requestBodyPointerParts,
        );

        for (
            const { contentType, mediaTypeObject, mediaTypePointerParts } of
            contentTypeEntries
        ) {

            const valueExpression = createClientRequest(
                mediaTypePointerParts,
                method,
            );

            yield factory.createCaseClause(
                factory.createStringLiteral(contentType),
                [
                    factory.createExpressionStatement(factory.createBinaryExpression(
                        factory.createIdentifier("clientRequest"),
                        factory.createToken(ts.SyntaxKind.EqualsToken),
                        valueExpression,
                    )),
                    factory.createBreakStatement(),
                ],
            );
        }

        yield factory.createDefaultClause([
            factory.createThrowStatement(factory.createNewExpression(
                factory.createIdentifier("TypeError"),
                undefined,
                [
                    factory.createStringLiteral("invalid content-type"),
                ],
            )),
        ]);

    }

    function* emitResponseStatusCaseClauses(
        operationPointerParts: string[],
        operationObject: OpenAPIV3.OperationObject,
    ) {
        const statusSet = new Set(generateAllStatusCodes());

        for (
            const { statusKind, responseObject, responsePointerParts } of
            selectReponsesFromOperation(document, operationObject, operationPointerParts)
        ) {
            const statusses = [...takeStatusCodes(statusSet, statusKind)];

            {
                let statusPrev = 0;
                for (const status of statusses) {
                    if (statusPrev !== 0) {
                        yield factory.createCaseClause(
                            factory.createNumericLiteral(statusPrev),
                            [],
                        );
                    }
                    statusPrev = status;
                }

                const responseTypeEntries = [...selectContentTypesFromContentContainer(
                    config.responseTypes,
                    responseObject,
                    responsePointerParts,
                )];

                if (responseTypeEntries.length > 0) {
                    const caseClauses = [...emitResponseContentTypeCaseClauses(
                        responsePointerParts,
                        responseObject,
                    )];

                    yield factory.createCaseClause(
                        factory.createNumericLiteral(statusPrev),
                        [
                            factory.createBlock([
                                ...emitResponseParametersStatements(
                                    responseObject,
                                    responsePointerParts,
                                ),
                                factory.createSwitchStatement(
                                    factory.createIdentifier("responseContentType"),
                                    factory.createCaseBlock(
                                        caseClauses,
                                    ),
                                ),
                                factory.createBreakStatement(),
                            ], true),
                        ],
                    );
                }
                else {
                    yield factory.createCaseClause(
                        factory.createNumericLiteral(statusPrev),
                        [
                            factory.createBlock([
                                ...emitResponseParametersStatements(
                                    responseObject,
                                    responsePointerParts,
                                ),
                                factory.createExpressionStatement(factory.createBinaryExpression(
                                    factory.createIdentifier("incomingResponseModel"),
                                    factory.createToken(ts.SyntaxKind.EqualsToken),
                                    createEmptyResponseModel(),
                                )),
                                factory.createBreakStatement(),
                            ]),
                        ],
                    );
                }

            }

        }

        yield factory.createDefaultClause([
            factory.createThrowStatement(factory.createNewExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("lib"),
                    factory.createIdentifier("UnexpectedStatusError"),
                ),
                undefined,
                [
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("clientResponse"),
                        factory.createIdentifier("status"),
                    ),
                ],
            )),
        ]);
    }

    function* emitResponseContentTypeCaseClauses(
        responsePointerParts: string[],
        responseObject: OpenAPIV3.ResponseObject,
    ) {
        for (
            const { contentType, mediaTypeObject, mediaTypePointerParts } of
            selectContentTypesFromContentContainer(
                config.responseTypes,
                responseObject,
                responsePointerParts,
            )
        ) {
            const valueExpression = createResponseModel(
                mediaTypePointerParts,
                contentType,
            );

            yield factory.createCaseClause(
                factory.createStringLiteral(contentType),
                [
                    factory.createExpressionStatement(factory.createBinaryExpression(
                        factory.createIdentifier("incomingResponseModel"),
                        factory.createToken(ts.SyntaxKind.EqualsToken),
                        valueExpression,
                    )),
                    factory.createBreakStatement(),
                ],
            );
        }

        yield factory.createDefaultClause([
            factory.createThrowStatement(factory.createNewExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("lib"),
                    factory.createIdentifier("UnexpectedResponseContentType"),
                ),
                undefined,
                [
                    factory.createIdentifier("responseContentType"),
                ],
            )),
        ]);
    }

    function createClientRequest(
        mediaTypePointerParts: string[],
        method: OpenAPIV3.HttpMethods,
    ) {
        const mediaTypeReference = toReference(mediaTypePointerParts);
        const mediaTypeName = nameIndex[mediaTypeReference];

        const serializeFn = stringifyIdentifier([
            "serialize",
            mediaTypeName,
        ]);

        return factory.createObjectLiteralExpression([
            factory.createPropertyAssignment(
                factory.createIdentifier("url"),
                factory.createIdentifier("requestUrl"),
            ),
            factory.createPropertyAssignment(
                factory.createIdentifier("method"),
                factory.createStringLiteral(method.toUpperCase()),
            ),
            factory.createPropertyAssignment(
                factory.createIdentifier("headers"),
                factory.createIdentifier("requestHeaders"),
            ),
            factory.createPropertyAssignment(
                factory.createIdentifier("stream"),
                factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("internal"),
                        serializeFn,
                    ),
                    undefined,
                    [
                        factory.createIdentifier("outgoingRequestModel"),
                        factory.createPropertyAccessExpression(
                            factory.createThis(),
                            factory.createIdentifier("validateOutgoingEntities"),
                        ),
                    ],
                ),
            ),
        ], true);
    }

    function createResponseModel(
        mediaTypePointerParts: string[],
        contentType: string,
    ) {
        const mediaTypeReference = toReference(mediaTypePointerParts);
        const mediaTypeName = nameIndex[mediaTypeReference];

        const deserializeFn = stringifyIdentifier([
            "deserialize",
            mediaTypeName,
        ]);

        return factory.createObjectLiteralExpression([
            factory.createSpreadAssignment(
                factory.createObjectLiteralExpression([
                    factory.createPropertyAssignment(
                        factory.createIdentifier("status"),
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("clientResponse"),
                            "status",
                        ),
                    ),
                    factory.createPropertyAssignment(
                        factory.createIdentifier("parameters"),
                        factory.createIdentifier("responseParameters"),
                    ),
                    factory.createPropertyAssignment(
                        factory.createIdentifier("contentType"),
                        factory.createStringLiteral(contentType),
                    ),
                ], true),
            ),
            factory.createSpreadAssignment(factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("internal"),
                    deserializeFn,
                ),
                undefined,
                [
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("clientResponse"),
                        factory.createIdentifier("stream"),
                    ),
                    factory.createPropertyAccessExpression(
                        factory.createThis(),
                        factory.createIdentifier("validateIncomingEntities"),
                    ),
                ],

            )),
        ], true);
    }

    function createEmptyClientRequest(
        method: OpenAPIV3.HttpMethods,
    ) {
        return factory.createObjectLiteralExpression([
            factory.createPropertyAssignment(
                factory.createIdentifier("url"),
                factory.createIdentifier("requestUrl"),
            ),
            factory.createPropertyAssignment(
                factory.createIdentifier("method"),
                factory.createStringLiteral(method.toUpperCase()),
            ),
            factory.createPropertyAssignment(
                factory.createIdentifier("headers"),
                factory.createIdentifier("requestHeaders"),
            ),
        ], true);
    }

    function createEmptyResponseModel() {
        return factory.createObjectLiteralExpression([
            factory.createPropertyAssignment(
                factory.createIdentifier("status"),
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("clientResponse"),
                    "status",
                ),
            ),
            factory.createPropertyAssignment(
                factory.createIdentifier("parameters"),
                factory.createIdentifier("responseParameters"),
            ),
            factory.createPropertyAssignment(
                factory.createIdentifier("contentType"),
                factory.createNull(),
            ),
        ], true);
    }

}
