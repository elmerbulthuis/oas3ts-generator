import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllOperations } from "../../selectors/index.js";
import { stringifyIdentifier, stringifyType } from "../../utils/index.js";

export function* generateServerOperationHandlerDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
) {
    for (
        const { operationObject, method, path } of
        selectAllOperations(document)
    ) {
        yield createPropertyDeclaration(operationObject, method, path);
    }

    function createPropertyDeclaration(
        operationObject: OpenAPIV3.OperationObject,
        method: OpenAPIV3.HttpMethods,
        path: string,
    ) {
        assert(operationObject.operationId);

        const propertyName = stringifyIdentifier([operationObject.operationId, "operation", "handler"]);
        const handlerTypeName = stringifyType([operationObject.operationId, "operation", "handler"]);

        const propertyDeclaration = factory.createPropertyDeclaration(
            [
                factory.createToken(ts.SyntaxKind.ProtectedKeyword),
            ],
            propertyName,
            factory.createToken(
                ts.SyntaxKind.QuestionToken,
            ),
            factory.createTypeReferenceNode(
                handlerTypeName,
                [
                    factory.createTypeReferenceNode("Authorization"),
                ],
            ),
            undefined,
        );

        return propertyDeclaration;
    }

}
