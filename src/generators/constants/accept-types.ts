import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllOperations, selectResponseContentTypes } from "../../selectors/index.js";
import { stringifyIdentifier, stringifyType } from "../../utils/index.js";
import { PackageConfig } from "../index.js";

export function* generateAcceptTypesConstantDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    config: PackageConfig,
) {
    for (
        const { operationObject } of
        selectAllOperations(document)
    ) {
        yield* emitFromOperation(operationObject);
    }

    function* emitFromOperation(
        operationObject: OpenAPIV3.OperationObject,
    ) {
        assert(operationObject.operationId);

        const typeName = stringifyType([operationObject.operationId, "accept", "types"]);
        const constantName = stringifyIdentifier([operationObject.operationId, "accept", "types"]);

        const responseTypeSet = selectResponseContentTypes(
            document, config.responseTypes, operationObject,
        );

        yield factory.createVariableStatement(
            [
                factory.createToken(ts.SyntaxKind.ExportKeyword),
            ],
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier(constantName),
                    undefined,
                    undefined,
                    factory.createNewExpression(
                        factory.createIdentifier("Set"),
                        [
                            factory.createTypeReferenceNode(
                                factory.createIdentifier(typeName),
                                undefined,
                            ),
                        ],
                        [
                            factory.createArrayLiteralExpression(
                                [...responseTypeSet].map(type => factory.createStringLiteral(type)),
                                false,
                            ),
                        ],
                    )),
            ], ts.NodeFlags.Const),
        );
    }

}
