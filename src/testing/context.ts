import assert from "assert";
import * as http from "http";
import { Promisable } from "type-fest";
import * as api from "./package/src/main.js";

export interface Context {
    endpoint: URL;
    server: api.Server;
    createClient(credentials?: api.ClientCredentials): api.Client,
    lastError?: Error,
}

export async function withContext(
    job: (context: Context) => Promisable<void>,
) {

    const endpoint = new URL("http://localhost:9000");

    const server = new api.Server({
        // validateIncomingEntities: true,
        // validateIncomingParameters: true,
        // validateOutgoingEntities: true,
        // validateOutgoingParameters: true,
    });

    const context: Context = {
        endpoint,
        server,
        createClient(credentials: api.ClientCredentials = {}) {
            const client = new api.Client(
                {
                    baseUrl: endpoint,
                    // validateIncomingEntities: true,
                    // validateIncomingParameters: true,
                    // validateOutgoingEntities: true,
                    // validateOutgoingParameters: true,
                },
                credentials,
            );
            return client;
        },
    };

    const onError = (error: unknown) => {
        assert(error instanceof Error);

        context.lastError = error;
    };

    const httpServer = http.createServer(server.asRequestListener({
        onError,
    }));

    await new Promise<void>(resolve => httpServer.listen(
        endpoint.port,
        () => resolve(),
    ));

    try {
        await job(context);
    }
    finally {
        await new Promise<void>((resolve, reject) => httpServer.close(
            error => error ?
                reject(error) :
                resolve(),
        ));
    }

}
