import test from "tape-promise/tape.js";
import { injectLabelArrayParameterVAlueRequestParameter, injectLabelDictionaryParameterVAlueRequestParameter, injectLabelExplodeArrayParameterVAlueRequestParameter, injectLabelExplodeDictionaryParameterVAlueRequestParameter, injectLabelExplodeObjectParameterVAlueRequestParameter, injectLabelNumberParameterVAlueRequestParameter, injectLabelObjectParameterVAlueRequestParameter, injectLabelStringParameterVAlueRequestParameter } from "./package/src/client-internal.js";
import { extractLabelArrayParameterVAlueRequestParameter, extractLabelDictionaryParameterVAlueRequestParameter, extractLabelExplodeArrayParameterVAlueRequestParameter, extractLabelExplodeDictionaryParameterVAlueRequestParameter, extractLabelExplodeObjectParameterVAlueRequestParameter, extractLabelNumberParameterVAlueRequestParameter, extractLabelObjectParameterVAlueRequestParameter, extractLabelStringParameterVAlueRequestParameter } from "./package/src/server-internal.js";

test("inject label string", async t => {
    const parameters = {};
    const value = ":/?#[]@!$&'()*+,;= ";
    injectLabelStringParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "V-Alue": ".%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20",
    });
});

test("extract label string", async t => {
    const parameters = {
        "V-Alue": ".%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20",
    };
    const value = extractLabelStringParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, ":/?#[]@!$&'()*+,;= ");
});

test("inject label number", async t => {
    const parameters = {};
    const value = 10;
    injectLabelNumberParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "V-Alue": ".10",
    });
});

test("extract label number", async t => {
    const parameters = {
        "V-Alue": ".10",
    };
    const value = extractLabelNumberParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, 10);
});

test("inject label array", async t => {
    const parameters = {};
    const value = [
        ":/?#[]@!$&'()*+,;= ",
        "2",
    ];
    injectLabelArrayParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "V-Alue": ".%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20,2",
    });
});

test("extract label array", async t => {
    const parameters = {
        "V-Alue": ".%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20,2",
    };
    const value = extractLabelArrayParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, [
        ":/?#[]@!$&'()*+,;= ",
        "2",
    ]);
});

test("inject label-explode array", async t => {
    const parameters = {};
    const value = [
        ":/?#[]@!$&'()*+,;= ",
        "2",
    ];
    injectLabelExplodeArrayParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "V-Alue": ".%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20.2",
    });
});

test("extract label-explode array", async t => {
    const parameters = {
        "V-Alue": ".%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20.2",
    };
    const value = extractLabelExplodeArrayParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, [
        ":/?#[]@!$&'()*+,;= ",
        "2",
    ]);
});

test("inject label object", async t => {
    const parameters = {};
    const value = {
        str: ":/?#[]@!$&'()*+,;= ",
        num: 2,
    };
    injectLabelObjectParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "V-Alue": ".str,%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20,num,2",
    });
});

test("extract label object", async t => {
    const parameters = {
        "V-Alue": ".str,%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20,num,2",
    };
    const value = extractLabelObjectParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, {
        str: ":/?#[]@!$&'()*+,;= ",
        num: 2,
    });
});

test("inject label-explode object", async t => {
    const parameters = {};
    const value = {
        str: ":/?#[]@!$&'()*+,;= ",
        num: 2,
    };
    injectLabelExplodeObjectParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "V-Alue": ".str=%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20.num=2",
    });
});

test("extract label-explode object", async t => {
    const parameters = {
        "V-Alue": ".str=%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20.num=2",
    };
    const value = extractLabelExplodeObjectParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, {
        str: ":/?#[]@!$&'()*+,;= ",
        num: 2,
    });
});

test("inject label dictionary", async t => {
    const parameters = {};
    const value = {
        a: ":/?#[]@!$&'()*+,;= ",
        b: "2",
    };
    injectLabelDictionaryParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "V-Alue": ".a,%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20,b,2",
    });
});

test("extract label dictionary", async t => {
    const parameters = {
        "V-Alue": ".a,%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20,b,2",
    };
    const value = extractLabelDictionaryParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, {
        a: ":/?#[]@!$&'()*+,;= ",
        b: "2",
    });
});

test("inject label-explode dictionary", async t => {
    const parameters = {};
    const value = {
        a: ":/?#[]@!$&'()*+,;= ",
        b: "2",
    };
    injectLabelExplodeDictionaryParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "V-Alue": ".a=%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20.b=2",
    });
});

test("extract label-explode dictionary", async t => {
    const parameters = {
        "V-Alue": ".a=%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20.b=2",
    };
    const value = extractLabelExplodeDictionaryParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, {
        a: ":/?#[]@!$&'()*+,;= ",
        b: "2",
    });
});
