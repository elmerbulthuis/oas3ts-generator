/* eslint-disable */

import * as lib from "@oas3/oas3ts-lib";
export const specification = {
    "openapi": "3.1.0",
    "info": {
        "title": "Testing",
        "version": "v0.0.0-local"
    },
    "paths": {
        "/session-metrics/{organization}": {
            "get": {
                "operationId": "session-metrics",
                "parameters": [
                    {
                        "name": "organization",
                        "required": true,
                        "in": "path",
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "query succesful\n",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "required": [
                                        "session-count"
                                    ],
                                    "properties": {
                                        "session-count": {
                                            "type": "array",
                                            "items": {
                                                "type": "object"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        "/metrics": {
            "get": {
                "operationId": "metrics",
                "summary": "Exposes a collection of metrical information.\n",
                "responses": {
                    "200": {
                        "description": "OK",
                        "content": {
                            "text/plain": {
                                "schema": {
                                    "type": "string"
                                }
                            }
                        }
                    }
                }
            }
        },
        "/echo": {
            "post": {
                "operationId": "echo",
                "requestBody": {
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/echo-message"
                            }
                        },
                        "application/x-www-form-urlencoded": {
                            "schema": {
                                "$ref": "#/components/schemas/echo-message"
                            }
                        },
                        "text/plain": {},
                        "application/octet-stream": {}
                    }
                },
                "responses": {
                    "200": {
                        "description": "Ok",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/echo-message"
                                }
                            },
                            "application/x-www-form-urlencoded": {
                                "schema": {
                                    "$ref": "#/components/schemas/echo-message"
                                }
                            },
                            "text/plain": {},
                            "application/octet-stream": {}
                        }
                    },
                    "400": {
                        "$ref": "#/components/responses/error-400"
                    },
                    "422": {
                        "$ref": "#/components/responses/error-400"
                    }
                }
            }
        },
        "/auth": {
            "get": {
                "operationId": "basic-auth",
                "security": [
                    {
                        "basic": []
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/string/simple": {
            "get": {
                "operationId": "simple-string-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "header",
                        "required": true,
                        "style": "simple",
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/number/simple": {
            "get": {
                "operationId": "simple-number-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "header",
                        "required": true,
                        "style": "simple",
                        "schema": {
                            "type": "number"
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/array/simple": {
            "get": {
                "operationId": "simple-array-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "header",
                        "required": true,
                        "style": "simple",
                        "explode": false,
                        "schema": {
                            "type": "array",
                            "items": {
                                "type": "string"
                            }
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/array/simple-explode": {
            "get": {
                "operationId": "simple-explode-array-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "header",
                        "required": true,
                        "style": "simple",
                        "explode": true,
                        "schema": {
                            "type": "array",
                            "items": {
                                "type": "string"
                            }
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/object/simple": {
            "get": {
                "operationId": "simple-object-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "header",
                        "required": true,
                        "style": "simple",
                        "explode": false,
                        "schema": {
                            "type": "object",
                            "required": [
                                "str",
                                "num"
                            ],
                            "properties": {
                                "str": {
                                    "type": "string"
                                },
                                "num": {
                                    "type": "number"
                                }
                            }
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/object/simple-explode": {
            "get": {
                "operationId": "simple-explode-object-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "header",
                        "required": true,
                        "style": "simple",
                        "explode": true,
                        "schema": {
                            "type": "object",
                            "required": [
                                "str",
                                "num"
                            ],
                            "properties": {
                                "str": {
                                    "type": "string"
                                },
                                "num": {
                                    "type": "number"
                                }
                            }
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/dictionary/simple": {
            "get": {
                "operationId": "simple-dictionary-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "header",
                        "required": true,
                        "style": "simple",
                        "explode": false,
                        "schema": {
                            "type": "object",
                            "additionalProperties": {
                                "type": "string"
                            }
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/dictionary/simple-explode": {
            "get": {
                "operationId": "simple-explode-dictionary-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "header",
                        "required": true,
                        "style": "simple",
                        "explode": true,
                        "schema": {
                            "type": "object",
                            "additionalProperties": {
                                "type": "string"
                            }
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/string/label/{V-Alue}": {
            "get": {
                "operationId": "label-string-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "path",
                        "required": true,
                        "style": "label",
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/number/label/{V-Alue}": {
            "get": {
                "operationId": "label-number-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "path",
                        "required": true,
                        "style": "label",
                        "schema": {
                            "type": "number"
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/array/label/{V-Alue}": {
            "get": {
                "operationId": "label-array-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "path",
                        "required": true,
                        "style": "label",
                        "explode": false,
                        "schema": {
                            "type": "array",
                            "items": {
                                "type": "string"
                            }
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/array/label-explode/{V-Alue}": {
            "get": {
                "operationId": "label-explode-array-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "path",
                        "required": true,
                        "style": "label",
                        "explode": true,
                        "schema": {
                            "type": "array",
                            "items": {
                                "type": "string"
                            }
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/object/label/{V-Alue}": {
            "get": {
                "operationId": "label-object-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "path",
                        "required": true,
                        "style": "label",
                        "explode": false,
                        "schema": {
                            "type": "object",
                            "required": [
                                "str",
                                "num"
                            ],
                            "properties": {
                                "str": {
                                    "type": "string"
                                },
                                "num": {
                                    "type": "number"
                                }
                            }
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/object/label-explode/{V-Alue}": {
            "get": {
                "operationId": "label-explode-object-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "path",
                        "required": true,
                        "style": "label",
                        "explode": true,
                        "schema": {
                            "type": "object",
                            "required": [
                                "str",
                                "num"
                            ],
                            "properties": {
                                "str": {
                                    "type": "string"
                                },
                                "num": {
                                    "type": "number"
                                }
                            }
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/dictionary/label/{V-Alue}": {
            "get": {
                "operationId": "label-dictionary-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "path",
                        "required": true,
                        "style": "label",
                        "explode": false,
                        "schema": {
                            "type": "object",
                            "additionalProperties": {
                                "type": "string"
                            }
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/dictionary/label-explode/{V-Alue}": {
            "get": {
                "operationId": "label-explode-dictionary-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "path",
                        "required": true,
                        "style": "label",
                        "explode": true,
                        "schema": {
                            "type": "object",
                            "additionalProperties": {
                                "type": "string"
                            }
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/string/form": {
            "get": {
                "operationId": "form-string-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "query",
                        "required": true,
                        "style": "form",
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/number/form": {
            "get": {
                "operationId": "form-number-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "query",
                        "required": true,
                        "style": "form",
                        "schema": {
                            "type": "number"
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/array/form": {
            "get": {
                "operationId": "form-array-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "query",
                        "required": true,
                        "style": "form",
                        "explode": false,
                        "schema": {
                            "type": "array",
                            "items": {
                                "type": "string"
                            }
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/array/form-explode": {
            "get": {
                "operationId": "form-explode-array-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "query",
                        "required": true,
                        "style": "form",
                        "explode": true,
                        "schema": {
                            "type": "array",
                            "items": {
                                "type": "string"
                            }
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/object/form": {
            "get": {
                "operationId": "form-object-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "query",
                        "required": true,
                        "style": "form",
                        "explode": false,
                        "schema": {
                            "type": "object",
                            "required": [
                                "str",
                                "num"
                            ],
                            "properties": {
                                "str": {
                                    "type": "string"
                                },
                                "num": {
                                    "type": "number"
                                }
                            }
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/object/form-explode": {
            "get": {
                "operationId": "form-explode-object-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "query",
                        "required": true,
                        "style": "form",
                        "explode": true,
                        "schema": {
                            "type": "object",
                            "required": [
                                "str",
                                "num"
                            ],
                            "properties": {
                                "str": {
                                    "type": "string"
                                },
                                "num": {
                                    "type": "number"
                                }
                            }
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/dictionary/form": {
            "get": {
                "operationId": "form-dictionary-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "query",
                        "required": true,
                        "style": "form",
                        "explode": false,
                        "schema": {
                            "type": "object",
                            "additionalProperties": {
                                "type": "string"
                            }
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/dictionary/form-explode": {
            "get": {
                "operationId": "form-explode-dictionary-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "query",
                        "required": true,
                        "style": "form",
                        "explode": true,
                        "schema": {
                            "type": "object",
                            "additionalProperties": {
                                "type": "string"
                            }
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/object/deep-object": {
            "get": {
                "operationId": "deep-object-object-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "query",
                        "required": true,
                        "style": "deepObject",
                        "schema": {
                            "type": "object",
                            "required": [
                                "str",
                                "num"
                            ],
                            "properties": {
                                "str": {
                                    "type": "string"
                                },
                                "num": {
                                    "type": "number"
                                }
                            }
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/parameter/dictionary/deep-object": {
            "get": {
                "operationId": "deep-object-dictionary-parameter",
                "parameters": [
                    {
                        "name": "V-Alue",
                        "in": "query",
                        "required": true,
                        "style": "deepObject",
                        "schema": {
                            "type": "object",
                            "additionalProperties": {
                                "type": "string"
                            }
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No content"
                    }
                }
            }
        },
        "/unsupported": {
            "post": {
                "operationId": "unsupported-request",
                "requestBody": {
                    "required": true,
                    "content": {
                        "unsupported/content": {}
                    }
                },
                "responses": {
                    "204": {
                        "description": "no content"
                    }
                }
            },
            "get": {
                "operationId": "unsupported-response",
                "responses": {
                    "200": {
                        "description": "ok",
                        "content": {
                            "unsupported/content": {}
                        }
                    }
                }
            }
        },
        "/unsupported-but-not-requires": {
            "post": {
                "operationId": "unsupported-but-not-required-request",
                "requestBody": {
                    "required": false,
                    "content": {
                        "unsupported/content": {}
                    }
                },
                "responses": {
                    "204": {
                        "description": "no content"
                    }
                }
            }
        }
    },
    "components": {
        "securitySchemes": {
            "basic": {
                "type": "http",
                "scheme": "basic"
            },
            "api-token": {
                "type": "http",
                "scheme": "bearer",
                "bearerFormat": "token"
            },
            "ApiKey": {
                "type": "apiKey",
                "name": "Authorization",
                "in": "header"
            },
            "access-token": {
                "type": "apiKey",
                "name": "access_token",
                "in": "query"
            }
        },
        "responses": {
            "error-400": {
                "description": "when parameters are missing or incorrect.\n",
                "content": {
                    "application/json": {
                        "schema": {
                            "$ref": "#/components/schemas/error-result"
                        }
                    }
                }
            }
        },
        "schemas": {
            "echo-message": {
                "type": "object",
                "additionalProperties": false,
                "required": [
                    "message"
                ],
                "properties": {
                    "message": {
                        "type": "string"
                    }
                }
            },
            "string-or-number": {
                "oneOf": [
                    {
                        "type": "string"
                    },
                    {
                        "type": "number"
                    }
                ]
            },
            "ip": {
                "oneOf": [
                    {
                        "type": "string",
                        "format": "ipv4"
                    },
                    {
                        "type": "string",
                        "format": "ipv6"
                    }
                ]
            },
            "tuple": {
                "type": "array",
                "items": [
                    {
                        "type": "number"
                    },
                    {
                        "type": "string"
                    }
                ]
            },
            "error-result": {
                "type": "object",
                "additionalProperties": false,
                "required": [
                    "error"
                ],
                "properties": {
                    "error": {
                        "type": "string"
                    }
                }
            }
        }
    }
};
export const metadata: lib.Metadata = {
    "operationsMap": {
        "/session-metrics/{organization}": {
            "GET": "session-metrics"
        },
        "/metrics": {
            "GET": "metrics"
        },
        "/echo": {
            "POST": "echo"
        },
        "/auth": {
            "GET": "basic-auth"
        },
        "/parameter/string/simple": {
            "GET": "simple-string-parameter"
        },
        "/parameter/number/simple": {
            "GET": "simple-number-parameter"
        },
        "/parameter/array/simple": {
            "GET": "simple-array-parameter"
        },
        "/parameter/array/simple-explode": {
            "GET": "simple-explode-array-parameter"
        },
        "/parameter/object/simple": {
            "GET": "simple-object-parameter"
        },
        "/parameter/object/simple-explode": {
            "GET": "simple-explode-object-parameter"
        },
        "/parameter/dictionary/simple": {
            "GET": "simple-dictionary-parameter"
        },
        "/parameter/dictionary/simple-explode": {
            "GET": "simple-explode-dictionary-parameter"
        },
        "/parameter/string/label/{V-Alue}": {
            "GET": "label-string-parameter"
        },
        "/parameter/number/label/{V-Alue}": {
            "GET": "label-number-parameter"
        },
        "/parameter/array/label/{V-Alue}": {
            "GET": "label-array-parameter"
        },
        "/parameter/array/label-explode/{V-Alue}": {
            "GET": "label-explode-array-parameter"
        },
        "/parameter/object/label/{V-Alue}": {
            "GET": "label-object-parameter"
        },
        "/parameter/object/label-explode/{V-Alue}": {
            "GET": "label-explode-object-parameter"
        },
        "/parameter/dictionary/label/{V-Alue}": {
            "GET": "label-dictionary-parameter"
        },
        "/parameter/dictionary/label-explode/{V-Alue}": {
            "GET": "label-explode-dictionary-parameter"
        },
        "/parameter/string/form": {
            "GET": "form-string-parameter"
        },
        "/parameter/number/form": {
            "GET": "form-number-parameter"
        },
        "/parameter/array/form": {
            "GET": "form-array-parameter"
        },
        "/parameter/array/form-explode": {
            "GET": "form-explode-array-parameter"
        },
        "/parameter/object/form": {
            "GET": "form-object-parameter"
        },
        "/parameter/object/form-explode": {
            "GET": "form-explode-object-parameter"
        },
        "/parameter/dictionary/form": {
            "GET": "form-dictionary-parameter"
        },
        "/parameter/dictionary/form-explode": {
            "GET": "form-explode-dictionary-parameter"
        },
        "/parameter/object/deep-object": {
            "GET": "deep-object-object-parameter"
        },
        "/parameter/dictionary/deep-object": {
            "GET": "deep-object-dictionary-parameter"
        },
        "/unsupported": {
            "GET": "unsupported-response",
            "POST": "unsupported-request"
        },
        "/unsupported-but-not-requires": {
            "POST": "unsupported-but-not-required-request"
        }
    },
    "availableRequestTypes": [
        "application/json",
        "application/x-www-form-urlencoded",
        "text/plain",
        "application/octet-stream"
    ],
    "availableResponseTypes": [
        "application/json",
        "application/x-www-form-urlencoded",
        "text/plain",
        "application/octet-stream"
    ],
    "requestHeadersMap": {
        "session-metrics": [
            "accept"
        ],
        "metrics": [
            "accept"
        ],
        "echo": [
            "content-type",
            "accept"
        ],
        "basic-auth": [
            "authorization"
        ],
        "simple-string-parameter": [
            "V-Alue"
        ],
        "simple-number-parameter": [
            "V-Alue"
        ],
        "simple-array-parameter": [
            "V-Alue"
        ],
        "simple-explode-array-parameter": [
            "V-Alue"
        ],
        "simple-object-parameter": [
            "V-Alue"
        ],
        "simple-explode-object-parameter": [
            "V-Alue"
        ],
        "simple-dictionary-parameter": [
            "V-Alue"
        ],
        "simple-explode-dictionary-parameter": [
            "V-Alue"
        ],
        "label-string-parameter": [],
        "label-number-parameter": [],
        "label-array-parameter": [],
        "label-explode-array-parameter": [],
        "label-object-parameter": [],
        "label-explode-object-parameter": [],
        "label-dictionary-parameter": [],
        "label-explode-dictionary-parameter": [],
        "form-string-parameter": [],
        "form-number-parameter": [],
        "form-array-parameter": [],
        "form-explode-array-parameter": [],
        "form-object-parameter": [],
        "form-explode-object-parameter": [],
        "form-dictionary-parameter": [],
        "form-explode-dictionary-parameter": [],
        "deep-object-object-parameter": [],
        "deep-object-dictionary-parameter": [],
        "unsupported-response": [],
        "unsupported-request": [],
        "unsupported-but-not-required-request": []
    },
    "responseHeadersMap": {
        "session-metrics": [
            "content-type"
        ],
        "metrics": [
            "content-type"
        ],
        "echo": [
            "content-type"
        ],
        "basic-auth": [],
        "simple-string-parameter": [],
        "simple-number-parameter": [],
        "simple-array-parameter": [],
        "simple-explode-array-parameter": [],
        "simple-object-parameter": [],
        "simple-explode-object-parameter": [],
        "simple-dictionary-parameter": [],
        "simple-explode-dictionary-parameter": [],
        "label-string-parameter": [],
        "label-number-parameter": [],
        "label-array-parameter": [],
        "label-explode-array-parameter": [],
        "label-object-parameter": [],
        "label-explode-object-parameter": [],
        "label-dictionary-parameter": [],
        "label-explode-dictionary-parameter": [],
        "form-string-parameter": [],
        "form-number-parameter": [],
        "form-array-parameter": [],
        "form-explode-array-parameter": [],
        "form-object-parameter": [],
        "form-explode-object-parameter": [],
        "form-dictionary-parameter": [],
        "form-explode-dictionary-parameter": [],
        "deep-object-object-parameter": [],
        "deep-object-dictionary-parameter": [],
        "unsupported-response": [],
        "unsupported-request": [],
        "unsupported-but-not-required-request": []
    }
};
export const sessionMetricsAcceptTypes = new Set<SessionMetricsAcceptTypes>(["application/json"]);
export const metricsAcceptTypes = new Set<MetricsAcceptTypes>(["text/plain"]);
export const echoAcceptTypes = new Set<EchoAcceptTypes>(["application/json", "application/x-www-form-urlencoded", "text/plain", "application/octet-stream"]);
export const basicAuthAcceptTypes = new Set<BasicAuthAcceptTypes>([]);
export const simpleStringParameterAcceptTypes = new Set<SimpleStringParameterAcceptTypes>([]);
export const simpleNumberParameterAcceptTypes = new Set<SimpleNumberParameterAcceptTypes>([]);
export const simpleArrayParameterAcceptTypes = new Set<SimpleArrayParameterAcceptTypes>([]);
export const simpleExplodeArrayParameterAcceptTypes = new Set<SimpleExplodeArrayParameterAcceptTypes>([]);
export const simpleObjectParameterAcceptTypes = new Set<SimpleObjectParameterAcceptTypes>([]);
export const simpleExplodeObjectParameterAcceptTypes = new Set<SimpleExplodeObjectParameterAcceptTypes>([]);
export const simpleDictionaryParameterAcceptTypes = new Set<SimpleDictionaryParameterAcceptTypes>([]);
export const simpleExplodeDictionaryParameterAcceptTypes = new Set<SimpleExplodeDictionaryParameterAcceptTypes>([]);
export const labelStringParameterAcceptTypes = new Set<LabelStringParameterAcceptTypes>([]);
export const labelNumberParameterAcceptTypes = new Set<LabelNumberParameterAcceptTypes>([]);
export const labelArrayParameterAcceptTypes = new Set<LabelArrayParameterAcceptTypes>([]);
export const labelExplodeArrayParameterAcceptTypes = new Set<LabelExplodeArrayParameterAcceptTypes>([]);
export const labelObjectParameterAcceptTypes = new Set<LabelObjectParameterAcceptTypes>([]);
export const labelExplodeObjectParameterAcceptTypes = new Set<LabelExplodeObjectParameterAcceptTypes>([]);
export const labelDictionaryParameterAcceptTypes = new Set<LabelDictionaryParameterAcceptTypes>([]);
export const labelExplodeDictionaryParameterAcceptTypes = new Set<LabelExplodeDictionaryParameterAcceptTypes>([]);
export const formStringParameterAcceptTypes = new Set<FormStringParameterAcceptTypes>([]);
export const formNumberParameterAcceptTypes = new Set<FormNumberParameterAcceptTypes>([]);
export const formArrayParameterAcceptTypes = new Set<FormArrayParameterAcceptTypes>([]);
export const formExplodeArrayParameterAcceptTypes = new Set<FormExplodeArrayParameterAcceptTypes>([]);
export const formObjectParameterAcceptTypes = new Set<FormObjectParameterAcceptTypes>([]);
export const formExplodeObjectParameterAcceptTypes = new Set<FormExplodeObjectParameterAcceptTypes>([]);
export const formDictionaryParameterAcceptTypes = new Set<FormDictionaryParameterAcceptTypes>([]);
export const formExplodeDictionaryParameterAcceptTypes = new Set<FormExplodeDictionaryParameterAcceptTypes>([]);
export const deepObjectObjectParameterAcceptTypes = new Set<DeepObjectObjectParameterAcceptTypes>([]);
export const deepObjectDictionaryParameterAcceptTypes = new Set<DeepObjectDictionaryParameterAcceptTypes>([]);
export const unsupportedResponseAcceptTypes = new Set<UnsupportedResponseAcceptTypes>([]);
export const unsupportedRequestAcceptTypes = new Set<UnsupportedRequestAcceptTypes>([]);
export const unsupportedButNotRequiredRequestAcceptTypes = new Set<UnsupportedButNotRequiredRequestAcceptTypes>([]);
export interface BasicCredential {
    username: string;
    password: string;
}
export type ApiTokenCredential = string;
export type ApiKeyCredential = string;
export type AccessTokenCredential = string;
export type SessionMetricsAcceptTypes = "application/json";
export type MetricsAcceptTypes = "text/plain";
export type EchoAcceptTypes = "application/json" | "application/x-www-form-urlencoded" | "text/plain" | "application/octet-stream";
export type BasicAuthAcceptTypes = never;
export type SimpleStringParameterAcceptTypes = never;
export type SimpleNumberParameterAcceptTypes = never;
export type SimpleArrayParameterAcceptTypes = never;
export type SimpleExplodeArrayParameterAcceptTypes = never;
export type SimpleObjectParameterAcceptTypes = never;
export type SimpleExplodeObjectParameterAcceptTypes = never;
export type SimpleDictionaryParameterAcceptTypes = never;
export type SimpleExplodeDictionaryParameterAcceptTypes = never;
export type LabelStringParameterAcceptTypes = never;
export type LabelNumberParameterAcceptTypes = never;
export type LabelArrayParameterAcceptTypes = never;
export type LabelExplodeArrayParameterAcceptTypes = never;
export type LabelObjectParameterAcceptTypes = never;
export type LabelExplodeObjectParameterAcceptTypes = never;
export type LabelDictionaryParameterAcceptTypes = never;
export type LabelExplodeDictionaryParameterAcceptTypes = never;
export type FormStringParameterAcceptTypes = never;
export type FormNumberParameterAcceptTypes = never;
export type FormArrayParameterAcceptTypes = never;
export type FormExplodeArrayParameterAcceptTypes = never;
export type FormObjectParameterAcceptTypes = never;
export type FormExplodeObjectParameterAcceptTypes = never;
export type FormDictionaryParameterAcceptTypes = never;
export type FormExplodeDictionaryParameterAcceptTypes = never;
export type DeepObjectObjectParameterAcceptTypes = never;
export type DeepObjectDictionaryParameterAcceptTypes = never;
export type UnsupportedResponseAcceptTypes = never;
export type UnsupportedRequestAcceptTypes = never;
export type UnsupportedButNotRequiredRequestAcceptTypes = never;
export type SessionMetricsOrganizationParameterSchema = string;
export interface SessionMetrics200ApplicationJsonResponseSchema {
    "session-count": SessionMetrics200ApplicationJsonResponseSessionCountSchema;
}
export type SessionMetrics200ApplicationJsonResponseSessionCountSchema = Array<SessionMetrics200ApplicationJsonResponseSessionCountItemsSchema>;
export type SessionMetrics200ApplicationJsonResponseSessionCountItemsSchema = any;
export type Metrics200TextPlainResponseSchema = string;
export type SimpleStringParameterVAlueParameterSchema = string;
export type SimpleNumberParameterVAlueParameterSchema = number;
export type SimpleArrayParameterVAlueParameterSchema = Array<SimpleArrayParameterVAlueParameterItemsSchema>;
export type SimpleArrayParameterVAlueParameterItemsSchema = string;
export type SimpleExplodeArrayParameterVAlueParameterSchema = Array<SimpleExplodeArrayParameterVAlueParameterItemsSchema>;
export type SimpleExplodeArrayParameterVAlueParameterItemsSchema = string;
export interface SimpleObjectParameterVAlueParameterSchema {
    "str": SimpleObjectParameterVAlueParameterStrSchema;
    "num": SimpleObjectParameterVAlueParameterNumSchema;
}
export type SimpleObjectParameterVAlueParameterStrSchema = string;
export type SimpleObjectParameterVAlueParameterNumSchema = number;
export interface SimpleExplodeObjectParameterVAlueParameterSchema {
    "str": SimpleExplodeObjectParameterVAlueParameterStrSchema;
    "num": SimpleExplodeObjectParameterVAlueParameterNumSchema;
}
export type SimpleExplodeObjectParameterVAlueParameterStrSchema = string;
export type SimpleExplodeObjectParameterVAlueParameterNumSchema = number;
export type SimpleDictionaryParameterVAlueParameterSchema = Record<string, SimpleDictionaryParameterVAlueParameterAdditionalPropertiesSchema>;
export type SimpleDictionaryParameterVAlueParameterAdditionalPropertiesSchema = string;
export type SimpleExplodeDictionaryParameterVAlueParameterSchema = Record<string, SimpleExplodeDictionaryParameterVAlueParameterAdditionalPropertiesSchema>;
export type SimpleExplodeDictionaryParameterVAlueParameterAdditionalPropertiesSchema = string;
export type LabelStringParameterVAlueParameterSchema = string;
export type LabelNumberParameterVAlueParameterSchema = number;
export type LabelArrayParameterVAlueParameterSchema = Array<LabelArrayParameterVAlueParameterItemsSchema>;
export type LabelArrayParameterVAlueParameterItemsSchema = string;
export type LabelExplodeArrayParameterVAlueParameterSchema = Array<LabelExplodeArrayParameterVAlueParameterItemsSchema>;
export type LabelExplodeArrayParameterVAlueParameterItemsSchema = string;
export interface LabelObjectParameterVAlueParameterSchema {
    "str": LabelObjectParameterVAlueParameterStrSchema;
    "num": LabelObjectParameterVAlueParameterNumSchema;
}
export type LabelObjectParameterVAlueParameterStrSchema = string;
export type LabelObjectParameterVAlueParameterNumSchema = number;
export interface LabelExplodeObjectParameterVAlueParameterSchema {
    "str": LabelExplodeObjectParameterVAlueParameterStrSchema;
    "num": LabelExplodeObjectParameterVAlueParameterNumSchema;
}
export type LabelExplodeObjectParameterVAlueParameterStrSchema = string;
export type LabelExplodeObjectParameterVAlueParameterNumSchema = number;
export type LabelDictionaryParameterVAlueParameterSchema = Record<string, LabelDictionaryParameterVAlueParameterAdditionalPropertiesSchema>;
export type LabelDictionaryParameterVAlueParameterAdditionalPropertiesSchema = string;
export type LabelExplodeDictionaryParameterVAlueParameterSchema = Record<string, LabelExplodeDictionaryParameterVAlueParameterAdditionalPropertiesSchema>;
export type LabelExplodeDictionaryParameterVAlueParameterAdditionalPropertiesSchema = string;
export type FormStringParameterVAlueParameterSchema = string;
export type FormNumberParameterVAlueParameterSchema = number;
export type FormArrayParameterVAlueParameterSchema = Array<FormArrayParameterVAlueParameterItemsSchema>;
export type FormArrayParameterVAlueParameterItemsSchema = string;
export type FormExplodeArrayParameterVAlueParameterSchema = Array<FormExplodeArrayParameterVAlueParameterItemsSchema>;
export type FormExplodeArrayParameterVAlueParameterItemsSchema = string;
export interface FormObjectParameterVAlueParameterSchema {
    "str": FormObjectParameterVAlueParameterStrSchema;
    "num": FormObjectParameterVAlueParameterNumSchema;
}
export type FormObjectParameterVAlueParameterStrSchema = string;
export type FormObjectParameterVAlueParameterNumSchema = number;
export interface FormExplodeObjectParameterVAlueParameterSchema {
    "str": FormExplodeObjectParameterVAlueParameterStrSchema;
    "num": FormExplodeObjectParameterVAlueParameterNumSchema;
}
export type FormExplodeObjectParameterVAlueParameterStrSchema = string;
export type FormExplodeObjectParameterVAlueParameterNumSchema = number;
export type FormDictionaryParameterVAlueParameterSchema = Record<string, FormDictionaryParameterVAlueParameterAdditionalPropertiesSchema>;
export type FormDictionaryParameterVAlueParameterAdditionalPropertiesSchema = string;
export type FormExplodeDictionaryParameterVAlueParameterSchema = Record<string, FormExplodeDictionaryParameterVAlueParameterAdditionalPropertiesSchema>;
export type FormExplodeDictionaryParameterVAlueParameterAdditionalPropertiesSchema = string;
export interface DeepObjectObjectParameterVAlueParameterSchema {
    "str": DeepObjectObjectParameterVAlueParameterStrSchema;
    "num": DeepObjectObjectParameterVAlueParameterNumSchema;
}
export type DeepObjectObjectParameterVAlueParameterStrSchema = string;
export type DeepObjectObjectParameterVAlueParameterNumSchema = number;
export type DeepObjectDictionaryParameterVAlueParameterSchema = Record<string, DeepObjectDictionaryParameterVAlueParameterAdditionalPropertiesSchema>;
export type DeepObjectDictionaryParameterVAlueParameterAdditionalPropertiesSchema = string;
export interface EchoMessageSchema {
    "message": EchoMessageMessageSchema;
}
export type EchoMessageMessageSchema = string;
export type StringOrNumberSchema = StringOrNumberOneOf0Schema | StringOrNumberOneOf1Schema;
export type StringOrNumberOneOf0Schema = string;
export type StringOrNumberOneOf1Schema = number;
export type IpSchema = IpOneOf0Schema | IpOneOf1Schema;
export type IpOneOf0Schema = string;
export type IpOneOf1Schema = string;
export type TupleSchema = [
    TupleItems0Schema,
    TupleItems1Schema
];
export type TupleItems0Schema = number;
export type TupleItems1Schema = string;
export interface ErrorResultSchema {
    "error": ErrorResultErrorSchema;
}
export type ErrorResultErrorSchema = string;
export function* validateSessionMetricsOrganizationParameterSchema(model: SessionMetricsOrganizationParameterSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateSessionMetrics200ApplicationJsonResponseSchema(model: SessionMetrics200ApplicationJsonResponseSchema, path: string[] = []) {
    if (!(lib.validateType(model, "object") && lib.validateRequired(model, [
        "session-count"
    ]))) {
        yield path;
        return;
    }
    if (model["session-count"] != null) {
        yield* validateSessionMetrics200ApplicationJsonResponseSessionCountSchema(model["session-count"], [...path, "session-count"]);
    }
}
validateSessionMetrics200ApplicationJsonResponseSchema.properties = {
    "session-count": validateSessionMetrics200ApplicationJsonResponseSessionCountSchema
};
export function* validateSessionMetrics200ApplicationJsonResponseSessionCountSchema(model: SessionMetrics200ApplicationJsonResponseSessionCountSchema, path: string[] = []) {
    if (!lib.validateType(model, "array")) {
        yield path;
        return;
    }
    for (const [key, value] of Object.entries(model)) {
        yield* validateSessionMetrics200ApplicationJsonResponseSessionCountItemsSchema(value, [...path, key]);
    }
}
validateSessionMetrics200ApplicationJsonResponseSessionCountSchema.items = validateSessionMetrics200ApplicationJsonResponseSessionCountItemsSchema;
export function* validateSessionMetrics200ApplicationJsonResponseSessionCountItemsSchema(model: SessionMetrics200ApplicationJsonResponseSessionCountItemsSchema, path: string[] = []) {
    if (!lib.validateType(model, "object")) {
        yield path;
        return;
    }
}
export function* validateMetrics200TextPlainResponseSchema(model: Metrics200TextPlainResponseSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateSimpleStringParameterVAlueParameterSchema(model: SimpleStringParameterVAlueParameterSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateSimpleNumberParameterVAlueParameterSchema(model: SimpleNumberParameterVAlueParameterSchema, path: string[] = []) {
    if (!lib.validateType(model, "number")) {
        yield path;
        return;
    }
}
export function* validateSimpleArrayParameterVAlueParameterSchema(model: SimpleArrayParameterVAlueParameterSchema, path: string[] = []) {
    if (!lib.validateType(model, "array")) {
        yield path;
        return;
    }
    for (const [key, value] of Object.entries(model)) {
        yield* validateSimpleArrayParameterVAlueParameterItemsSchema(value, [...path, key]);
    }
}
validateSimpleArrayParameterVAlueParameterSchema.items = validateSimpleArrayParameterVAlueParameterItemsSchema;
export function* validateSimpleArrayParameterVAlueParameterItemsSchema(model: SimpleArrayParameterVAlueParameterItemsSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateSimpleExplodeArrayParameterVAlueParameterSchema(model: SimpleExplodeArrayParameterVAlueParameterSchema, path: string[] = []) {
    if (!lib.validateType(model, "array")) {
        yield path;
        return;
    }
    for (const [key, value] of Object.entries(model)) {
        yield* validateSimpleExplodeArrayParameterVAlueParameterItemsSchema(value, [...path, key]);
    }
}
validateSimpleExplodeArrayParameterVAlueParameterSchema.items = validateSimpleExplodeArrayParameterVAlueParameterItemsSchema;
export function* validateSimpleExplodeArrayParameterVAlueParameterItemsSchema(model: SimpleExplodeArrayParameterVAlueParameterItemsSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateSimpleObjectParameterVAlueParameterSchema(model: SimpleObjectParameterVAlueParameterSchema, path: string[] = []) {
    if (!(lib.validateType(model, "object") && lib.validateRequired(model, [
        "str",
        "num"
    ]))) {
        yield path;
        return;
    }
    if (model["str"] != null) {
        yield* validateSimpleObjectParameterVAlueParameterStrSchema(model["str"], [...path, "str"]);
    }
    if (model["num"] != null) {
        yield* validateSimpleObjectParameterVAlueParameterNumSchema(model["num"], [...path, "num"]);
    }
}
validateSimpleObjectParameterVAlueParameterSchema.properties = {
    "str": validateSimpleObjectParameterVAlueParameterStrSchema,
    "num": validateSimpleObjectParameterVAlueParameterNumSchema
};
export function* validateSimpleObjectParameterVAlueParameterStrSchema(model: SimpleObjectParameterVAlueParameterStrSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateSimpleObjectParameterVAlueParameterNumSchema(model: SimpleObjectParameterVAlueParameterNumSchema, path: string[] = []) {
    if (!lib.validateType(model, "number")) {
        yield path;
        return;
    }
}
export function* validateSimpleExplodeObjectParameterVAlueParameterSchema(model: SimpleExplodeObjectParameterVAlueParameterSchema, path: string[] = []) {
    if (!(lib.validateType(model, "object") && lib.validateRequired(model, [
        "str",
        "num"
    ]))) {
        yield path;
        return;
    }
    if (model["str"] != null) {
        yield* validateSimpleExplodeObjectParameterVAlueParameterStrSchema(model["str"], [...path, "str"]);
    }
    if (model["num"] != null) {
        yield* validateSimpleExplodeObjectParameterVAlueParameterNumSchema(model["num"], [...path, "num"]);
    }
}
validateSimpleExplodeObjectParameterVAlueParameterSchema.properties = {
    "str": validateSimpleExplodeObjectParameterVAlueParameterStrSchema,
    "num": validateSimpleExplodeObjectParameterVAlueParameterNumSchema
};
export function* validateSimpleExplodeObjectParameterVAlueParameterStrSchema(model: SimpleExplodeObjectParameterVAlueParameterStrSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateSimpleExplodeObjectParameterVAlueParameterNumSchema(model: SimpleExplodeObjectParameterVAlueParameterNumSchema, path: string[] = []) {
    if (!lib.validateType(model, "number")) {
        yield path;
        return;
    }
}
export function* validateSimpleDictionaryParameterVAlueParameterSchema(model: SimpleDictionaryParameterVAlueParameterSchema, path: string[] = []) {
    if (!lib.validateType(model, "object")) {
        yield path;
        return;
    }
    for (const [key, value] of Object.entries(model)) {
        yield* validateSimpleDictionaryParameterVAlueParameterAdditionalPropertiesSchema(value, [...path, key]);
    }
}
validateSimpleDictionaryParameterVAlueParameterSchema.additionalProperties = validateSimpleDictionaryParameterVAlueParameterAdditionalPropertiesSchema;
export function* validateSimpleDictionaryParameterVAlueParameterAdditionalPropertiesSchema(model: SimpleDictionaryParameterVAlueParameterAdditionalPropertiesSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateSimpleExplodeDictionaryParameterVAlueParameterSchema(model: SimpleExplodeDictionaryParameterVAlueParameterSchema, path: string[] = []) {
    if (!lib.validateType(model, "object")) {
        yield path;
        return;
    }
    for (const [key, value] of Object.entries(model)) {
        yield* validateSimpleExplodeDictionaryParameterVAlueParameterAdditionalPropertiesSchema(value, [...path, key]);
    }
}
validateSimpleExplodeDictionaryParameterVAlueParameterSchema.additionalProperties = validateSimpleExplodeDictionaryParameterVAlueParameterAdditionalPropertiesSchema;
export function* validateSimpleExplodeDictionaryParameterVAlueParameterAdditionalPropertiesSchema(model: SimpleExplodeDictionaryParameterVAlueParameterAdditionalPropertiesSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateLabelStringParameterVAlueParameterSchema(model: LabelStringParameterVAlueParameterSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateLabelNumberParameterVAlueParameterSchema(model: LabelNumberParameterVAlueParameterSchema, path: string[] = []) {
    if (!lib.validateType(model, "number")) {
        yield path;
        return;
    }
}
export function* validateLabelArrayParameterVAlueParameterSchema(model: LabelArrayParameterVAlueParameterSchema, path: string[] = []) {
    if (!lib.validateType(model, "array")) {
        yield path;
        return;
    }
    for (const [key, value] of Object.entries(model)) {
        yield* validateLabelArrayParameterVAlueParameterItemsSchema(value, [...path, key]);
    }
}
validateLabelArrayParameterVAlueParameterSchema.items = validateLabelArrayParameterVAlueParameterItemsSchema;
export function* validateLabelArrayParameterVAlueParameterItemsSchema(model: LabelArrayParameterVAlueParameterItemsSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateLabelExplodeArrayParameterVAlueParameterSchema(model: LabelExplodeArrayParameterVAlueParameterSchema, path: string[] = []) {
    if (!lib.validateType(model, "array")) {
        yield path;
        return;
    }
    for (const [key, value] of Object.entries(model)) {
        yield* validateLabelExplodeArrayParameterVAlueParameterItemsSchema(value, [...path, key]);
    }
}
validateLabelExplodeArrayParameterVAlueParameterSchema.items = validateLabelExplodeArrayParameterVAlueParameterItemsSchema;
export function* validateLabelExplodeArrayParameterVAlueParameterItemsSchema(model: LabelExplodeArrayParameterVAlueParameterItemsSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateLabelObjectParameterVAlueParameterSchema(model: LabelObjectParameterVAlueParameterSchema, path: string[] = []) {
    if (!(lib.validateType(model, "object") && lib.validateRequired(model, [
        "str",
        "num"
    ]))) {
        yield path;
        return;
    }
    if (model["str"] != null) {
        yield* validateLabelObjectParameterVAlueParameterStrSchema(model["str"], [...path, "str"]);
    }
    if (model["num"] != null) {
        yield* validateLabelObjectParameterVAlueParameterNumSchema(model["num"], [...path, "num"]);
    }
}
validateLabelObjectParameterVAlueParameterSchema.properties = {
    "str": validateLabelObjectParameterVAlueParameterStrSchema,
    "num": validateLabelObjectParameterVAlueParameterNumSchema
};
export function* validateLabelObjectParameterVAlueParameterStrSchema(model: LabelObjectParameterVAlueParameterStrSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateLabelObjectParameterVAlueParameterNumSchema(model: LabelObjectParameterVAlueParameterNumSchema, path: string[] = []) {
    if (!lib.validateType(model, "number")) {
        yield path;
        return;
    }
}
export function* validateLabelExplodeObjectParameterVAlueParameterSchema(model: LabelExplodeObjectParameterVAlueParameterSchema, path: string[] = []) {
    if (!(lib.validateType(model, "object") && lib.validateRequired(model, [
        "str",
        "num"
    ]))) {
        yield path;
        return;
    }
    if (model["str"] != null) {
        yield* validateLabelExplodeObjectParameterVAlueParameterStrSchema(model["str"], [...path, "str"]);
    }
    if (model["num"] != null) {
        yield* validateLabelExplodeObjectParameterVAlueParameterNumSchema(model["num"], [...path, "num"]);
    }
}
validateLabelExplodeObjectParameterVAlueParameterSchema.properties = {
    "str": validateLabelExplodeObjectParameterVAlueParameterStrSchema,
    "num": validateLabelExplodeObjectParameterVAlueParameterNumSchema
};
export function* validateLabelExplodeObjectParameterVAlueParameterStrSchema(model: LabelExplodeObjectParameterVAlueParameterStrSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateLabelExplodeObjectParameterVAlueParameterNumSchema(model: LabelExplodeObjectParameterVAlueParameterNumSchema, path: string[] = []) {
    if (!lib.validateType(model, "number")) {
        yield path;
        return;
    }
}
export function* validateLabelDictionaryParameterVAlueParameterSchema(model: LabelDictionaryParameterVAlueParameterSchema, path: string[] = []) {
    if (!lib.validateType(model, "object")) {
        yield path;
        return;
    }
    for (const [key, value] of Object.entries(model)) {
        yield* validateLabelDictionaryParameterVAlueParameterAdditionalPropertiesSchema(value, [...path, key]);
    }
}
validateLabelDictionaryParameterVAlueParameterSchema.additionalProperties = validateLabelDictionaryParameterVAlueParameterAdditionalPropertiesSchema;
export function* validateLabelDictionaryParameterVAlueParameterAdditionalPropertiesSchema(model: LabelDictionaryParameterVAlueParameterAdditionalPropertiesSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateLabelExplodeDictionaryParameterVAlueParameterSchema(model: LabelExplodeDictionaryParameterVAlueParameterSchema, path: string[] = []) {
    if (!lib.validateType(model, "object")) {
        yield path;
        return;
    }
    for (const [key, value] of Object.entries(model)) {
        yield* validateLabelExplodeDictionaryParameterVAlueParameterAdditionalPropertiesSchema(value, [...path, key]);
    }
}
validateLabelExplodeDictionaryParameterVAlueParameterSchema.additionalProperties = validateLabelExplodeDictionaryParameterVAlueParameterAdditionalPropertiesSchema;
export function* validateLabelExplodeDictionaryParameterVAlueParameterAdditionalPropertiesSchema(model: LabelExplodeDictionaryParameterVAlueParameterAdditionalPropertiesSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateFormStringParameterVAlueParameterSchema(model: FormStringParameterVAlueParameterSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateFormNumberParameterVAlueParameterSchema(model: FormNumberParameterVAlueParameterSchema, path: string[] = []) {
    if (!lib.validateType(model, "number")) {
        yield path;
        return;
    }
}
export function* validateFormArrayParameterVAlueParameterSchema(model: FormArrayParameterVAlueParameterSchema, path: string[] = []) {
    if (!lib.validateType(model, "array")) {
        yield path;
        return;
    }
    for (const [key, value] of Object.entries(model)) {
        yield* validateFormArrayParameterVAlueParameterItemsSchema(value, [...path, key]);
    }
}
validateFormArrayParameterVAlueParameterSchema.items = validateFormArrayParameterVAlueParameterItemsSchema;
export function* validateFormArrayParameterVAlueParameterItemsSchema(model: FormArrayParameterVAlueParameterItemsSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateFormExplodeArrayParameterVAlueParameterSchema(model: FormExplodeArrayParameterVAlueParameterSchema, path: string[] = []) {
    if (!lib.validateType(model, "array")) {
        yield path;
        return;
    }
    for (const [key, value] of Object.entries(model)) {
        yield* validateFormExplodeArrayParameterVAlueParameterItemsSchema(value, [...path, key]);
    }
}
validateFormExplodeArrayParameterVAlueParameterSchema.items = validateFormExplodeArrayParameterVAlueParameterItemsSchema;
export function* validateFormExplodeArrayParameterVAlueParameterItemsSchema(model: FormExplodeArrayParameterVAlueParameterItemsSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateFormObjectParameterVAlueParameterSchema(model: FormObjectParameterVAlueParameterSchema, path: string[] = []) {
    if (!(lib.validateType(model, "object") && lib.validateRequired(model, [
        "str",
        "num"
    ]))) {
        yield path;
        return;
    }
    if (model["str"] != null) {
        yield* validateFormObjectParameterVAlueParameterStrSchema(model["str"], [...path, "str"]);
    }
    if (model["num"] != null) {
        yield* validateFormObjectParameterVAlueParameterNumSchema(model["num"], [...path, "num"]);
    }
}
validateFormObjectParameterVAlueParameterSchema.properties = {
    "str": validateFormObjectParameterVAlueParameterStrSchema,
    "num": validateFormObjectParameterVAlueParameterNumSchema
};
export function* validateFormObjectParameterVAlueParameterStrSchema(model: FormObjectParameterVAlueParameterStrSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateFormObjectParameterVAlueParameterNumSchema(model: FormObjectParameterVAlueParameterNumSchema, path: string[] = []) {
    if (!lib.validateType(model, "number")) {
        yield path;
        return;
    }
}
export function* validateFormExplodeObjectParameterVAlueParameterSchema(model: FormExplodeObjectParameterVAlueParameterSchema, path: string[] = []) {
    if (!(lib.validateType(model, "object") && lib.validateRequired(model, [
        "str",
        "num"
    ]))) {
        yield path;
        return;
    }
    if (model["str"] != null) {
        yield* validateFormExplodeObjectParameterVAlueParameterStrSchema(model["str"], [...path, "str"]);
    }
    if (model["num"] != null) {
        yield* validateFormExplodeObjectParameterVAlueParameterNumSchema(model["num"], [...path, "num"]);
    }
}
validateFormExplodeObjectParameterVAlueParameterSchema.properties = {
    "str": validateFormExplodeObjectParameterVAlueParameterStrSchema,
    "num": validateFormExplodeObjectParameterVAlueParameterNumSchema
};
export function* validateFormExplodeObjectParameterVAlueParameterStrSchema(model: FormExplodeObjectParameterVAlueParameterStrSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateFormExplodeObjectParameterVAlueParameterNumSchema(model: FormExplodeObjectParameterVAlueParameterNumSchema, path: string[] = []) {
    if (!lib.validateType(model, "number")) {
        yield path;
        return;
    }
}
export function* validateFormDictionaryParameterVAlueParameterSchema(model: FormDictionaryParameterVAlueParameterSchema, path: string[] = []) {
    if (!lib.validateType(model, "object")) {
        yield path;
        return;
    }
    for (const [key, value] of Object.entries(model)) {
        yield* validateFormDictionaryParameterVAlueParameterAdditionalPropertiesSchema(value, [...path, key]);
    }
}
validateFormDictionaryParameterVAlueParameterSchema.additionalProperties = validateFormDictionaryParameterVAlueParameterAdditionalPropertiesSchema;
export function* validateFormDictionaryParameterVAlueParameterAdditionalPropertiesSchema(model: FormDictionaryParameterVAlueParameterAdditionalPropertiesSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateFormExplodeDictionaryParameterVAlueParameterSchema(model: FormExplodeDictionaryParameterVAlueParameterSchema, path: string[] = []) {
    if (!lib.validateType(model, "object")) {
        yield path;
        return;
    }
    for (const [key, value] of Object.entries(model)) {
        yield* validateFormExplodeDictionaryParameterVAlueParameterAdditionalPropertiesSchema(value, [...path, key]);
    }
}
validateFormExplodeDictionaryParameterVAlueParameterSchema.additionalProperties = validateFormExplodeDictionaryParameterVAlueParameterAdditionalPropertiesSchema;
export function* validateFormExplodeDictionaryParameterVAlueParameterAdditionalPropertiesSchema(model: FormExplodeDictionaryParameterVAlueParameterAdditionalPropertiesSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateDeepObjectObjectParameterVAlueParameterSchema(model: DeepObjectObjectParameterVAlueParameterSchema, path: string[] = []) {
    if (!(lib.validateType(model, "object") && lib.validateRequired(model, [
        "str",
        "num"
    ]))) {
        yield path;
        return;
    }
    if (model["str"] != null) {
        yield* validateDeepObjectObjectParameterVAlueParameterStrSchema(model["str"], [...path, "str"]);
    }
    if (model["num"] != null) {
        yield* validateDeepObjectObjectParameterVAlueParameterNumSchema(model["num"], [...path, "num"]);
    }
}
validateDeepObjectObjectParameterVAlueParameterSchema.properties = {
    "str": validateDeepObjectObjectParameterVAlueParameterStrSchema,
    "num": validateDeepObjectObjectParameterVAlueParameterNumSchema
};
export function* validateDeepObjectObjectParameterVAlueParameterStrSchema(model: DeepObjectObjectParameterVAlueParameterStrSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateDeepObjectObjectParameterVAlueParameterNumSchema(model: DeepObjectObjectParameterVAlueParameterNumSchema, path: string[] = []) {
    if (!lib.validateType(model, "number")) {
        yield path;
        return;
    }
}
export function* validateDeepObjectDictionaryParameterVAlueParameterSchema(model: DeepObjectDictionaryParameterVAlueParameterSchema, path: string[] = []) {
    if (!lib.validateType(model, "object")) {
        yield path;
        return;
    }
    for (const [key, value] of Object.entries(model)) {
        yield* validateDeepObjectDictionaryParameterVAlueParameterAdditionalPropertiesSchema(value, [...path, key]);
    }
}
validateDeepObjectDictionaryParameterVAlueParameterSchema.additionalProperties = validateDeepObjectDictionaryParameterVAlueParameterAdditionalPropertiesSchema;
export function* validateDeepObjectDictionaryParameterVAlueParameterAdditionalPropertiesSchema(model: DeepObjectDictionaryParameterVAlueParameterAdditionalPropertiesSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateEchoMessageSchema(model: EchoMessageSchema, path: string[] = []) {
    if (!(lib.validateType(model, "object") && lib.validateRequired(model, [
        "message"
    ]))) {
        yield path;
        return;
    }
    if (model["message"] != null) {
        yield* validateEchoMessageMessageSchema(model["message"], [...path, "message"]);
    }
}
validateEchoMessageSchema.properties = {
    "message": validateEchoMessageMessageSchema
};
export function* validateEchoMessageMessageSchema(model: EchoMessageMessageSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateStringOrNumberSchema(model: StringOrNumberSchema, path: string[] = []) {
    let validCount = 0;
    {
        const invalidPaths = [...validateStringOrNumberOneOf0Schema(model as StringOrNumberOneOf0Schema)];
        if (invalidPaths.length > 0) {
            validCount++;
        }
    }
    {
        const invalidPaths = [...validateStringOrNumberOneOf1Schema(model as StringOrNumberOneOf1Schema)];
        if (invalidPaths.length > 0) {
            validCount++;
        }
    }
    if (validCount !== 1) {
        yield path;
        return;
    }
}
validateStringOrNumberSchema.oneOf = [
    validateStringOrNumberOneOf0Schema,
    validateStringOrNumberOneOf1Schema
];
export function* validateStringOrNumberOneOf0Schema(model: StringOrNumberOneOf0Schema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateStringOrNumberOneOf1Schema(model: StringOrNumberOneOf1Schema, path: string[] = []) {
    if (!lib.validateType(model, "number")) {
        yield path;
        return;
    }
}
export function* validateIpSchema(model: IpSchema, path: string[] = []) {
    let validCount = 0;
    {
        const invalidPaths = [...validateIpOneOf0Schema(model as IpOneOf0Schema)];
        if (invalidPaths.length > 0) {
            validCount++;
        }
    }
    {
        const invalidPaths = [...validateIpOneOf1Schema(model as IpOneOf1Schema)];
        if (invalidPaths.length > 0) {
            validCount++;
        }
    }
    if (validCount !== 1) {
        yield path;
        return;
    }
}
validateIpSchema.oneOf = [
    validateIpOneOf0Schema,
    validateIpOneOf1Schema
];
export function* validateIpOneOf0Schema(model: IpOneOf0Schema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateIpOneOf1Schema(model: IpOneOf1Schema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateTupleSchema(model: TupleSchema, path: string[] = []) {
    if (!lib.validateType(model, "array")) {
        yield path;
        return;
    }
    yield* validateTupleItems0Schema(model[0], path);
    yield* validateTupleItems1Schema(model[1], path);
}
validateTupleSchema.items = [
    validateTupleItems0Schema,
    validateTupleItems1Schema
];
export function* validateTupleItems0Schema(model: TupleItems0Schema, path: string[] = []) {
    if (!lib.validateType(model, "number")) {
        yield path;
        return;
    }
}
export function* validateTupleItems1Schema(model: TupleItems1Schema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export function* validateErrorResultSchema(model: ErrorResultSchema, path: string[] = []) {
    if (!(lib.validateType(model, "object") && lib.validateRequired(model, [
        "error"
    ]))) {
        yield path;
        return;
    }
    if (model["error"] != null) {
        yield* validateErrorResultErrorSchema(model["error"], [...path, "error"]);
    }
}
validateErrorResultSchema.properties = {
    "error": validateErrorResultErrorSchema
};
export function* validateErrorResultErrorSchema(model: ErrorResultErrorSchema, path: string[] = []) {
    if (!lib.validateType(model, "string")) {
        yield path;
        return;
    }
}
export interface SessionMetricsRequestParameters {
    organization: SessionMetricsOrganizationParameterSchema;
}
export interface MetricsRequestParameters {
}
export interface EchoRequestParameters {
}
export interface BasicAuthRequestParameters {
}
export interface SimpleStringParameterRequestParameters {
    vAlue: SimpleStringParameterVAlueParameterSchema;
}
export interface SimpleNumberParameterRequestParameters {
    vAlue: SimpleNumberParameterVAlueParameterSchema;
}
export interface SimpleArrayParameterRequestParameters {
    vAlue: SimpleArrayParameterVAlueParameterSchema;
}
export interface SimpleExplodeArrayParameterRequestParameters {
    vAlue: SimpleExplodeArrayParameterVAlueParameterSchema;
}
export interface SimpleObjectParameterRequestParameters {
    vAlue: SimpleObjectParameterVAlueParameterSchema;
}
export interface SimpleExplodeObjectParameterRequestParameters {
    vAlue: SimpleExplodeObjectParameterVAlueParameterSchema;
}
export interface SimpleDictionaryParameterRequestParameters {
    vAlue: SimpleDictionaryParameterVAlueParameterSchema;
}
export interface SimpleExplodeDictionaryParameterRequestParameters {
    vAlue: SimpleExplodeDictionaryParameterVAlueParameterSchema;
}
export interface LabelStringParameterRequestParameters {
    vAlue: LabelStringParameterVAlueParameterSchema;
}
export interface LabelNumberParameterRequestParameters {
    vAlue: LabelNumberParameterVAlueParameterSchema;
}
export interface LabelArrayParameterRequestParameters {
    vAlue: LabelArrayParameterVAlueParameterSchema;
}
export interface LabelExplodeArrayParameterRequestParameters {
    vAlue: LabelExplodeArrayParameterVAlueParameterSchema;
}
export interface LabelObjectParameterRequestParameters {
    vAlue: LabelObjectParameterVAlueParameterSchema;
}
export interface LabelExplodeObjectParameterRequestParameters {
    vAlue: LabelExplodeObjectParameterVAlueParameterSchema;
}
export interface LabelDictionaryParameterRequestParameters {
    vAlue: LabelDictionaryParameterVAlueParameterSchema;
}
export interface LabelExplodeDictionaryParameterRequestParameters {
    vAlue: LabelExplodeDictionaryParameterVAlueParameterSchema;
}
export interface FormStringParameterRequestParameters {
    vAlue: FormStringParameterVAlueParameterSchema;
}
export interface FormNumberParameterRequestParameters {
    vAlue: FormNumberParameterVAlueParameterSchema;
}
export interface FormArrayParameterRequestParameters {
    vAlue: FormArrayParameterVAlueParameterSchema;
}
export interface FormExplodeArrayParameterRequestParameters {
    vAlue: FormExplodeArrayParameterVAlueParameterSchema;
}
export interface FormObjectParameterRequestParameters {
    vAlue: FormObjectParameterVAlueParameterSchema;
}
export interface FormExplodeObjectParameterRequestParameters {
    vAlue: FormExplodeObjectParameterVAlueParameterSchema;
}
export interface FormDictionaryParameterRequestParameters {
    vAlue: FormDictionaryParameterVAlueParameterSchema;
}
export interface FormExplodeDictionaryParameterRequestParameters {
    vAlue: FormExplodeDictionaryParameterVAlueParameterSchema;
}
export interface DeepObjectObjectParameterRequestParameters {
    vAlue: DeepObjectObjectParameterVAlueParameterSchema;
}
export interface DeepObjectDictionaryParameterRequestParameters {
    vAlue: DeepObjectDictionaryParameterVAlueParameterSchema;
}
export interface UnsupportedResponseRequestParameters {
}
export interface UnsupportedRequestRequestParameters {
}
export interface UnsupportedButNotRequiredRequestRequestParameters {
}
export interface SessionMetrics200ResponseResponseParameters {
}
export interface Metrics200ResponseResponseParameters {
}
export interface Echo200ResponseResponseParameters {
}
export interface BasicAuth204ResponseResponseParameters {
}
export interface SimpleStringParameter204ResponseResponseParameters {
}
export interface SimpleNumberParameter204ResponseResponseParameters {
}
export interface SimpleArrayParameter204ResponseResponseParameters {
}
export interface SimpleExplodeArrayParameter204ResponseResponseParameters {
}
export interface SimpleObjectParameter204ResponseResponseParameters {
}
export interface SimpleExplodeObjectParameter204ResponseResponseParameters {
}
export interface SimpleDictionaryParameter204ResponseResponseParameters {
}
export interface SimpleExplodeDictionaryParameter204ResponseResponseParameters {
}
export interface LabelStringParameter204ResponseResponseParameters {
}
export interface LabelNumberParameter204ResponseResponseParameters {
}
export interface LabelArrayParameter204ResponseResponseParameters {
}
export interface LabelExplodeArrayParameter204ResponseResponseParameters {
}
export interface LabelObjectParameter204ResponseResponseParameters {
}
export interface LabelExplodeObjectParameter204ResponseResponseParameters {
}
export interface LabelDictionaryParameter204ResponseResponseParameters {
}
export interface LabelExplodeDictionaryParameter204ResponseResponseParameters {
}
export interface FormStringParameter204ResponseResponseParameters {
}
export interface FormNumberParameter204ResponseResponseParameters {
}
export interface FormArrayParameter204ResponseResponseParameters {
}
export interface FormExplodeArrayParameter204ResponseResponseParameters {
}
export interface FormObjectParameter204ResponseResponseParameters {
}
export interface FormExplodeObjectParameter204ResponseResponseParameters {
}
export interface FormDictionaryParameter204ResponseResponseParameters {
}
export interface FormExplodeDictionaryParameter204ResponseResponseParameters {
}
export interface DeepObjectObjectParameter204ResponseResponseParameters {
}
export interface DeepObjectDictionaryParameter204ResponseResponseParameters {
}
export interface UnsupportedResponse200ResponseResponseParameters {
}
export interface UnsupportedRequest204ResponseResponseParameters {
}
export interface UnsupportedButNotRequiredRequest204ResponseResponseParameters {
}
export interface Error400ResponseResponseParameters {
}
export function* validateSessionMetricsRequestParameters(parameters: SessionMetricsRequestParameters, path: string[] = []) {
    if (parameters.organization == null) {
        yield * [path];
        return;
    }
    yield* validateSessionMetricsOrganizationParameterSchema(parameters.organization, [...path, "organization"]);
}
validateSessionMetricsRequestParameters.parameters = {
    "organization": validateSessionMetricsOrganizationParameterSchema
};
export function* validateMetricsRequestParameters(parameters: MetricsRequestParameters, path: string[] = []) {
}
validateMetricsRequestParameters.parameters = {};
export function* validateEchoRequestParameters(parameters: EchoRequestParameters, path: string[] = []) {
}
validateEchoRequestParameters.parameters = {};
export function* validateBasicAuthRequestParameters(parameters: BasicAuthRequestParameters, path: string[] = []) {
}
validateBasicAuthRequestParameters.parameters = {};
export function* validateSimpleStringParameterRequestParameters(parameters: SimpleStringParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateSimpleStringParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateSimpleStringParameterRequestParameters.parameters = {
    "vAlue": validateSimpleStringParameterVAlueParameterSchema
};
export function* validateSimpleNumberParameterRequestParameters(parameters: SimpleNumberParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateSimpleNumberParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateSimpleNumberParameterRequestParameters.parameters = {
    "vAlue": validateSimpleNumberParameterVAlueParameterSchema
};
export function* validateSimpleArrayParameterRequestParameters(parameters: SimpleArrayParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateSimpleArrayParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateSimpleArrayParameterRequestParameters.parameters = {
    "vAlue": validateSimpleArrayParameterVAlueParameterSchema
};
export function* validateSimpleExplodeArrayParameterRequestParameters(parameters: SimpleExplodeArrayParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateSimpleExplodeArrayParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateSimpleExplodeArrayParameterRequestParameters.parameters = {
    "vAlue": validateSimpleExplodeArrayParameterVAlueParameterSchema
};
export function* validateSimpleObjectParameterRequestParameters(parameters: SimpleObjectParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateSimpleObjectParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateSimpleObjectParameterRequestParameters.parameters = {
    "vAlue": validateSimpleObjectParameterVAlueParameterSchema
};
export function* validateSimpleExplodeObjectParameterRequestParameters(parameters: SimpleExplodeObjectParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateSimpleExplodeObjectParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateSimpleExplodeObjectParameterRequestParameters.parameters = {
    "vAlue": validateSimpleExplodeObjectParameterVAlueParameterSchema
};
export function* validateSimpleDictionaryParameterRequestParameters(parameters: SimpleDictionaryParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateSimpleDictionaryParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateSimpleDictionaryParameterRequestParameters.parameters = {
    "vAlue": validateSimpleDictionaryParameterVAlueParameterSchema
};
export function* validateSimpleExplodeDictionaryParameterRequestParameters(parameters: SimpleExplodeDictionaryParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateSimpleExplodeDictionaryParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateSimpleExplodeDictionaryParameterRequestParameters.parameters = {
    "vAlue": validateSimpleExplodeDictionaryParameterVAlueParameterSchema
};
export function* validateLabelStringParameterRequestParameters(parameters: LabelStringParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateLabelStringParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateLabelStringParameterRequestParameters.parameters = {
    "vAlue": validateLabelStringParameterVAlueParameterSchema
};
export function* validateLabelNumberParameterRequestParameters(parameters: LabelNumberParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateLabelNumberParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateLabelNumberParameterRequestParameters.parameters = {
    "vAlue": validateLabelNumberParameterVAlueParameterSchema
};
export function* validateLabelArrayParameterRequestParameters(parameters: LabelArrayParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateLabelArrayParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateLabelArrayParameterRequestParameters.parameters = {
    "vAlue": validateLabelArrayParameterVAlueParameterSchema
};
export function* validateLabelExplodeArrayParameterRequestParameters(parameters: LabelExplodeArrayParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateLabelExplodeArrayParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateLabelExplodeArrayParameterRequestParameters.parameters = {
    "vAlue": validateLabelExplodeArrayParameterVAlueParameterSchema
};
export function* validateLabelObjectParameterRequestParameters(parameters: LabelObjectParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateLabelObjectParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateLabelObjectParameterRequestParameters.parameters = {
    "vAlue": validateLabelObjectParameterVAlueParameterSchema
};
export function* validateLabelExplodeObjectParameterRequestParameters(parameters: LabelExplodeObjectParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateLabelExplodeObjectParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateLabelExplodeObjectParameterRequestParameters.parameters = {
    "vAlue": validateLabelExplodeObjectParameterVAlueParameterSchema
};
export function* validateLabelDictionaryParameterRequestParameters(parameters: LabelDictionaryParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateLabelDictionaryParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateLabelDictionaryParameterRequestParameters.parameters = {
    "vAlue": validateLabelDictionaryParameterVAlueParameterSchema
};
export function* validateLabelExplodeDictionaryParameterRequestParameters(parameters: LabelExplodeDictionaryParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateLabelExplodeDictionaryParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateLabelExplodeDictionaryParameterRequestParameters.parameters = {
    "vAlue": validateLabelExplodeDictionaryParameterVAlueParameterSchema
};
export function* validateFormStringParameterRequestParameters(parameters: FormStringParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateFormStringParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateFormStringParameterRequestParameters.parameters = {
    "vAlue": validateFormStringParameterVAlueParameterSchema
};
export function* validateFormNumberParameterRequestParameters(parameters: FormNumberParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateFormNumberParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateFormNumberParameterRequestParameters.parameters = {
    "vAlue": validateFormNumberParameterVAlueParameterSchema
};
export function* validateFormArrayParameterRequestParameters(parameters: FormArrayParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateFormArrayParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateFormArrayParameterRequestParameters.parameters = {
    "vAlue": validateFormArrayParameterVAlueParameterSchema
};
export function* validateFormExplodeArrayParameterRequestParameters(parameters: FormExplodeArrayParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateFormExplodeArrayParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateFormExplodeArrayParameterRequestParameters.parameters = {
    "vAlue": validateFormExplodeArrayParameterVAlueParameterSchema
};
export function* validateFormObjectParameterRequestParameters(parameters: FormObjectParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateFormObjectParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateFormObjectParameterRequestParameters.parameters = {
    "vAlue": validateFormObjectParameterVAlueParameterSchema
};
export function* validateFormExplodeObjectParameterRequestParameters(parameters: FormExplodeObjectParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateFormExplodeObjectParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateFormExplodeObjectParameterRequestParameters.parameters = {
    "vAlue": validateFormExplodeObjectParameterVAlueParameterSchema
};
export function* validateFormDictionaryParameterRequestParameters(parameters: FormDictionaryParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateFormDictionaryParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateFormDictionaryParameterRequestParameters.parameters = {
    "vAlue": validateFormDictionaryParameterVAlueParameterSchema
};
export function* validateFormExplodeDictionaryParameterRequestParameters(parameters: FormExplodeDictionaryParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateFormExplodeDictionaryParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateFormExplodeDictionaryParameterRequestParameters.parameters = {
    "vAlue": validateFormExplodeDictionaryParameterVAlueParameterSchema
};
export function* validateDeepObjectObjectParameterRequestParameters(parameters: DeepObjectObjectParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateDeepObjectObjectParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateDeepObjectObjectParameterRequestParameters.parameters = {
    "vAlue": validateDeepObjectObjectParameterVAlueParameterSchema
};
export function* validateDeepObjectDictionaryParameterRequestParameters(parameters: DeepObjectDictionaryParameterRequestParameters, path: string[] = []) {
    if (parameters.vAlue == null) {
        yield * [path];
        return;
    }
    yield* validateDeepObjectDictionaryParameterVAlueParameterSchema(parameters.vAlue, [...path, "vAlue"]);
}
validateDeepObjectDictionaryParameterRequestParameters.parameters = {
    "vAlue": validateDeepObjectDictionaryParameterVAlueParameterSchema
};
export function* validateUnsupportedResponseRequestParameters(parameters: UnsupportedResponseRequestParameters, path: string[] = []) {
}
validateUnsupportedResponseRequestParameters.parameters = {};
export function* validateUnsupportedRequestRequestParameters(parameters: UnsupportedRequestRequestParameters, path: string[] = []) {
}
validateUnsupportedRequestRequestParameters.parameters = {};
export function* validateUnsupportedButNotRequiredRequestRequestParameters(parameters: UnsupportedButNotRequiredRequestRequestParameters, path: string[] = []) {
}
validateUnsupportedButNotRequiredRequestRequestParameters.parameters = {};
export function* validateSessionMetrics200ResponseResponseParameters(parameters: SessionMetrics200ResponseResponseParameters, path: string[] = []) {
}
validateSessionMetrics200ResponseResponseParameters.parameters = {};
export function* validateMetrics200ResponseResponseParameters(parameters: Metrics200ResponseResponseParameters, path: string[] = []) {
}
validateMetrics200ResponseResponseParameters.parameters = {};
export function* validateEcho200ResponseResponseParameters(parameters: Echo200ResponseResponseParameters, path: string[] = []) {
}
validateEcho200ResponseResponseParameters.parameters = {};
export function* validateBasicAuth204ResponseResponseParameters(parameters: BasicAuth204ResponseResponseParameters, path: string[] = []) {
}
validateBasicAuth204ResponseResponseParameters.parameters = {};
export function* validateSimpleStringParameter204ResponseResponseParameters(parameters: SimpleStringParameter204ResponseResponseParameters, path: string[] = []) {
}
validateSimpleStringParameter204ResponseResponseParameters.parameters = {};
export function* validateSimpleNumberParameter204ResponseResponseParameters(parameters: SimpleNumberParameter204ResponseResponseParameters, path: string[] = []) {
}
validateSimpleNumberParameter204ResponseResponseParameters.parameters = {};
export function* validateSimpleArrayParameter204ResponseResponseParameters(parameters: SimpleArrayParameter204ResponseResponseParameters, path: string[] = []) {
}
validateSimpleArrayParameter204ResponseResponseParameters.parameters = {};
export function* validateSimpleExplodeArrayParameter204ResponseResponseParameters(parameters: SimpleExplodeArrayParameter204ResponseResponseParameters, path: string[] = []) {
}
validateSimpleExplodeArrayParameter204ResponseResponseParameters.parameters = {};
export function* validateSimpleObjectParameter204ResponseResponseParameters(parameters: SimpleObjectParameter204ResponseResponseParameters, path: string[] = []) {
}
validateSimpleObjectParameter204ResponseResponseParameters.parameters = {};
export function* validateSimpleExplodeObjectParameter204ResponseResponseParameters(parameters: SimpleExplodeObjectParameter204ResponseResponseParameters, path: string[] = []) {
}
validateSimpleExplodeObjectParameter204ResponseResponseParameters.parameters = {};
export function* validateSimpleDictionaryParameter204ResponseResponseParameters(parameters: SimpleDictionaryParameter204ResponseResponseParameters, path: string[] = []) {
}
validateSimpleDictionaryParameter204ResponseResponseParameters.parameters = {};
export function* validateSimpleExplodeDictionaryParameter204ResponseResponseParameters(parameters: SimpleExplodeDictionaryParameter204ResponseResponseParameters, path: string[] = []) {
}
validateSimpleExplodeDictionaryParameter204ResponseResponseParameters.parameters = {};
export function* validateLabelStringParameter204ResponseResponseParameters(parameters: LabelStringParameter204ResponseResponseParameters, path: string[] = []) {
}
validateLabelStringParameter204ResponseResponseParameters.parameters = {};
export function* validateLabelNumberParameter204ResponseResponseParameters(parameters: LabelNumberParameter204ResponseResponseParameters, path: string[] = []) {
}
validateLabelNumberParameter204ResponseResponseParameters.parameters = {};
export function* validateLabelArrayParameter204ResponseResponseParameters(parameters: LabelArrayParameter204ResponseResponseParameters, path: string[] = []) {
}
validateLabelArrayParameter204ResponseResponseParameters.parameters = {};
export function* validateLabelExplodeArrayParameter204ResponseResponseParameters(parameters: LabelExplodeArrayParameter204ResponseResponseParameters, path: string[] = []) {
}
validateLabelExplodeArrayParameter204ResponseResponseParameters.parameters = {};
export function* validateLabelObjectParameter204ResponseResponseParameters(parameters: LabelObjectParameter204ResponseResponseParameters, path: string[] = []) {
}
validateLabelObjectParameter204ResponseResponseParameters.parameters = {};
export function* validateLabelExplodeObjectParameter204ResponseResponseParameters(parameters: LabelExplodeObjectParameter204ResponseResponseParameters, path: string[] = []) {
}
validateLabelExplodeObjectParameter204ResponseResponseParameters.parameters = {};
export function* validateLabelDictionaryParameter204ResponseResponseParameters(parameters: LabelDictionaryParameter204ResponseResponseParameters, path: string[] = []) {
}
validateLabelDictionaryParameter204ResponseResponseParameters.parameters = {};
export function* validateLabelExplodeDictionaryParameter204ResponseResponseParameters(parameters: LabelExplodeDictionaryParameter204ResponseResponseParameters, path: string[] = []) {
}
validateLabelExplodeDictionaryParameter204ResponseResponseParameters.parameters = {};
export function* validateFormStringParameter204ResponseResponseParameters(parameters: FormStringParameter204ResponseResponseParameters, path: string[] = []) {
}
validateFormStringParameter204ResponseResponseParameters.parameters = {};
export function* validateFormNumberParameter204ResponseResponseParameters(parameters: FormNumberParameter204ResponseResponseParameters, path: string[] = []) {
}
validateFormNumberParameter204ResponseResponseParameters.parameters = {};
export function* validateFormArrayParameter204ResponseResponseParameters(parameters: FormArrayParameter204ResponseResponseParameters, path: string[] = []) {
}
validateFormArrayParameter204ResponseResponseParameters.parameters = {};
export function* validateFormExplodeArrayParameter204ResponseResponseParameters(parameters: FormExplodeArrayParameter204ResponseResponseParameters, path: string[] = []) {
}
validateFormExplodeArrayParameter204ResponseResponseParameters.parameters = {};
export function* validateFormObjectParameter204ResponseResponseParameters(parameters: FormObjectParameter204ResponseResponseParameters, path: string[] = []) {
}
validateFormObjectParameter204ResponseResponseParameters.parameters = {};
export function* validateFormExplodeObjectParameter204ResponseResponseParameters(parameters: FormExplodeObjectParameter204ResponseResponseParameters, path: string[] = []) {
}
validateFormExplodeObjectParameter204ResponseResponseParameters.parameters = {};
export function* validateFormDictionaryParameter204ResponseResponseParameters(parameters: FormDictionaryParameter204ResponseResponseParameters, path: string[] = []) {
}
validateFormDictionaryParameter204ResponseResponseParameters.parameters = {};
export function* validateFormExplodeDictionaryParameter204ResponseResponseParameters(parameters: FormExplodeDictionaryParameter204ResponseResponseParameters, path: string[] = []) {
}
validateFormExplodeDictionaryParameter204ResponseResponseParameters.parameters = {};
export function* validateDeepObjectObjectParameter204ResponseResponseParameters(parameters: DeepObjectObjectParameter204ResponseResponseParameters, path: string[] = []) {
}
validateDeepObjectObjectParameter204ResponseResponseParameters.parameters = {};
export function* validateDeepObjectDictionaryParameter204ResponseResponseParameters(parameters: DeepObjectDictionaryParameter204ResponseResponseParameters, path: string[] = []) {
}
validateDeepObjectDictionaryParameter204ResponseResponseParameters.parameters = {};
export function* validateUnsupportedResponse200ResponseResponseParameters(parameters: UnsupportedResponse200ResponseResponseParameters, path: string[] = []) {
}
validateUnsupportedResponse200ResponseResponseParameters.parameters = {};
export function* validateUnsupportedRequest204ResponseResponseParameters(parameters: UnsupportedRequest204ResponseResponseParameters, path: string[] = []) {
}
validateUnsupportedRequest204ResponseResponseParameters.parameters = {};
export function* validateUnsupportedButNotRequiredRequest204ResponseResponseParameters(parameters: UnsupportedButNotRequiredRequest204ResponseResponseParameters, path: string[] = []) {
}
validateUnsupportedButNotRequiredRequest204ResponseResponseParameters.parameters = {};
export function* validateError400ResponseResponseParameters(parameters: Error400ResponseResponseParameters, path: string[] = []) {
}
validateError400ResponseResponseParameters.parameters = {};
