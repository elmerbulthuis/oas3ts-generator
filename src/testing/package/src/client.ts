/* eslint-disable */

import * as lib from "@oas3/oas3ts-lib";
import * as shared from "./shared.js";
import * as internal from "./client-internal.js";
export interface ClientCredentials {
    basic?: shared.BasicCredential;
    apiToken?: shared.ApiTokenCredential;
    apiKey?: shared.ApiKeyCredential;
    accessToken?: shared.AccessTokenCredential;
}
export class Client extends lib.ClientBase {
    constructor(options: lib.ClientOptions, private readonly credentials: ClientCredentials = {}) {
        super(options);
        this.router.insertRoute("/session-metrics/{organization}", "/session-metrics/{organization}");
        this.router.insertRoute("/metrics", "/metrics");
        this.router.insertRoute("/echo", "/echo");
        this.router.insertRoute("/auth", "/auth");
        this.router.insertRoute("/parameter/string/simple", "/parameter/string/simple");
        this.router.insertRoute("/parameter/number/simple", "/parameter/number/simple");
        this.router.insertRoute("/parameter/array/simple", "/parameter/array/simple");
        this.router.insertRoute("/parameter/array/simple-explode", "/parameter/array/simple-explode");
        this.router.insertRoute("/parameter/object/simple", "/parameter/object/simple");
        this.router.insertRoute("/parameter/object/simple-explode", "/parameter/object/simple-explode");
        this.router.insertRoute("/parameter/dictionary/simple", "/parameter/dictionary/simple");
        this.router.insertRoute("/parameter/dictionary/simple-explode", "/parameter/dictionary/simple-explode");
        this.router.insertRoute("/parameter/string/label/{V-Alue}", "/parameter/string/label/{V-Alue}");
        this.router.insertRoute("/parameter/number/label/{V-Alue}", "/parameter/number/label/{V-Alue}");
        this.router.insertRoute("/parameter/array/label/{V-Alue}", "/parameter/array/label/{V-Alue}");
        this.router.insertRoute("/parameter/array/label-explode/{V-Alue}", "/parameter/array/label-explode/{V-Alue}");
        this.router.insertRoute("/parameter/object/label/{V-Alue}", "/parameter/object/label/{V-Alue}");
        this.router.insertRoute("/parameter/object/label-explode/{V-Alue}", "/parameter/object/label-explode/{V-Alue}");
        this.router.insertRoute("/parameter/dictionary/label/{V-Alue}", "/parameter/dictionary/label/{V-Alue}");
        this.router.insertRoute("/parameter/dictionary/label-explode/{V-Alue}", "/parameter/dictionary/label-explode/{V-Alue}");
        this.router.insertRoute("/parameter/string/form", "/parameter/string/form");
        this.router.insertRoute("/parameter/number/form", "/parameter/number/form");
        this.router.insertRoute("/parameter/array/form", "/parameter/array/form");
        this.router.insertRoute("/parameter/array/form-explode", "/parameter/array/form-explode");
        this.router.insertRoute("/parameter/object/form", "/parameter/object/form");
        this.router.insertRoute("/parameter/object/form-explode", "/parameter/object/form-explode");
        this.router.insertRoute("/parameter/dictionary/form", "/parameter/dictionary/form");
        this.router.insertRoute("/parameter/dictionary/form-explode", "/parameter/dictionary/form-explode");
        this.router.insertRoute("/parameter/object/deep-object", "/parameter/object/deep-object");
        this.router.insertRoute("/parameter/dictionary/deep-object", "/parameter/dictionary/deep-object");
        this.router.insertRoute("/unsupported", "/unsupported");
        this.router.insertRoute("/unsupported-but-not-requires", "/unsupported-but-not-requires");
    }
    /**
    */
    public async sessionMetrics(outgoingRequestModel: SessionMetricsOutgoingRequest): Promise<SessionMetricsIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateSessionMetricsRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectSessionMetricsOrganizationRequestParameter(routeParameters, outgoingRequestModel.parameters.organization);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.sessionMetricsAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/session-metrics/{organization}",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: SessionMetricsIncomingResponse;
        switch (clientResponse.status) {
            case 200: {
                if (responseContentTypeHeader == null) {
                    throw new lib.MissingResponseContentTypeError();
                }
                const responseContentType = lib.parseContentTypeHeader(responseContentTypeHeader);
                let responseParameters;
                try {
                    responseParameters = {} as shared.SessionMetrics200ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateSessionMetrics200ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                switch (responseContentType) {
                    case "application/json":
                        incomingResponseModel = {
                            ...{
                                status: clientResponse.status,
                                parameters: responseParameters,
                                contentType: "application/json"
                            },
                            ...internal.deserializeSessionMetrics200ResponseApplicationJson(clientResponse.stream, this.validateIncomingEntities)
                        };
                        break;
                    default: throw new lib.UnexpectedResponseContentType(responseContentType);
                }
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async metrics(outgoingRequestModel: MetricsOutgoingRequest): Promise<MetricsIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateMetricsRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.metricsAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/metrics",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: MetricsIncomingResponse;
        switch (clientResponse.status) {
            case 200: {
                if (responseContentTypeHeader == null) {
                    throw new lib.MissingResponseContentTypeError();
                }
                const responseContentType = lib.parseContentTypeHeader(responseContentTypeHeader);
                let responseParameters;
                try {
                    responseParameters = {} as shared.Metrics200ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateMetrics200ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                switch (responseContentType) {
                    case "text/plain":
                        incomingResponseModel = {
                            ...{
                                status: clientResponse.status,
                                parameters: responseParameters,
                                contentType: "text/plain"
                            },
                            ...internal.deserializeMetrics200ResponseTextPlain(clientResponse.stream, this.validateIncomingEntities)
                        };
                        break;
                    default: throw new lib.UnexpectedResponseContentType(responseContentType);
                }
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async echo(outgoingRequestModel: EchoOutgoingRequest): Promise<EchoIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateEchoRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.echoAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/echo",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        let clientRequest: lib.ClientOutgoingRequest;
        if ("contentType" in outgoingRequestModel) {
            lib.addParameter(requestHeaders, "content-type", outgoingRequestModel.contentType);
            switch (outgoingRequestModel.contentType) {
                case "application/json":
                    clientRequest = {
                        url: requestUrl,
                        method: "POST",
                        headers: requestHeaders,
                        stream: internal.serializeEchoApplicationJson(outgoingRequestModel, this.validateOutgoingEntities)
                    };
                    break;
                case "application/x-www-form-urlencoded":
                    clientRequest = {
                        url: requestUrl,
                        method: "POST",
                        headers: requestHeaders,
                        stream: internal.serializeEchoApplicationXWwwFormUrlencoded(outgoingRequestModel, this.validateOutgoingEntities)
                    };
                    break;
                case "text/plain":
                    clientRequest = {
                        url: requestUrl,
                        method: "POST",
                        headers: requestHeaders,
                        stream: internal.serializeEchoTextPlain(outgoingRequestModel, this.validateOutgoingEntities)
                    };
                    break;
                case "application/octet-stream":
                    clientRequest = {
                        url: requestUrl,
                        method: "POST",
                        headers: requestHeaders,
                        stream: internal.serializeEchoApplicationOctetStream(outgoingRequestModel, this.validateOutgoingEntities)
                    };
                    break;
                default: throw new TypeError("invalid content-type");
            }
        }
        else {
            lib.addParameter(requestHeaders, "content-type", "application/json");
            clientRequest = {
                url: requestUrl,
                method: "POST",
                headers: requestHeaders,
                stream: internal.serializeEchoApplicationJson(outgoingRequestModel, this.validateOutgoingEntities)
            };
        }
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: EchoIncomingResponse;
        switch (clientResponse.status) {
            case 200: {
                if (responseContentTypeHeader == null) {
                    throw new lib.MissingResponseContentTypeError();
                }
                const responseContentType = lib.parseContentTypeHeader(responseContentTypeHeader);
                let responseParameters;
                try {
                    responseParameters = {} as shared.Echo200ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateEcho200ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                switch (responseContentType) {
                    case "application/json":
                        incomingResponseModel = {
                            ...{
                                status: clientResponse.status,
                                parameters: responseParameters,
                                contentType: "application/json"
                            },
                            ...internal.deserializeEcho200ResponseApplicationJson(clientResponse.stream, this.validateIncomingEntities)
                        };
                        break;
                    case "application/x-www-form-urlencoded":
                        incomingResponseModel = {
                            ...{
                                status: clientResponse.status,
                                parameters: responseParameters,
                                contentType: "application/x-www-form-urlencoded"
                            },
                            ...internal.deserializeEcho200ResponseApplicationXWwwFormUrlencoded(clientResponse.stream, this.validateIncomingEntities)
                        };
                        break;
                    case "text/plain":
                        incomingResponseModel = {
                            ...{
                                status: clientResponse.status,
                                parameters: responseParameters,
                                contentType: "text/plain"
                            },
                            ...internal.deserializeEcho200ResponseTextPlain(clientResponse.stream, this.validateIncomingEntities)
                        };
                        break;
                    case "application/octet-stream":
                        incomingResponseModel = {
                            ...{
                                status: clientResponse.status,
                                parameters: responseParameters,
                                contentType: "application/octet-stream"
                            },
                            ...internal.deserializeEcho200ResponseApplicationOctetStream(clientResponse.stream, this.validateIncomingEntities)
                        };
                        break;
                    default: throw new lib.UnexpectedResponseContentType(responseContentType);
                }
                break;
            }
            case 400: {
                if (responseContentTypeHeader == null) {
                    throw new lib.MissingResponseContentTypeError();
                }
                const responseContentType = lib.parseContentTypeHeader(responseContentTypeHeader);
                let responseParameters;
                try {
                    responseParameters = {} as shared.Error400ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateError400ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                switch (responseContentType) {
                    case "application/json":
                        incomingResponseModel = {
                            ...{
                                status: clientResponse.status,
                                parameters: responseParameters,
                                contentType: "application/json"
                            },
                            ...internal.deserializeError400ResponseApplicationJson(clientResponse.stream, this.validateIncomingEntities)
                        };
                        break;
                    default: throw new lib.UnexpectedResponseContentType(responseContentType);
                }
                break;
            }
            case 422: {
                if (responseContentTypeHeader == null) {
                    throw new lib.MissingResponseContentTypeError();
                }
                const responseContentType = lib.parseContentTypeHeader(responseContentTypeHeader);
                let responseParameters;
                try {
                    responseParameters = {} as shared.Error400ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateError400ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                switch (responseContentType) {
                    case "application/json":
                        incomingResponseModel = {
                            ...{
                                status: clientResponse.status,
                                parameters: responseParameters,
                                contentType: "application/json"
                            },
                            ...internal.deserializeError400ResponseApplicationJson(clientResponse.stream, this.validateIncomingEntities)
                        };
                        break;
                    default: throw new lib.UnexpectedResponseContentType(responseContentType);
                }
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async basicAuth(outgoingRequestModel: BasicAuthOutgoingRequest): Promise<BasicAuthIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateBasicAuthRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectBasicCredential(requestHeaders, this.credentials.basic);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.basicAuthAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/auth",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: BasicAuthIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.BasicAuth204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateBasicAuth204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async simpleStringParameter(outgoingRequestModel: SimpleStringParameterOutgoingRequest): Promise<SimpleStringParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateSimpleStringParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectSimpleStringParameterVAlueRequestParameter(requestHeaders, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.simpleStringParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/string/simple",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: SimpleStringParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.SimpleStringParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateSimpleStringParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async simpleNumberParameter(outgoingRequestModel: SimpleNumberParameterOutgoingRequest): Promise<SimpleNumberParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateSimpleNumberParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectSimpleNumberParameterVAlueRequestParameter(requestHeaders, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.simpleNumberParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/number/simple",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: SimpleNumberParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.SimpleNumberParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateSimpleNumberParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async simpleArrayParameter(outgoingRequestModel: SimpleArrayParameterOutgoingRequest): Promise<SimpleArrayParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateSimpleArrayParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectSimpleArrayParameterVAlueRequestParameter(requestHeaders, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.simpleArrayParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/array/simple",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: SimpleArrayParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.SimpleArrayParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateSimpleArrayParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async simpleExplodeArrayParameter(outgoingRequestModel: SimpleExplodeArrayParameterOutgoingRequest): Promise<SimpleExplodeArrayParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateSimpleExplodeArrayParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectSimpleExplodeArrayParameterVAlueRequestParameter(requestHeaders, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.simpleExplodeArrayParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/array/simple-explode",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: SimpleExplodeArrayParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.SimpleExplodeArrayParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateSimpleExplodeArrayParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async simpleObjectParameter(outgoingRequestModel: SimpleObjectParameterOutgoingRequest): Promise<SimpleObjectParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateSimpleObjectParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectSimpleObjectParameterVAlueRequestParameter(requestHeaders, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.simpleObjectParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/object/simple",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: SimpleObjectParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.SimpleObjectParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateSimpleObjectParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async simpleExplodeObjectParameter(outgoingRequestModel: SimpleExplodeObjectParameterOutgoingRequest): Promise<SimpleExplodeObjectParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateSimpleExplodeObjectParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectSimpleExplodeObjectParameterVAlueRequestParameter(requestHeaders, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.simpleExplodeObjectParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/object/simple-explode",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: SimpleExplodeObjectParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.SimpleExplodeObjectParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateSimpleExplodeObjectParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async simpleDictionaryParameter(outgoingRequestModel: SimpleDictionaryParameterOutgoingRequest): Promise<SimpleDictionaryParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateSimpleDictionaryParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectSimpleDictionaryParameterVAlueRequestParameter(requestHeaders, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.simpleDictionaryParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/dictionary/simple",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: SimpleDictionaryParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.SimpleDictionaryParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateSimpleDictionaryParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async simpleExplodeDictionaryParameter(outgoingRequestModel: SimpleExplodeDictionaryParameterOutgoingRequest): Promise<SimpleExplodeDictionaryParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateSimpleExplodeDictionaryParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectSimpleExplodeDictionaryParameterVAlueRequestParameter(requestHeaders, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.simpleExplodeDictionaryParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/dictionary/simple-explode",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: SimpleExplodeDictionaryParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.SimpleExplodeDictionaryParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateSimpleExplodeDictionaryParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async labelStringParameter(outgoingRequestModel: LabelStringParameterOutgoingRequest): Promise<LabelStringParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateLabelStringParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectLabelStringParameterVAlueRequestParameter(routeParameters, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.labelStringParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/string/label/{V-Alue}",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: LabelStringParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.LabelStringParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateLabelStringParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async labelNumberParameter(outgoingRequestModel: LabelNumberParameterOutgoingRequest): Promise<LabelNumberParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateLabelNumberParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectLabelNumberParameterVAlueRequestParameter(routeParameters, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.labelNumberParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/number/label/{V-Alue}",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: LabelNumberParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.LabelNumberParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateLabelNumberParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async labelArrayParameter(outgoingRequestModel: LabelArrayParameterOutgoingRequest): Promise<LabelArrayParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateLabelArrayParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectLabelArrayParameterVAlueRequestParameter(routeParameters, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.labelArrayParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/array/label/{V-Alue}",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: LabelArrayParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.LabelArrayParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateLabelArrayParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async labelExplodeArrayParameter(outgoingRequestModel: LabelExplodeArrayParameterOutgoingRequest): Promise<LabelExplodeArrayParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateLabelExplodeArrayParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectLabelExplodeArrayParameterVAlueRequestParameter(routeParameters, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.labelExplodeArrayParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/array/label-explode/{V-Alue}",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: LabelExplodeArrayParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.LabelExplodeArrayParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateLabelExplodeArrayParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async labelObjectParameter(outgoingRequestModel: LabelObjectParameterOutgoingRequest): Promise<LabelObjectParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateLabelObjectParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectLabelObjectParameterVAlueRequestParameter(routeParameters, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.labelObjectParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/object/label/{V-Alue}",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: LabelObjectParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.LabelObjectParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateLabelObjectParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async labelExplodeObjectParameter(outgoingRequestModel: LabelExplodeObjectParameterOutgoingRequest): Promise<LabelExplodeObjectParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateLabelExplodeObjectParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectLabelExplodeObjectParameterVAlueRequestParameter(routeParameters, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.labelExplodeObjectParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/object/label-explode/{V-Alue}",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: LabelExplodeObjectParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.LabelExplodeObjectParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateLabelExplodeObjectParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async labelDictionaryParameter(outgoingRequestModel: LabelDictionaryParameterOutgoingRequest): Promise<LabelDictionaryParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateLabelDictionaryParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectLabelDictionaryParameterVAlueRequestParameter(routeParameters, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.labelDictionaryParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/dictionary/label/{V-Alue}",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: LabelDictionaryParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.LabelDictionaryParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateLabelDictionaryParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async labelExplodeDictionaryParameter(outgoingRequestModel: LabelExplodeDictionaryParameterOutgoingRequest): Promise<LabelExplodeDictionaryParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateLabelExplodeDictionaryParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectLabelExplodeDictionaryParameterVAlueRequestParameter(routeParameters, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.labelExplodeDictionaryParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/dictionary/label-explode/{V-Alue}",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: LabelExplodeDictionaryParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.LabelExplodeDictionaryParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateLabelExplodeDictionaryParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async formStringParameter(outgoingRequestModel: FormStringParameterOutgoingRequest): Promise<FormStringParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateFormStringParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectFormStringParameterVAlueRequestParameter(requestQuery, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.formStringParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/string/form",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: FormStringParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.FormStringParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateFormStringParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async formNumberParameter(outgoingRequestModel: FormNumberParameterOutgoingRequest): Promise<FormNumberParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateFormNumberParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectFormNumberParameterVAlueRequestParameter(requestQuery, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.formNumberParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/number/form",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: FormNumberParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.FormNumberParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateFormNumberParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async formArrayParameter(outgoingRequestModel: FormArrayParameterOutgoingRequest): Promise<FormArrayParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateFormArrayParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectFormArrayParameterVAlueRequestParameter(requestQuery, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.formArrayParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/array/form",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: FormArrayParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.FormArrayParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateFormArrayParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async formExplodeArrayParameter(outgoingRequestModel: FormExplodeArrayParameterOutgoingRequest): Promise<FormExplodeArrayParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateFormExplodeArrayParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectFormExplodeArrayParameterVAlueRequestParameter(requestQuery, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.formExplodeArrayParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/array/form-explode",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: FormExplodeArrayParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.FormExplodeArrayParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateFormExplodeArrayParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async formObjectParameter(outgoingRequestModel: FormObjectParameterOutgoingRequest): Promise<FormObjectParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateFormObjectParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectFormObjectParameterVAlueRequestParameter(requestQuery, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.formObjectParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/object/form",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: FormObjectParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.FormObjectParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateFormObjectParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async formExplodeObjectParameter(outgoingRequestModel: FormExplodeObjectParameterOutgoingRequest): Promise<FormExplodeObjectParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateFormExplodeObjectParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectFormExplodeObjectParameterVAlueRequestParameter(requestQuery, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.formExplodeObjectParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/object/form-explode",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: FormExplodeObjectParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.FormExplodeObjectParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateFormExplodeObjectParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async formDictionaryParameter(outgoingRequestModel: FormDictionaryParameterOutgoingRequest): Promise<FormDictionaryParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateFormDictionaryParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectFormDictionaryParameterVAlueRequestParameter(requestQuery, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.formDictionaryParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/dictionary/form",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: FormDictionaryParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.FormDictionaryParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateFormDictionaryParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async formExplodeDictionaryParameter(outgoingRequestModel: FormExplodeDictionaryParameterOutgoingRequest): Promise<FormExplodeDictionaryParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateFormExplodeDictionaryParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectFormExplodeDictionaryParameterVAlueRequestParameter(requestQuery, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.formExplodeDictionaryParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/dictionary/form-explode",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: FormExplodeDictionaryParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.FormExplodeDictionaryParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateFormExplodeDictionaryParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async deepObjectObjectParameter(outgoingRequestModel: DeepObjectObjectParameterOutgoingRequest): Promise<DeepObjectObjectParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateDeepObjectObjectParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectDeepObjectObjectParameterVAlueRequestParameter(requestQuery, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.deepObjectObjectParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/object/deep-object",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: DeepObjectObjectParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.DeepObjectObjectParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateDeepObjectObjectParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async deepObjectDictionaryParameter(outgoingRequestModel: DeepObjectDictionaryParameterOutgoingRequest): Promise<DeepObjectDictionaryParameterIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateDeepObjectDictionaryParameterRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
            internal.injectDeepObjectDictionaryParameterVAlueRequestParameter(requestQuery, outgoingRequestModel.parameters.vAlue);
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.deepObjectDictionaryParameterAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/parameter/dictionary/deep-object",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: DeepObjectDictionaryParameterIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.DeepObjectDictionaryParameter204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateDeepObjectDictionaryParameter204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async unsupportedResponse(outgoingRequestModel: UnsupportedResponseOutgoingRequest): Promise<UnsupportedResponseIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateUnsupportedResponseRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.unsupportedResponseAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/unsupported",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "GET",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: UnsupportedResponseIncomingResponse;
        switch (clientResponse.status) {
            case 200: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.UnsupportedResponse200ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateUnsupportedResponse200ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async unsupportedRequest(outgoingRequestModel: UnsupportedRequestOutgoingRequest): Promise<UnsupportedRequestIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateUnsupportedRequestRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.unsupportedRequestAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/unsupported",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "POST",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: UnsupportedRequestIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.UnsupportedRequest204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateUnsupportedRequest204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
    /**
    */
    public async unsupportedButNotRequiredRequest(outgoingRequestModel: UnsupportedButNotRequiredRequestOutgoingRequest): Promise<UnsupportedButNotRequiredRequestIncomingResponse> {
        if (this.validateOutgoingParameters) {
            const invalidPaths = [...shared.validateUnsupportedButNotRequiredRequestRequestParameters(outgoingRequestModel.parameters)];
            if (invalidPaths.length > 0) {
                throw new lib.OutgoingRequestParametersValidationError(invalidPaths);
            }
        }
        const routeParameters: lib.Parameters = {};
        const requestQuery: lib.Parameters = {};
        const requestHeaders: lib.Parameters = {};
        try {
        }
        catch {
            throw new lib.OutgoingRequestParameterInjectionError();
        }
        lib.addParameter(requestHeaders, "accept", lib.stringifyAcceptHeader(shared.unsupportedButNotRequiredRequestAcceptTypes));
        const requestPath = this.router.stringifyRoute({
            name: "/unsupported-but-not-requires",
            parameters: Object.fromEntries(lib.parametersToEntries(routeParameters))
        });
        if (requestPath == null) {
            throw new Error("Expected requestPath");
        }
        const requestUrl = new URL(requestPath, this.baseUrl);
        requestUrl.search = lib.stringifyParameters(requestQuery);
        const clientRequest: lib.ClientOutgoingRequest = {
            url: requestUrl,
            method: "POST",
            headers: requestHeaders
        };
        const clientResponse = await this.send(clientRequest);
        const responseHeaders = { ...clientResponse.headers };
        const responseContentTypeHeader = lib.getParameterValue(responseHeaders, "content-type");
        let incomingResponseModel: UnsupportedButNotRequiredRequestIncomingResponse;
        switch (clientResponse.status) {
            case 204: {
                let responseParameters;
                try {
                    responseParameters = {} as shared.UnsupportedButNotRequiredRequest204ResponseResponseParameters;
                }
                catch {
                    throw new lib.IncomingResponseParameterExtractionError();
                }
                if (this.validateIncomingParameters) {
                    const invalidPaths = [...shared.validateUnsupportedButNotRequiredRequest204ResponseResponseParameters(responseParameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseParametersValidationError(invalidPaths);
                    }
                }
                incomingResponseModel = {
                    status: clientResponse.status,
                    parameters: responseParameters,
                    contentType: null
                };
                break;
            }
            default: throw new lib.UnexpectedStatusError(clientResponse.status);
        }
        return incomingResponseModel;
    }
}
export type SessionMetricsIncomingResponse = lib.IncomingJsonResponse<200, shared.SessionMetrics200ResponseResponseParameters, "application/json", shared.SessionMetrics200ApplicationJsonResponseSchema>;
export type MetricsIncomingResponse = lib.IncomingTextResponse<200, shared.Metrics200ResponseResponseParameters, "text/plain">;
export type EchoIncomingResponse = lib.IncomingJsonResponse<200, shared.Echo200ResponseResponseParameters, "application/json", shared.EchoMessageSchema> | lib.IncomingFormResponse<200, shared.Echo200ResponseResponseParameters, "application/x-www-form-urlencoded", shared.EchoMessageSchema> | lib.IncomingTextResponse<200, shared.Echo200ResponseResponseParameters, "text/plain"> | lib.IncomingStreamResponse<200, shared.Echo200ResponseResponseParameters, "application/octet-stream"> | lib.IncomingJsonResponse<400, shared.Error400ResponseResponseParameters, "application/json", shared.ErrorResultSchema> | lib.IncomingJsonResponse<422, shared.Error400ResponseResponseParameters, "application/json", shared.ErrorResultSchema>;
export type BasicAuthIncomingResponse = lib.IncomingEmptyResponse<204, shared.BasicAuth204ResponseResponseParameters>;
export type SimpleStringParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.SimpleStringParameter204ResponseResponseParameters>;
export type SimpleNumberParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.SimpleNumberParameter204ResponseResponseParameters>;
export type SimpleArrayParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.SimpleArrayParameter204ResponseResponseParameters>;
export type SimpleExplodeArrayParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.SimpleExplodeArrayParameter204ResponseResponseParameters>;
export type SimpleObjectParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.SimpleObjectParameter204ResponseResponseParameters>;
export type SimpleExplodeObjectParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.SimpleExplodeObjectParameter204ResponseResponseParameters>;
export type SimpleDictionaryParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.SimpleDictionaryParameter204ResponseResponseParameters>;
export type SimpleExplodeDictionaryParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.SimpleExplodeDictionaryParameter204ResponseResponseParameters>;
export type LabelStringParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.LabelStringParameter204ResponseResponseParameters>;
export type LabelNumberParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.LabelNumberParameter204ResponseResponseParameters>;
export type LabelArrayParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.LabelArrayParameter204ResponseResponseParameters>;
export type LabelExplodeArrayParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.LabelExplodeArrayParameter204ResponseResponseParameters>;
export type LabelObjectParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.LabelObjectParameter204ResponseResponseParameters>;
export type LabelExplodeObjectParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.LabelExplodeObjectParameter204ResponseResponseParameters>;
export type LabelDictionaryParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.LabelDictionaryParameter204ResponseResponseParameters>;
export type LabelExplodeDictionaryParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.LabelExplodeDictionaryParameter204ResponseResponseParameters>;
export type FormStringParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.FormStringParameter204ResponseResponseParameters>;
export type FormNumberParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.FormNumberParameter204ResponseResponseParameters>;
export type FormArrayParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.FormArrayParameter204ResponseResponseParameters>;
export type FormExplodeArrayParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.FormExplodeArrayParameter204ResponseResponseParameters>;
export type FormObjectParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.FormObjectParameter204ResponseResponseParameters>;
export type FormExplodeObjectParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.FormExplodeObjectParameter204ResponseResponseParameters>;
export type FormDictionaryParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.FormDictionaryParameter204ResponseResponseParameters>;
export type FormExplodeDictionaryParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.FormExplodeDictionaryParameter204ResponseResponseParameters>;
export type DeepObjectObjectParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.DeepObjectObjectParameter204ResponseResponseParameters>;
export type DeepObjectDictionaryParameterIncomingResponse = lib.IncomingEmptyResponse<204, shared.DeepObjectDictionaryParameter204ResponseResponseParameters>;
export type UnsupportedResponseIncomingResponse = lib.IncomingEmptyResponse<200, shared.UnsupportedResponse200ResponseResponseParameters>;
export type UnsupportedRequestIncomingResponse = lib.IncomingEmptyResponse<204, shared.UnsupportedRequest204ResponseResponseParameters>;
export type UnsupportedButNotRequiredRequestIncomingResponse = lib.IncomingEmptyResponse<204, shared.UnsupportedButNotRequiredRequest204ResponseResponseParameters>;
export type SessionMetricsOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.SessionMetricsRequestParameters> | lib.OutgoingEmptyRequest<shared.SessionMetricsRequestParameters>;
export type MetricsOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.MetricsRequestParameters> | lib.OutgoingEmptyRequest<shared.MetricsRequestParameters>;
export type EchoOutgoingRequest = lib.OutgoingJsonRequestDefault<shared.EchoRequestParameters, shared.EchoMessageSchema> | lib.OutgoingJsonRequest<shared.EchoRequestParameters, "application/json", shared.EchoMessageSchema> | lib.OutgoingFormRequest<shared.EchoRequestParameters, "application/x-www-form-urlencoded", shared.EchoMessageSchema> | lib.OutgoingTextRequest<shared.EchoRequestParameters, "text/plain"> | lib.OutgoingStreamRequest<shared.EchoRequestParameters, "application/octet-stream">;
export type BasicAuthOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.BasicAuthRequestParameters> | lib.OutgoingEmptyRequest<shared.BasicAuthRequestParameters>;
export type SimpleStringParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.SimpleStringParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.SimpleStringParameterRequestParameters>;
export type SimpleNumberParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.SimpleNumberParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.SimpleNumberParameterRequestParameters>;
export type SimpleArrayParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.SimpleArrayParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.SimpleArrayParameterRequestParameters>;
export type SimpleExplodeArrayParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.SimpleExplodeArrayParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.SimpleExplodeArrayParameterRequestParameters>;
export type SimpleObjectParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.SimpleObjectParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.SimpleObjectParameterRequestParameters>;
export type SimpleExplodeObjectParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.SimpleExplodeObjectParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.SimpleExplodeObjectParameterRequestParameters>;
export type SimpleDictionaryParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.SimpleDictionaryParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.SimpleDictionaryParameterRequestParameters>;
export type SimpleExplodeDictionaryParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.SimpleExplodeDictionaryParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.SimpleExplodeDictionaryParameterRequestParameters>;
export type LabelStringParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.LabelStringParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.LabelStringParameterRequestParameters>;
export type LabelNumberParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.LabelNumberParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.LabelNumberParameterRequestParameters>;
export type LabelArrayParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.LabelArrayParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.LabelArrayParameterRequestParameters>;
export type LabelExplodeArrayParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.LabelExplodeArrayParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.LabelExplodeArrayParameterRequestParameters>;
export type LabelObjectParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.LabelObjectParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.LabelObjectParameterRequestParameters>;
export type LabelExplodeObjectParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.LabelExplodeObjectParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.LabelExplodeObjectParameterRequestParameters>;
export type LabelDictionaryParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.LabelDictionaryParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.LabelDictionaryParameterRequestParameters>;
export type LabelExplodeDictionaryParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.LabelExplodeDictionaryParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.LabelExplodeDictionaryParameterRequestParameters>;
export type FormStringParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.FormStringParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.FormStringParameterRequestParameters>;
export type FormNumberParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.FormNumberParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.FormNumberParameterRequestParameters>;
export type FormArrayParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.FormArrayParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.FormArrayParameterRequestParameters>;
export type FormExplodeArrayParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.FormExplodeArrayParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.FormExplodeArrayParameterRequestParameters>;
export type FormObjectParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.FormObjectParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.FormObjectParameterRequestParameters>;
export type FormExplodeObjectParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.FormExplodeObjectParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.FormExplodeObjectParameterRequestParameters>;
export type FormDictionaryParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.FormDictionaryParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.FormDictionaryParameterRequestParameters>;
export type FormExplodeDictionaryParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.FormExplodeDictionaryParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.FormExplodeDictionaryParameterRequestParameters>;
export type DeepObjectObjectParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.DeepObjectObjectParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.DeepObjectObjectParameterRequestParameters>;
export type DeepObjectDictionaryParameterOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.DeepObjectDictionaryParameterRequestParameters> | lib.OutgoingEmptyRequest<shared.DeepObjectDictionaryParameterRequestParameters>;
export type UnsupportedResponseOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.UnsupportedResponseRequestParameters> | lib.OutgoingEmptyRequest<shared.UnsupportedResponseRequestParameters>;
export type UnsupportedRequestOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.UnsupportedRequestRequestParameters> | lib.OutgoingEmptyRequest<shared.UnsupportedRequestRequestParameters>;
export type UnsupportedButNotRequiredRequestOutgoingRequest = lib.OutgoingEmptyRequestDefault<shared.UnsupportedButNotRequiredRequestRequestParameters> | lib.OutgoingEmptyRequest<shared.UnsupportedButNotRequiredRequestRequestParameters>;
