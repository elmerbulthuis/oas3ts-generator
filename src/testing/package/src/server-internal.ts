/* eslint-disable */

import * as lib from "@oas3/oas3ts-lib";
import * as shared from "./shared.js";
export function extractBasicCredential(parameters: lib.Parameters): undefined | shared.BasicCredential {
    const authorizationHeader = lib.getParameterValue(parameters, "authorization");
    if (authorizationHeader == null)
        return;
    const parameterValue = lib.parseBasicAuthorizationHeader(authorizationHeader);
    if (parameterValue == null)
        return;
    lib.clearParameter(parameters, "authorization");
    return parameterValue;
}
export function extractApiTokenCredential(parameters: lib.Parameters): undefined | shared.ApiTokenCredential {
    const authorizationHeader = lib.getParameterValue(parameters, "authorization");
    if (authorizationHeader == null)
        return;
    const parameterValue = lib.parseAuthorizationHeader("bearer", authorizationHeader);
    if (parameterValue == null)
        return;
    lib.clearParameter(parameters, "authorization");
    return parameterValue;
}
export function extractApiKeyCredential(parameters: lib.Parameters): undefined | shared.ApiKeyCredential {
    const parameterValue = lib.getParameterValue(parameters, "Authorization".toLowerCase());
    if (parameterValue == null)
        return;
    lib.clearParameter(parameters, "ApiKey".toLowerCase());
    return parameterValue;
}
export function extractAccessTokenCredential(parameters: lib.Parameters): undefined | shared.AccessTokenCredential {
    const parameterValue = lib.getParameterValue(parameters, "access_token");
    if (parameterValue == null)
        return;
    lib.clearParameter(parameters, "access-token");
    return decodeURIComponent(parameterValue);
}
export function extractSessionMetricsOrganizationRequestParameter(parameters: lib.Parameters): undefined | shared.SessionMetricsOrganizationParameterSchema {
    const parameterValue = lib.getParameterValue(parameters, "organization");
    if (parameterValue == null)
        return;
    lib.clearParameter(parameters, "organization");
    return decodeURIComponent(parameterValue);
}
export function extractSimpleStringParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.SimpleStringParameterVAlueParameterSchema {
    const parameterValue = lib.getParameterValue(parameters, "V-Alue".toLowerCase());
    if (parameterValue == null)
        return;
    lib.clearParameter(parameters, "V-Alue".toLowerCase());
    return parameterValue;
}
export function extractSimpleNumberParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.SimpleNumberParameterVAlueParameterSchema {
    const parameterValue = lib.getParameterValue(parameters, "V-Alue".toLowerCase());
    if (parameterValue == null)
        return;
    lib.clearParameter(parameters, "V-Alue".toLowerCase());
    return Number(parameterValue);
}
export function extractSimpleArrayParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.SimpleArrayParameterVAlueParameterSchema {
    const parameterValue = lib.getParameterValue(parameters, "V-Alue".toLowerCase());
    if (parameterValue == null)
        return;
    const parameterValues = parameterValue.split(",");
    if (parameterValues.length === 0)
        return;
    lib.clearParameter(parameters, "V-Alue".toLowerCase());
    return parameterValues.map(parameterValue => parameterValue);
}
export function extractSimpleExplodeArrayParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.SimpleExplodeArrayParameterVAlueParameterSchema {
    const parameterValue = lib.getParameterValue(parameters, "V-Alue".toLowerCase());
    if (parameterValue == null)
        return;
    const parameterValues = parameterValue.split(",");
    if (parameterValues.length === 0)
        return;
    lib.clearParameter(parameters, "V-Alue".toLowerCase());
    return parameterValues.map(parameterValue => parameterValue);
}
export function extractSimpleObjectParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.SimpleObjectParameterVAlueParameterSchema {
    const value: any = {};
    const parameterValue = lib.getParameterValue(parameters, "V-Alue".toLowerCase());
    if (parameterValue == null)
        return;
    lib.clearParameter(parameters, "V-Alue".toLowerCase());
    const encodedValues = Object.fromEntries(lib.parseParameterEntries(parameterValue, "", ",", ","));
    {
        const encodedValue = encodedValues["str"];
        if (encodedValue != null) {
            value["str"] = encodedValue;
        }
    }
    {
        const encodedValue = encodedValues["num"];
        if (encodedValue != null) {
            value["num"] = Number(encodedValue);
        }
    }
    return value as shared.SimpleObjectParameterVAlueParameterSchema;
}
export function extractSimpleExplodeObjectParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.SimpleExplodeObjectParameterVAlueParameterSchema {
    const value: any = {};
    const parameterValue = lib.getParameterValue(parameters, "V-Alue".toLowerCase());
    if (parameterValue == null)
        return;
    lib.clearParameter(parameters, "V-Alue".toLowerCase());
    const encodedValues = Object.fromEntries(lib.parseParameterEntries(parameterValue, "", ",", "="));
    {
        const encodedValue = encodedValues["str"];
        if (encodedValue != null) {
            value["str"] = encodedValue;
        }
    }
    {
        const encodedValue = encodedValues["num"];
        if (encodedValue != null) {
            value["num"] = Number(encodedValue);
        }
    }
    return value as shared.SimpleExplodeObjectParameterVAlueParameterSchema;
}
export function extractSimpleDictionaryParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.SimpleDictionaryParameterVAlueParameterSchema {
    const value: any = {};
    const parameterValue = lib.getParameterValue(parameters, "V-Alue".toLowerCase());
    if (parameterValue == null)
        return;
    lib.clearParameter(parameters, "V-Alue".toLowerCase());
    for (const [propertyName, encodedValue] of lib.parseParameterEntries(parameterValue, "", ",", ",")) {
        value[propertyName] = encodedValue;
    }
    return value as shared.SimpleDictionaryParameterVAlueParameterSchema;
}
export function extractSimpleExplodeDictionaryParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.SimpleExplodeDictionaryParameterVAlueParameterSchema {
    const value: any = {};
    const parameterValue = lib.getParameterValue(parameters, "V-Alue".toLowerCase());
    if (parameterValue == null)
        return;
    lib.clearParameter(parameters, "V-Alue".toLowerCase());
    for (const [propertyName, encodedValue] of lib.parseParameterEntries(parameterValue, "", ",", "=")) {
        value[propertyName] = encodedValue;
    }
    return value as shared.SimpleExplodeDictionaryParameterVAlueParameterSchema;
}
export function extractLabelStringParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.LabelStringParameterVAlueParameterSchema {
    const parameterValue = lib.getParameterValue(parameters, "V-Alue");
    if (parameterValue == null)
        return;
    if (!parameterValue.startsWith("."))
        return;
    lib.clearParameter(parameters, "V-Alue");
    return decodeURIComponent(parameterValue.substring(1));
}
export function extractLabelNumberParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.LabelNumberParameterVAlueParameterSchema {
    const parameterValue = lib.getParameterValue(parameters, "V-Alue");
    if (parameterValue == null)
        return;
    if (!parameterValue.startsWith("."))
        return;
    lib.clearParameter(parameters, "V-Alue");
    return Number(parameterValue.substring(1));
}
export function extractLabelArrayParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.LabelArrayParameterVAlueParameterSchema {
    const parameterValue = lib.getParameterValue(parameters, "V-Alue");
    if (parameterValue == null)
        return;
    if (!parameterValue.startsWith("."))
        return;
    const parameterValues = (parameterValue.substring)(1).split(",");
    if (parameterValues.length === 0)
        return;
    lib.clearParameter(parameters, "V-Alue");
    return parameterValues.map(parameterValue => decodeURIComponent(parameterValue));
}
export function extractLabelExplodeArrayParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.LabelExplodeArrayParameterVAlueParameterSchema {
    const parameterValue = lib.getParameterValue(parameters, "V-Alue");
    if (parameterValue == null)
        return;
    if (!parameterValue.startsWith("."))
        return;
    const parameterValues = (parameterValue.substring)(1).split(".");
    if (parameterValues.length === 0)
        return;
    lib.clearParameter(parameters, "V-Alue");
    return parameterValues.map(parameterValue => decodeURIComponent(parameterValue));
}
export function extractLabelObjectParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.LabelObjectParameterVAlueParameterSchema {
    const value: any = {};
    const parameterValue = lib.getParameterValue(parameters, "V-Alue");
    if (parameterValue == null)
        return;
    if (!parameterValue.startsWith("."))
        return;
    lib.clearParameter(parameters, "V-Alue");
    const encodedValues = Object.fromEntries(lib.parseParameterEntries(parameterValue, ".", ",", ","));
    {
        const encodedValue = encodedValues["str"];
        if (encodedValue != null) {
            value["str"] = decodeURIComponent(encodedValue);
        }
    }
    {
        const encodedValue = encodedValues["num"];
        if (encodedValue != null) {
            value["num"] = Number(encodedValue);
        }
    }
    return value as shared.LabelObjectParameterVAlueParameterSchema;
}
export function extractLabelExplodeObjectParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.LabelExplodeObjectParameterVAlueParameterSchema {
    const value: any = {};
    const parameterValue = lib.getParameterValue(parameters, "V-Alue");
    if (parameterValue == null)
        return;
    if (!parameterValue.startsWith("."))
        return;
    lib.clearParameter(parameters, "V-Alue");
    const encodedValues = Object.fromEntries(lib.parseParameterEntries(parameterValue, ".", ".", "="));
    {
        const encodedValue = encodedValues["str"];
        if (encodedValue != null) {
            value["str"] = decodeURIComponent(encodedValue);
        }
    }
    {
        const encodedValue = encodedValues["num"];
        if (encodedValue != null) {
            value["num"] = Number(encodedValue);
        }
    }
    return value as shared.LabelExplodeObjectParameterVAlueParameterSchema;
}
export function extractLabelDictionaryParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.LabelDictionaryParameterVAlueParameterSchema {
    const value: any = {};
    const parameterValue = lib.getParameterValue(parameters, "V-Alue");
    if (parameterValue == null)
        return;
    if (!parameterValue.startsWith("."))
        return;
    lib.clearParameter(parameters, "V-Alue");
    for (const [propertyName, encodedValue] of lib.parseParameterEntries(parameterValue, ".", ",", ",")) {
        value[propertyName] = decodeURIComponent(encodedValue);
    }
    return value as shared.LabelDictionaryParameterVAlueParameterSchema;
}
export function extractLabelExplodeDictionaryParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.LabelExplodeDictionaryParameterVAlueParameterSchema {
    const value: any = {};
    const parameterValue = lib.getParameterValue(parameters, "V-Alue");
    if (parameterValue == null)
        return;
    if (!parameterValue.startsWith("."))
        return;
    lib.clearParameter(parameters, "V-Alue");
    for (const [propertyName, encodedValue] of lib.parseParameterEntries(parameterValue, ".", ".", "=")) {
        value[propertyName] = decodeURIComponent(encodedValue);
    }
    return value as shared.LabelExplodeDictionaryParameterVAlueParameterSchema;
}
export function extractFormStringParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.FormStringParameterVAlueParameterSchema {
    const parameterValue = lib.getParameterValue(parameters, "V-Alue");
    if (parameterValue == null)
        return;
    lib.clearParameter(parameters, "V-Alue");
    return decodeURIComponent(parameterValue);
}
export function extractFormNumberParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.FormNumberParameterVAlueParameterSchema {
    const parameterValue = lib.getParameterValue(parameters, "V-Alue");
    if (parameterValue == null)
        return;
    lib.clearParameter(parameters, "V-Alue");
    return Number(parameterValue);
}
export function extractFormArrayParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.FormArrayParameterVAlueParameterSchema {
    const parameterValue = lib.getParameterValue(parameters, "V-Alue");
    if (parameterValue == null)
        return;
    const parameterValues = parameterValue.split(",");
    if (parameterValues.length === 0)
        return;
    lib.clearParameter(parameters, "V-Alue");
    return parameterValues.map(parameterValue => decodeURIComponent(parameterValue));
}
export function extractFormExplodeArrayParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.FormExplodeArrayParameterVAlueParameterSchema {
    const parameterValues = lib.getParameterValues(parameters, "V-Alue");
    if (parameterValues == null)
        return;
    if (parameterValues.length === 0)
        return;
    lib.clearParameter(parameters, "V-Alue");
    return parameterValues.map(parameterValue => decodeURIComponent(parameterValue));
}
export function extractFormObjectParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.FormObjectParameterVAlueParameterSchema {
    const value: any = {};
    const parameterValue = lib.getParameterValue(parameters, "V-Alue");
    if (parameterValue == null)
        return;
    lib.clearParameter(parameters, "V-Alue");
    const encodedValues = Object.fromEntries(lib.parseParameterEntries(parameterValue, "", ",", ","));
    {
        const encodedValue = encodedValues["str"];
        if (encodedValue != null) {
            value["str"] = decodeURIComponent(encodedValue);
        }
    }
    {
        const encodedValue = encodedValues["num"];
        if (encodedValue != null) {
            value["num"] = Number(encodedValue);
        }
    }
    return value as shared.FormObjectParameterVAlueParameterSchema;
}
export function extractFormExplodeObjectParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.FormExplodeObjectParameterVAlueParameterSchema {
    const value: any = {};
    {
        const parameterValue = lib.getParameterValue(parameters, "str");
        if (parameterValue != null) {
            lib.clearParameter(parameters, "str");
            value["str"] = decodeURIComponent(parameterValue);
        }
    }
    {
        const parameterValue = lib.getParameterValue(parameters, "num");
        if (parameterValue != null) {
            lib.clearParameter(parameters, "num");
            value["num"] = Number(parameterValue);
        }
    }
    return value as shared.FormExplodeObjectParameterVAlueParameterSchema;
}
export function extractFormDictionaryParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.FormDictionaryParameterVAlueParameterSchema {
    const value: any = {};
    const parameterValue = lib.getParameterValue(parameters, "V-Alue");
    if (parameterValue == null)
        return;
    lib.clearParameter(parameters, "V-Alue");
    for (const [propertyName, encodedValue] of lib.parseParameterEntries(parameterValue, "", ",", ",")) {
        value[propertyName] = decodeURIComponent(encodedValue);
    }
    return value as shared.FormDictionaryParameterVAlueParameterSchema;
}
export function extractFormExplodeDictionaryParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.FormExplodeDictionaryParameterVAlueParameterSchema {
    const value: any = {};
    for (const parameterName of lib.getAllParameterNames(parameters)) {
        const parameterValue = lib.getParameterValue(parameters, parameterName);
        if (parameterValue == null)
            continue;
        lib.clearParameter(parameters, parameterName);
        value[parameterName] = decodeURIComponent(parameterValue);
    }
    return value as shared.FormExplodeDictionaryParameterVAlueParameterSchema;
}
export function extractDeepObjectObjectParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.DeepObjectObjectParameterVAlueParameterSchema {
    const value: any = {};
    {
        const parameterName = lib.stringifyDeepObjectPropertyName("V-Alue", "str");
        const parameterValue = lib.getParameterValue(parameters, parameterName);
        if (parameterValue != null) {
            lib.clearParameter(parameters, parameterName);
            value["str"] = decodeURIComponent(parameterValue);
        }
    }
    {
        const parameterName = lib.stringifyDeepObjectPropertyName("V-Alue", "num");
        const parameterValue = lib.getParameterValue(parameters, parameterName);
        if (parameterValue != null) {
            lib.clearParameter(parameters, parameterName);
            value["num"] = Number(parameterValue);
        }
    }
    return value as shared.DeepObjectObjectParameterVAlueParameterSchema;
}
export function extractDeepObjectDictionaryParameterVAlueRequestParameter(parameters: lib.Parameters): undefined | shared.DeepObjectDictionaryParameterVAlueParameterSchema {
    const value: any = {};
    for (const parameterName of lib.getAllParameterNames(parameters)) {
        const propertyName = lib.parseDeepObjectPropertyName(parameterName, "V-Alue");
        if (propertyName == null)
            continue;
        const parameterValue = lib.getParameterValue(parameters, parameterName);
        if (parameterValue == null)
            continue;
        lib.clearParameter(parameters, parameterName);
        value[propertyName] = decodeURIComponent(parameterValue);
    }
    return value as shared.DeepObjectDictionaryParameterVAlueParameterSchema;
}
export function deserializeEchoApplicationJson(stream: (signal?: AbortSignal) => AsyncIterable<Uint8Array>, validate: boolean): lib.IncomingJsonContainer<shared.EchoMessageSchema> {
    return {
        stream,
        async entity() {
            try {
                const entity = await lib.deserializeJsonEntity<shared.EchoMessageSchema>(stream);
                if (validate) {
                    const invalidPaths = [...shared.validateEchoMessageSchema(entity)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingRequestEntityValidationError(invalidPaths);
                    }
                }
                return entity;
            }
            catch (error) {
                if (error instanceof lib.DeserializationError) {
                    throw new lib.IncomingRequestEntityDeserializationError();
                }
                throw error;
            }
        },
        async *entities(signal?: AbortSignal) {
            try {
                for await (const entity of lib.deserializeJsonEntities<shared.EchoMessageSchema>(stream, signal)) {
                    if (validate) {
                        const invalidPaths = [...shared.validateEchoMessageSchema(entity)];
                        if (invalidPaths.length > 0) {
                            throw new lib.IncomingRequestEntityValidationError(invalidPaths);
                        }
                    }
                    yield entity;
                }
            }
            catch (error) {
                if (error instanceof lib.DeserializationError) {
                    throw new lib.IncomingRequestEntityDeserializationError();
                }
                throw error;
            }
        }
    };
}
export function deserializeEchoApplicationXWwwFormUrlencoded(stream: (signal?: AbortSignal) => AsyncIterable<Uint8Array>, validate: boolean): lib.IncomingFormContainer<shared.EchoMessageSchema> {
    return {
        stream,
        async entity() {
            try {
                const entity = await lib.deserializeFormEntity<shared.EchoMessageSchema>(stream);
                if (validate) {
                    const invalidPaths = [...shared.validateEchoMessageSchema(entity)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingRequestEntityValidationError(invalidPaths);
                    }
                }
                return entity;
            }
            catch (error) {
                if (error instanceof lib.DeserializationError) {
                    throw new lib.IncomingRequestEntityDeserializationError();
                }
                throw error;
            }
        }
    };
}
export function deserializeEchoTextPlain(stream: (signal?: AbortSignal) => AsyncIterable<Uint8Array>, validate: boolean): lib.IncomingTextContainer {
    return {
        stream,
        async value() {
            try {
                const value = await lib.deserializeTextValue(stream);
                return value;
            }
            catch (error) {
                if (error instanceof lib.DeserializationError) {
                    throw new lib.IncomingRequestEntityDeserializationError();
                }
                throw error;
            }
        },
        async *lines(signal?: AbortSignal) {
            try {
                const lines = lib.deserializeTextLines(stream, signal);
                yield* lines;
            }
            catch (error) {
                if (error instanceof lib.DeserializationError) {
                    throw new lib.IncomingRequestEntityDeserializationError();
                }
                throw error;
            }
        }
    };
}
export function deserializeEchoApplicationOctetStream(stream: (signal?: AbortSignal) => AsyncIterable<Uint8Array>, validate: boolean): lib.IncomingStreamContainer {
    return { stream };
}
export function serializeSessionMetrics200ResponseApplicationJson(container: lib.OutgoingJsonContainer<shared.SessionMetrics200ApplicationJsonResponseSchema>, validate: boolean): (signal?: AbortSignal) => AsyncIterable<Uint8Array> {
    if ("entity" in container) {
        return () => lib.serializeJsonEntity((async function () {
            try {
                const entity = await container.entity();
                if (validate) {
                    const invalidPaths = [...shared.validateSessionMetrics200ApplicationJsonResponseSchema(entity)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseEntityValidationError(invalidPaths);
                    }
                }
                return entity;
            }
            catch (error) {
                if (error instanceof lib.SerializationError) {
                    throw new lib.OutgoingResponseEntitySerializationError();
                }
                throw error;
            }
        })());
    }
    if ("entities" in container) {
        return (signal?: AbortSignal) => lib.serializeJsonEntities((async function* () {
            try {
                for await (const entity of container.entities(signal)) {
                    if (validate) {
                        const invalidPaths = [...shared.validateSessionMetrics200ApplicationJsonResponseSchema(entity)];
                        if (invalidPaths.length > 0) {
                            throw new lib.OutgoingResponseEntityValidationError(invalidPaths);
                        }
                    }
                    yield entity;
                }
            }
            catch (error) {
                if (error instanceof lib.SerializationError) {
                    throw new lib.OutgoingResponseEntitySerializationError();
                }
                throw error;
            }
        })());
    }
    if ("stream" in container) {
        return container.stream;
    }
    throw new TypeError("bad container");
}
export function serializeMetrics200ResponseTextPlain(container: lib.OutgoingTextContainer, validate: boolean): (signal?: AbortSignal) => AsyncIterable<Uint8Array> {
    if ("value" in container) {
        return () => lib.serializeTextValue((async function () {
            try {
                const value = await container.value();
                return value;
            }
            catch (error) {
                if (error instanceof lib.SerializationError) {
                    throw new lib.OutgoingResponseEntitySerializationError();
                }
                throw error;
            }
        })());
    }
    if ("lines" in container) {
        return (signal?: AbortSignal) => lib.serializeTextLines((async function* () {
            try {
                const lines = container.lines(signal);
                yield* lines;
            }
            catch (error) {
                if (error instanceof lib.SerializationError) {
                    throw new lib.OutgoingResponseEntitySerializationError();
                }
                throw error;
            }
        })());
    }
    if ("stream" in container) {
        return container.stream;
    }
    throw new TypeError("bad container");
}
export function serializeEcho200ResponseApplicationJson(container: lib.OutgoingJsonContainer<shared.EchoMessageSchema>, validate: boolean): (signal?: AbortSignal) => AsyncIterable<Uint8Array> {
    if ("entity" in container) {
        return () => lib.serializeJsonEntity((async function () {
            try {
                const entity = await container.entity();
                if (validate) {
                    const invalidPaths = [...shared.validateEchoMessageSchema(entity)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseEntityValidationError(invalidPaths);
                    }
                }
                return entity;
            }
            catch (error) {
                if (error instanceof lib.SerializationError) {
                    throw new lib.OutgoingResponseEntitySerializationError();
                }
                throw error;
            }
        })());
    }
    if ("entities" in container) {
        return (signal?: AbortSignal) => lib.serializeJsonEntities((async function* () {
            try {
                for await (const entity of container.entities(signal)) {
                    if (validate) {
                        const invalidPaths = [...shared.validateEchoMessageSchema(entity)];
                        if (invalidPaths.length > 0) {
                            throw new lib.OutgoingResponseEntityValidationError(invalidPaths);
                        }
                    }
                    yield entity;
                }
            }
            catch (error) {
                if (error instanceof lib.SerializationError) {
                    throw new lib.OutgoingResponseEntitySerializationError();
                }
                throw error;
            }
        })());
    }
    if ("stream" in container) {
        return container.stream;
    }
    throw new TypeError("bad container");
}
export function serializeEcho200ResponseApplicationXWwwFormUrlencoded(container: lib.OutgoingFormContainer<shared.EchoMessageSchema>, validate: boolean): (signal?: AbortSignal) => AsyncIterable<Uint8Array> {
    if ("entity" in container) {
        return () => lib.serializeFormEntity((async function () {
            try {
                const entity = await container.entity();
                if (validate) {
                    const invalidPaths = [...shared.validateEchoMessageSchema(entity)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseEntityValidationError(invalidPaths);
                    }
                }
                return entity;
            }
            catch (error) {
                if (error instanceof lib.SerializationError) {
                    throw new lib.OutgoingResponseEntitySerializationError();
                }
                throw error;
            }
        })());
    }
    if ("stream" in container) {
        return container.stream;
    }
    throw new TypeError("bad container");
}
export function serializeEcho200ResponseTextPlain(container: lib.OutgoingTextContainer, validate: boolean): (signal?: AbortSignal) => AsyncIterable<Uint8Array> {
    if ("value" in container) {
        return () => lib.serializeTextValue((async function () {
            try {
                const value = await container.value();
                return value;
            }
            catch (error) {
                if (error instanceof lib.SerializationError) {
                    throw new lib.OutgoingResponseEntitySerializationError();
                }
                throw error;
            }
        })());
    }
    if ("lines" in container) {
        return (signal?: AbortSignal) => lib.serializeTextLines((async function* () {
            try {
                const lines = container.lines(signal);
                yield* lines;
            }
            catch (error) {
                if (error instanceof lib.SerializationError) {
                    throw new lib.OutgoingResponseEntitySerializationError();
                }
                throw error;
            }
        })());
    }
    if ("stream" in container) {
        return container.stream;
    }
    throw new TypeError("bad container");
}
export function serializeEcho200ResponseApplicationOctetStream(container: lib.OutgoingStreamContainer, validate: boolean): (signal?: AbortSignal) => AsyncIterable<Uint8Array> {
    return container.stream;
}
export function serializeError400ResponseApplicationJson(container: lib.OutgoingJsonContainer<shared.ErrorResultSchema>, validate: boolean): (signal?: AbortSignal) => AsyncIterable<Uint8Array> {
    if ("entity" in container) {
        return () => lib.serializeJsonEntity((async function () {
            try {
                const entity = await container.entity();
                if (validate) {
                    const invalidPaths = [...shared.validateErrorResultSchema(entity)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseEntityValidationError(invalidPaths);
                    }
                }
                return entity;
            }
            catch (error) {
                if (error instanceof lib.SerializationError) {
                    throw new lib.OutgoingResponseEntitySerializationError();
                }
                throw error;
            }
        })());
    }
    if ("entities" in container) {
        return (signal?: AbortSignal) => lib.serializeJsonEntities((async function* () {
            try {
                for await (const entity of container.entities(signal)) {
                    if (validate) {
                        const invalidPaths = [...shared.validateErrorResultSchema(entity)];
                        if (invalidPaths.length > 0) {
                            throw new lib.OutgoingResponseEntityValidationError(invalidPaths);
                        }
                    }
                    yield entity;
                }
            }
            catch (error) {
                if (error instanceof lib.SerializationError) {
                    throw new lib.OutgoingResponseEntitySerializationError();
                }
                throw error;
            }
        })());
    }
    if ("stream" in container) {
        return container.stream;
    }
    throw new TypeError("bad container");
}
