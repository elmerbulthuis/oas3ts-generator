import test from "tape-promise/tape.js";
import { validateStringOrNumberSchema } from "./package/src/shared.js";

test("one-of string or number", async t => {
    {
        const result = [...validateStringOrNumberSchema("a")];
        t.deepEqual(result, []);
    }

    {
        const result = [...validateStringOrNumberSchema(1)];
        t.deepEqual(result, []);
    }

    {
        const result = [...validateStringOrNumberSchema(true as any)];
        t.deepEqual(result, [[]]);
    }
});
