import test from "tape-promise/tape.js";
import { injectFormArrayParameterVAlueRequestParameter, injectFormDictionaryParameterVAlueRequestParameter, injectFormExplodeArrayParameterVAlueRequestParameter, injectFormExplodeDictionaryParameterVAlueRequestParameter, injectFormExplodeObjectParameterVAlueRequestParameter, injectFormNumberParameterVAlueRequestParameter, injectFormObjectParameterVAlueRequestParameter, injectFormStringParameterVAlueRequestParameter } from "./package/src/client-internal.js";
import { extractFormArrayParameterVAlueRequestParameter, extractFormDictionaryParameterVAlueRequestParameter, extractFormExplodeArrayParameterVAlueRequestParameter, extractFormExplodeDictionaryParameterVAlueRequestParameter, extractFormExplodeObjectParameterVAlueRequestParameter, extractFormNumberParameterVAlueRequestParameter, extractFormObjectParameterVAlueRequestParameter, extractFormStringParameterVAlueRequestParameter } from "./package/src/server-internal.js";

test("inject form string", async t => {
    const parameters = {};
    const value = ":/?#[]@!$&'()*+,;= ";
    injectFormStringParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "V-Alue": "%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20",
    });
});

test("extract form string", async t => {
    const parameters = {
        "V-Alue": "%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20",
    };
    const value = extractFormStringParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, ":/?#[]@!$&'()*+,;= ");
});

test("inject form number", async t => {
    const parameters = {};
    const value = 10;
    injectFormNumberParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "V-Alue": "10",
    });
});

test("extract form number", async t => {
    const parameters = {
        "V-Alue": "10",
    };
    const value = extractFormNumberParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, 10);
});

test("inject form array", async t => {
    const parameters = {};
    const value = [
        ":/?#[]@!$&'()*+,;= ",
        "2",
    ];
    injectFormArrayParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "V-Alue": "%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20,2",
    });
});

test("extract form array", async t => {
    const parameters = {
        "V-Alue": "%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20,2",
    };
    const value = extractFormArrayParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, [
        ":/?#[]@!$&'()*+,;= ",
        "2",
    ]);
});

test("inject form-explode array", async t => {
    const parameters = {};
    const value = [
        ":/?#[]@!$&'()*+,;= ",
        "2",
    ];
    injectFormExplodeArrayParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "V-Alue": [
            "%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20",
            "2",
        ],
    });
});

test("extract form-explode array", async t => {
    const parameters = {
        "V-Alue": [
            "%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20",
            "2",
        ],
    };
    const value = extractFormExplodeArrayParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, [
        ":/?#[]@!$&'()*+,;= ",
        "2",
    ]);
});

test("inject form object", async t => {
    const parameters = {};
    const value = {
        str: ":/?#[]@!$&'()*+,;= ",
        num: 2,
    };
    injectFormObjectParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "V-Alue": "str,%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20,num,2",
    });
});

test("extract form object", async t => {
    const parameters = {
        "V-Alue": "str,%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20,num,2",
    };
    const value = extractFormObjectParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, {
        str: ":/?#[]@!$&'()*+,;= ",
        num: 2,
    });
});

test("inject form-explode object", async t => {
    const parameters = {};
    const value = {
        str: ":/?#[]@!$&'()*+,;= ",
        num: 2,
    };
    injectFormExplodeObjectParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "str": "%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20",
        "num": "2",
    });
});

test("extract form-explode object", async t => {
    const parameters = {
        "str": "%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20",
        "num": "2",
    };
    const value = extractFormExplodeObjectParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, {
        str: ":/?#[]@!$&'()*+,;= ",
        num: 2,
    });
});

test("inject form dictionary", async t => {
    const parameters = {};
    const value = {
        a: ":/?#[]@!$&'()*+,;= ",
        b: "2",
    };
    injectFormDictionaryParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "V-Alue": "a,%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20,b,2",
    });
});

test("extract form dictionary", async t => {
    const parameters = {
        "V-Alue": "a,%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20,b,2",
    };
    const value = extractFormDictionaryParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, {
        a: ":/?#[]@!$&'()*+,;= ",
        b: "2",
    });
});

test("inject form-explode dictionary", async t => {
    const parameters = {};
    const value = {
        a: ":/?#[]@!$&'()*+,;= ",
        b: "2",
    };
    injectFormExplodeDictionaryParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "a": "%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20",
        "b": "2",
    });
});

test("extract form-explode dictionary", async t => {
    const parameters = {
        "a": "%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20",
        "b": "2",
    };
    const value = extractFormExplodeDictionaryParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, {
        a: ":/?#[]@!$&'()*+,;= ",
        b: "2",
    });
});
