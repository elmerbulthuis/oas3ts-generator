import test from "tape-promise/tape.js";
import { validateTupleSchema } from "./package/src/shared.js";

test("tuples test", async t => {
    {
        const result = [...validateTupleSchema([1, "a"])];
        t.deepEqual(result, []);
    }

    {
        const result = [...validateTupleSchema([1, 2 as any])];
        t.notDeepEqual(result, []);
    }

});
