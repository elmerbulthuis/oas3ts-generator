import * as oas3ts from "@oas3/oas3ts-lib";
import assert from "assert";
import test from "tape-promise/tape.js";
import { injectApiKeyCredential, withContext } from "./index.js";

test("basic-auth", t => withContext(async context => {
    initializeMocks();

    const client = context.createClient({
        basic: {
            username: "x",
            password: "y",
        },
    });

    const basicAuthResult = await client.basicAuth({
        parameters: {},
    });
    assert(basicAuthResult.status === 204);

    function initializeMocks() {
        context.server.registerBasicAuthOperation(async incomingRequest => {
            return {
                status: 204,
                parameters: {},
            };
        });

        context.server.registerBasicAuthorization(credentials => {
            return true;
        });
    }
}));

test("bearer auth without bearer prefix", t => withContext(async context => {
    const credentials = {
        APIKey: "MY_API_KEY",
    };

    const headers: oas3ts.Parameters = {};
    injectApiKeyCredential(headers, credentials.APIKey);

    t.equal(headers["authorization"], credentials.APIKey);
}));

