import { OpenAPIV3 } from "openapi-types";

export function selectSchemaType(schemaObject: OpenAPIV3.SchemaObject) {
    if ("type" in schemaObject) return schemaObject.type;

    if ("properties" in schemaObject) return "object";
    if ("additionalProperties" in schemaObject) return "object";
    if ("items" in schemaObject) return "array";
    if ("enum" in schemaObject) return "string";
}
