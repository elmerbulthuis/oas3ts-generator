import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import { isReferenceObject, resolveObject, resolvePointerParts } from "../utils/index.js";

export function selectAllRequestParameters(
    document: OpenAPIV3.Document,
) {
    return selectFromDocument([], []);

    function* selectFromDocument(
        pointerParts: Array<string>,
        nameParts: Array<string>,
    ) {
        for (
            const [path, pathObject] of
            Object.entries(document.paths)
        ) {
            yield* selectFromPath(
                pathObject,
                [...pointerParts, "paths", path],
                [...nameParts],
            );
        }

        for (
            const [parameter, parameterObject] of
            Object.entries(document.components?.parameters ?? {})
        ) {
            yield* selectFromParameter(
                parameterObject,
                [...pointerParts, "components", "parameters", parameter],
                [...nameParts],
            );
        }

    }

    function* selectFromPath(
        pathObject: OpenAPIV3.ReferenceObject | OpenAPIV3.PathItemObject | undefined,
        pointerParts: Array<string>,
        nameParts: Array<string>,
    ) {
        if (!pathObject) return;
        if (isReferenceObject(pathObject)) return;

        for (
            const [parameter, parameterObject] of
            Object.entries(pathObject.parameters ?? {})
        ) {
            yield* selectFromParameter(
                parameterObject,
                [...pointerParts, "parameters", parameter],
                [...nameParts],
            );
        }

        for (const method of Object.values(OpenAPIV3.HttpMethods)) {
            const operationObject = pathObject[method];

            yield* selectFromOperation(
                operationObject,
                [...pointerParts, method],
                [...nameParts],
            );
        }
    }

    function* selectFromOperation(
        operationObject: OpenAPIV3.OperationObject | undefined,
        pointerParts: Array<string>,
        nameParts: Array<string>,
    ) {
        if (!operationObject) return;

        assert(operationObject.operationId);

        for (
            const [parameter, parameterObject] of
            Object.entries(operationObject.parameters ?? [])
        ) {
            yield* selectFromParameter(
                parameterObject,
                [...pointerParts, "parameters", parameter],
                [...nameParts, operationObject.operationId],
            );
        }

    }

    function* selectFromParameter(
        parameterObject: OpenAPIV3.ReferenceObject | OpenAPIV3.ParameterObject,
        pointerParts: Array<string>,
        nameParts: Array<string>,
    ) {
        if (isReferenceObject(parameterObject)) return;

        yield {
            parameterObject,
            pointerParts,
            nameParts: [...nameParts, parameterObject.name, "request", "parameter"],
        };
    }
}

export function* selectParametersFromContainer(
    document: OpenAPIV3.Document,
    parametersContainer: OpenAPIV3.PathItemObject | OpenAPIV3.OperationObject,
    parametersContainerPointerParts: string[],
) {
    for (
        const [parameter, maybeParameterObject] of
        Object.entries(parametersContainer.parameters ?? {})
    ) {
        const parameterObject = resolveObject(document, maybeParameterObject);
        assert(parameterObject);

        const parameterPointerParts = resolvePointerParts(
            [...parametersContainerPointerParts, "parameters", parameter],
            maybeParameterObject,
        );
        assert(parameterPointerParts);

        const schemaObject = resolveObject(
            document,
            parameterObject.schema,
        );
        assert(schemaObject);

        const schemaPointerParts = resolvePointerParts(
            [...parameterPointerParts, "schema"],
            parameterObject.schema,
        );
        assert(schemaPointerParts);

        yield {
            parameter,
            parameterObject,
            parameterPointerParts,
            schemaObject,
            schemaPointerParts,
        };
    }
}

export function* selectParametersFromPathAndMethod(
    document: OpenAPIV3.Document,
    path: string,
    method: OpenAPIV3.HttpMethods,
) {
    const pathObject = document.paths[path];
    assert(pathObject);
    const operationObject = pathObject[method];
    assert(operationObject);

    yield* selectParametersFromContainer(document, pathObject, ["paths", path]);
    yield* selectParametersFromContainer(document, operationObject, ["paths", path, method]);
}
