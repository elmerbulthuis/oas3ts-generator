import { OpenAPIV3 } from "openapi-types";

export function* selectAllPaths(
    document: OpenAPIV3.Document,
) {
    for (
        const [path, pathObject] of
        Object.entries(document.paths)
    ) {
        if (!pathObject) continue;

        yield {
            path, pathObject,
        };
    }
}

