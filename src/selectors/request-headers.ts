import { OpenAPIV3 } from "openapi-types";
import { resolveObject } from "../utils/index.js";
import { selectRequestContentTypes, selectResponseContentTypes } from "./content-types.js";
import { selectAllOperations } from "./operation.js";
import { selectParametersFromPathAndMethod } from "./request-parameter.js";

export function* selectAllOperationRequestHeaders(
    document: OpenAPIV3.Document,
    availableRequestTypes: string[],
    availableResponseTypes: string[],
) {
    for (
        const { operationObject, pathObject, path, method } of
        selectAllOperations(document)
    ) {
        const headers = new Set<string>();
        const requestContentTypes = new Set(
            selectRequestContentTypes(document, availableRequestTypes, operationObject),
        );
        const responseContentTypes = new Set(
            selectResponseContentTypes(document, availableResponseTypes, operationObject),
        );

        if (requestContentTypes.size > 0) {
            headers.add("content-type");
        }

        if (responseContentTypes.size > 0) {
            headers.add("accept");
        }

        for (const securityRequirement of operationObject.security ?? document.security ?? []) {
            for (const securityScheme of Object.keys(securityRequirement)) {
                const maybeSecuritySchemeObject =
                    document.components?.securitySchemes?.[securityScheme];

                const securitySchemeObject = resolveObject(document, maybeSecuritySchemeObject);
                if (!securitySchemeObject) continue;

                switch (securitySchemeObject.type) {
                    case "apiKey":
                        if (securitySchemeObject.in !== "header") continue;

                        headers.add(securitySchemeObject.name);
                        break;

                    case "http":
                        headers.add("authorization");
                        break;
                }
            }

        }

        for (
            const { parameterObject } of
            selectParametersFromPathAndMethod(document, path, method)
        ) {
            if (parameterObject.in !== "header") continue;

            headers.add(parameterObject.name);
        }

        yield {
            operationObject,
            pathObject,
            path,
            method,
            headers: [...headers],
        };
    }

}
