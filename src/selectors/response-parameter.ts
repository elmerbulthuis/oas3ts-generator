import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import { selectAllResponses } from "../selectors/index.js";
import { isReferenceObject, resolveObject, resolvePointerParts } from "../utils/index.js";

export function* selectAllResponseParameters(
    document: OpenAPIV3.Document,
) {
    for (
        const { responseObject, pointerParts, nameParts } of
        selectAllResponses(document)
    ) {
        for (
            const [header, headerObject] of
            Object.entries(responseObject.headers ?? {})
        ) {
            if (isReferenceObject(headerObject)) return;

            yield {
                header,
                headerObject,
                pointerParts: [...pointerParts, "headers", header],
                nameParts: [...nameParts, header, "response", "parameter"],
            };

        }
    }
}

export function* selectHeadersFromResponse(
    document: OpenAPIV3.Document,
    responseObject: OpenAPIV3.ResponseObject,
    responsePointerParts: string[],
) {
    for (
        const [header, maybeHeaderObject] of
        Object.entries(responseObject.headers ?? {})
    ) {
        const headerPointerParts = resolvePointerParts(
            [...responsePointerParts, "headers", header],
            maybeHeaderObject,
        );
        assert(headerPointerParts);

        const headerObject = resolveObject(document, maybeHeaderObject);
        assert(headerObject);

        const schemaObject = resolveObject(
            document,
            headerObject.schema,
        );
        assert(schemaObject);

        const schemaPointerParts = resolvePointerParts(
            [...headerPointerParts, "schema"],
            headerObject.schema,
        );
        assert(schemaPointerParts);

        yield {
            header,
            headerObject,
            headerPointerParts,
            schemaObject,
            schemaPointerParts,
        };
    }
}
