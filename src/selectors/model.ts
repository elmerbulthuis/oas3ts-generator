import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import { PackageConfig } from "../generators/package.js";
import { isReferenceObject } from "../utils/index.js";

export function selectModel(
    document: OpenAPIV3.Document,
    config: PackageConfig,
) {
    const requestTypeSet = new Set(config.requestTypes);
    const responseTypeSet = new Set(config.responseTypes);

    return selectFromDocument([], []);

    function* selectFromDocument(
        pointerParts: Array<string>,
        nameParts: Array<string>,
    ) {
        for (
            const [path, pathObject] of
            Object.entries(document.paths)
        ) {
            yield* selectFromPath(
                pathObject,
                [...pointerParts, "paths", path],
                [...nameParts],
            );
        }

        for (
            const [schema, schemaObject] of
            Object.entries(document.components?.schemas ?? {})
        ) {
            yield* selectFromSchema(
                schemaObject,
                [...pointerParts, "components", "schemas", schema],
                [...nameParts, schema],
            );
        }

        for (
            const [requestBody, requestBodyObject] of
            Object.entries(document.components?.requestBodies ?? {})
        ) {
            yield* selectFromRequestBody(
                requestBodyObject,
                [...pointerParts, "components", "requestBodies", requestBody],
                [...nameParts, requestBody],
            );

        }

        for (
            const [response, responseObject] of
            Object.entries(document.components?.responses ?? {})
        ) {
            yield* selectFromResponse(
                responseObject,
                [...pointerParts, "components", "responses", response],
                [...nameParts, response],
            );
        }

        for (
            const [parameter, parameterObject] of
            Object.entries(document.components?.parameters ?? {})
        ) {
            yield* selectFromParameter(
                parameterObject,
                [...pointerParts, "components", "parameters", parameter],
                [...nameParts],
            );
        }

        for (
            const [header, headerObject] of
            Object.entries(document.components?.headers ?? {})
        ) {
            yield* selectFromHeader(
                headerObject,
                [...pointerParts, "components", "headers", header],
                [...nameParts, header],
            );
        }
    }

    function* selectFromPath(
        pathObject: OpenAPIV3.ReferenceObject | OpenAPIV3.PathItemObject | undefined,
        pointerParts: Array<string>,
        nameParts: Array<string>,
    ) {
        if (!pathObject) return;
        if (isReferenceObject(pathObject)) return;

        for (
            const [parameter, parameterObject] of
            Object.entries(pathObject.parameters ?? {})
        ) {
            yield* selectFromParameter(
                parameterObject,
                [...pointerParts, "parameters", parameter],
                [...nameParts],
            );
        }

        for (const method of Object.values(OpenAPIV3.HttpMethods)) {
            const operationObject = pathObject[method];

            yield* selectFromOperation(
                operationObject,
                [...pointerParts, method],
                [...nameParts],
            );
        }
    }

    function* selectFromOperation(
        operationObject: OpenAPIV3.OperationObject | undefined,
        pointerParts: Array<string>,
        nameParts: Array<string>,
    ) {
        if (!operationObject) return;

        assert(operationObject.operationId);

        for (
            const [parameter, parameterObject] of
            Object.entries(operationObject.parameters ?? [])
        ) {
            yield* selectFromParameter(
                parameterObject,
                [...pointerParts, "parameters", parameter],
                [...nameParts, operationObject.operationId],
            );
        }

        for (
            const [response, responseObject] of
            Object.entries(operationObject.responses ?? {})
        ) {
            yield* selectFromResponse(
                responseObject,
                [...pointerParts, "responses", response],
                [...nameParts, operationObject.operationId, response],
            );
        }

        if (operationObject.requestBody) {
            yield* selectFromRequestBody(
                operationObject.requestBody,
                [...pointerParts, "requestBody"],
                [...nameParts, operationObject.operationId],
            );
        }
    }

    function* selectFromRequestBody(
        requestBodyObject: OpenAPIV3.RequestBodyObject | OpenAPIV3.ReferenceObject,
        pointerParts: Array<string>,
        nameParts: Array<string>,
    ) {
        if (isReferenceObject(requestBodyObject)) return;

        for (
            const [contentType, contentObject] of
            Object.entries(requestBodyObject.content)
        ) {
            if (!requestTypeSet.has(contentType)) continue;

            yield* selectFromMediaTypeObject(
                contentObject,
                [...pointerParts, "content", contentType],
                [...nameParts, contentType, "requestBody"],
            );
        }
    }

    function* selectFromMediaTypeObject(
        mediaTypeObject: OpenAPIV3.MediaTypeObject | OpenAPIV3.ReferenceObject,
        pointerParts: Array<string>,
        nameParts: Array<string>,
    ) {
        if (isReferenceObject(mediaTypeObject)) return;

        yield* selectFromSchema(
            mediaTypeObject.schema,
            [...pointerParts, "schema"],
            [...nameParts],
        );
    }

    function* selectFromResponse(
        responseObject: OpenAPIV3.ReferenceObject | OpenAPIV3.ResponseObject,
        pointerParts: Array<string>,
        nameParts: Array<string>,
    ) {
        if (isReferenceObject(responseObject)) return;

        for (
            const [contentType, contentObject] of
            Object.entries(responseObject.content ?? {})
        ) {
            if (!responseTypeSet.has(contentType)) continue;

            yield* selectFromSchema(
                contentObject.schema,
                [...pointerParts, "content", contentType, "schema"],
                [...nameParts, contentType, "response"],
            );
        }

        for (
            const [header, headerObject] of
            Object.entries(responseObject.headers ?? {})
        ) {
            yield* selectFromHeader(
                headerObject,
                [...pointerParts, "headers", header],
                [...nameParts, header],
            );
        }
    }

    function* selectFromParameter(
        parameterObject: OpenAPIV3.ReferenceObject | OpenAPIV3.ParameterObject,
        pointerParts: Array<string>,
        nameParts: Array<string>,
    ) {
        if (isReferenceObject(parameterObject)) return;

        yield* selectFromSchema(
            parameterObject.schema,
            [...pointerParts, "schema"],
            [...nameParts, parameterObject.name, "parameter"],
        );
    }

    function* selectFromHeader(
        headerObject: OpenAPIV3.ReferenceObject | OpenAPIV3.HeaderObject,
        pointerParts: Array<string>,
        nameParts: Array<string>,
    ) {
        if (isReferenceObject(headerObject)) return;

        yield* selectFromSchema(
            headerObject.schema,
            [...pointerParts, "schema"],
            [...nameParts, "header"],
        );
    }

    function* selectFromSchema(
        schemaObject: OpenAPIV3.ReferenceObject | OpenAPIV3.SchemaObject | undefined,
        pointerParts: Array<string>,
        nameParts: Array<string>,
    ): Iterable<{
        schemaObject: OpenAPIV3.SchemaObject,
        pointerParts: string[],
        nameParts: string[],
    }> {
        if (!schemaObject) return;
        if (isReferenceObject(schemaObject)) return;

        yield {
            schemaObject,
            pointerParts,
            nameParts: [...nameParts, "schema"],
        };

        for (
            const [property, propertyObject] of
            Object.entries(schemaObject.properties ?? {})
        ) {
            yield* selectFromSchema(
                propertyObject,
                [...pointerParts, "properties", property],
                [...nameParts, property],
            );
        }

        for (
            const [allOf, allOfObject] of
            Object.entries(schemaObject.allOf ?? {})
        ) {
            yield* selectFromSchema(
                allOfObject,
                [...pointerParts, "allOf", allOf],
                [...nameParts, "allOf", allOf],
            );
        }

        for (
            const [anyOf, anyOfObject] of
            Object.entries(schemaObject.anyOf ?? {})
        ) {
            yield* selectFromSchema(
                anyOfObject,
                [...pointerParts, "anyOf", anyOf],
                [...nameParts, "anyOf", anyOf],
            );
        }

        for (
            const [oneOf, oneOfObject] of
            Object.entries(schemaObject.oneOf ?? {})
        ) {
            yield* selectFromSchema(
                oneOfObject,
                [...pointerParts, "oneOf", oneOf],
                [...nameParts, "oneOf", oneOf],
            );
        }

        if (
            "items" in schemaObject
        ) {
            if (Array.isArray(schemaObject.items)) {
                for (
                    const [item, itemObject] of
                    Object.entries(schemaObject.items)
                ) {
                    yield* selectFromSchema(
                        itemObject,
                        [...pointerParts, "items", item],
                        [...nameParts, "items", item],
                    );
                }
            }
            else {
                yield* selectFromSchema(
                    schemaObject.items,
                    [...pointerParts, "items"],
                    [...nameParts, "items"],
                );

            }
        }

        if (
            typeof schemaObject.additionalProperties === "object"
        ) {
            yield* selectFromSchema(
                schemaObject.additionalProperties,
                [...pointerParts, "additionalProperties"],
                [...nameParts, "additionalProperties"],
            );
        }

        yield* selectFromSchema(
            schemaObject.not,
            [...pointerParts, "not"],
            [...nameParts, "not"],
        );

    }

}
