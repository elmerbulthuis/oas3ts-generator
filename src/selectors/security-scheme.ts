import { OpenAPIV3 } from "openapi-types";
import { resolveObject, resolvePointerParts } from "../utils/index.js";

export function* selectAllSecuritySchemes(
    document: OpenAPIV3.Document,
) {
    for (
        const [securityScheme, maybeSecuritySchemeObject] of
        Object.entries(document.components?.securitySchemes ?? {})
    ) {
        const securitySchemeObject = resolveObject(document, maybeSecuritySchemeObject);
        if (!securitySchemeObject) continue;

        const pointerParts = resolvePointerParts(
            ["components", "securitySchemes", securityScheme],
            maybeSecuritySchemeObject,
        );
        if (!pointerParts) continue;

        const nameParts = [securityScheme, "credential"];

        yield {
            securityScheme,
            securitySchemeObject,
            pointerParts,
            nameParts,
        };
    }
}

export function* selectSecuritySchemesForOperation(
    document: OpenAPIV3.Document,
    operationObject: OpenAPIV3.OperationObject,
) {
    const securitySchemeSet = new Set<string>();
    for (const {
        requirementObject,
    } of selectSecurityRequirementsForOperation(document, operationObject)) {
        for (const {
            securityScheme,
            securitySchemeObject,
            pointerParts,
        } of selectSecuritySchemesForRequirement(document, requirementObject)) {
            if (securitySchemeSet.has(securityScheme)) continue;
            securitySchemeSet.add(securityScheme);

            yield {
                securityScheme,
                securitySchemeObject,
                pointerParts,
            };
        }
    }
}

export function* selectSecurityRequirementsForOperation(
    document: OpenAPIV3.Document,
    operationObject: OpenAPIV3.OperationObject,
) {
    const requirements = operationObject.security ?? document.security ?? [];
    for (const requirementObject of requirements) {
        yield { requirementObject };
    }

}

export function* selectSecuritySchemesForRequirement(
    document: OpenAPIV3.Document,
    requirementObject: OpenAPIV3.SecurityRequirementObject,
) {
    for (const securityScheme of Object.keys(requirementObject)) {
        const maybeSecuritySchemeObject = document.components?.securitySchemes?.[securityScheme];

        const securitySchemeObject = resolveObject(document, maybeSecuritySchemeObject);
        if (!securitySchemeObject) continue;

        const pointerParts = resolvePointerParts(
            ["components", "securitySchemes", securityScheme],
            maybeSecuritySchemeObject,
        );
        if (!pointerParts) continue;

        yield {
            securityScheme,
            securitySchemeObject,
            pointerParts,
        };
    }
}
