import { program } from "commander";
import * as fs from "fs";
import * as yaml from "js-yaml";
import { OpenAPIV3 } from "openapi-types";
import semver from "semver";
import { generatePackage } from "../generators/index.js";

function collect(value: string, previous?: string[]) {
    if (!previous) return [value];
    return [...previous, value];
}

program.
    command("package <openapi-file>").
    requiredOption(
        "--request-type <type>",
        "Preferred request content-type",
        collect,
    ).
    requiredOption(
        "--response-type <type>",
        "Preferred response content-type",
        collect,
    ).
    requiredOption("--package-dir <path>", "folder to write the package files to").
    requiredOption("--package-name <name>", "name of the npm package to generate").
    action(programAction);

interface Options {
    packageDir: string;
    packageName: string;
    requestType: string[];
    responseType: string[];
}
function programAction(
    openapiFile: string,
    options: Options,
) {
    const {
        packageDir,
        packageName,
        requestType: requestTypes,
        responseType: responseTypes,
    } = options;

    const data = fs.readFileSync(openapiFile);
    const document = yaml.load(data.toString()) as OpenAPIV3.Document;

    if (semver.compare(document.openapi, "3.0.0") < 0) {
        throw new Error("Only openapi 3 is supported");
    }

    generatePackage(
        document,
        {
            packageDir,
            packageName,
            requestTypes,
            responseTypes,
        },
    );
}
